<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210105193008 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE base_performance (id INT AUTO_INCREMENT NOT NULL, date DATE NOT NULL, percentage NUMERIC(10, 5) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE charge (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, charge_id VARCHAR(500) NOT NULL, url VARCHAR(1000) NOT NULL, code VARCHAR(255) NOT NULL, created_at VARCHAR(255) NOT NULL, expires_at VARCHAR(255) NOT NULL, amount VARCHAR(255) NOT NULL, INDEX IDX_556BA434A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, subject VARCHAR(255) NOT NULL, message LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact1 (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE document (id INT AUTO_INCREMENT NOT NULL, doc VARCHAR(255) NOT NULL, updated_at DATETIME NOT NULL, description LONGTEXT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE performance_data (id INT AUTO_INCREMENT NOT NULL, percentage_min NUMERIC(10, 5) NOT NULL, percentage_max NUMERIC(10, 5) NOT NULL, percentage_exchange_limit NUMERIC(10, 5) NOT NULL, minimun_amount_to_reinvest NUMERIC(65, 5) DEFAULT NULL, min_amount_deposit NUMERIC(65, 5) DEFAULT NULL, max_amount_deposit NUMERIC(65, 5) DEFAULT NULL, days_delay_performance INT DEFAULT NULL, min_amount_whitdrawal NUMERIC(65, 5) DEFAULT NULL, days_delay_whitdrawal INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE performance_data_user (id INT AUTO_INCREMENT NOT NULL, performance_data_id INT NOT NULL, min_amount NUMERIC(65, 2) NOT NULL, max_amount NUMERIC(65, 2) NOT NULL, percentage NUMERIC(10, 5) NOT NULL, INDEX IDX_D85961E394294966 (performance_data_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE referral (id INT AUTO_INCREMENT NOT NULL, parent_id INT NOT NULL, children_id INT DEFAULT NULL, amount NUMERIC(65, 5) DEFAULT NULL, level INT NOT NULL, INDEX IDX_73079D00727ACA70 (parent_id), INDEX IDX_73079D003D3D2749 (children_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE referral_config (id INT AUTO_INCREMENT NOT NULL, min_amount_deposited NUMERIC(65, 5) DEFAULT NULL, min_amount_deposited_referrals NUMERIC(65, 5) DEFAULT NULL, level INT NOT NULL, percentage NUMERIC(10, 7) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, transaction_id INT DEFAULT NULL, base_performance_id INT DEFAULT NULL, charge_id INT DEFAULT NULL, date DATE DEFAULT NULL, status VARCHAR(255) DEFAULT NULL, amount NUMERIC(65, 5) DEFAULT NULL, type VARCHAR(255) NOT NULL, whitdrawal TINYINT(1) DEFAULT NULL, message JSON DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_723705D1A76ED395 (user_id), INDEX IDX_723705D12FC0CB0F (transaction_id), INDEX IDX_723705D1B2F6EF3 (base_performance_id), INDEX IDX_723705D155284914 (charge_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', password VARCHAR(255) NOT NULL, enabled TINYINT(1) NOT NULL, confirmation_token VARCHAR(255) DEFAULT NULL, reset_password_token VARCHAR(255) DEFAULT NULL, name VARCHAR(255) NOT NULL, surname VARCHAR(255) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, address VARCHAR(255) DEFAULT NULL, anti_phishing_password VARCHAR(255) DEFAULT NULL, two_af TINYINT(1) DEFAULT NULL, hash2_fa LONGTEXT DEFAULT NULL, access2_fa TINYINT(1) DEFAULT NULL, balance NUMERIC(65, 5) DEFAULT NULL, compound_interest TINYINT(1) DEFAULT NULL, deposited NUMERIC(65, 5) DEFAULT NULL, referral_code VARCHAR(255) DEFAULT NULL, referred NUMERIC(65, 5) DEFAULT NULL, performance NUMERIC(65, 5) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE charge ADD CONSTRAINT FK_556BA434A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE performance_data_user ADD CONSTRAINT FK_D85961E394294966 FOREIGN KEY (performance_data_id) REFERENCES performance_data (id)');
        $this->addSql('ALTER TABLE referral ADD CONSTRAINT FK_73079D00727ACA70 FOREIGN KEY (parent_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE referral ADD CONSTRAINT FK_73079D003D3D2749 FOREIGN KEY (children_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D1A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D12FC0CB0F FOREIGN KEY (transaction_id) REFERENCES transaction (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D1B2F6EF3 FOREIGN KEY (base_performance_id) REFERENCES base_performance (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D155284914 FOREIGN KEY (charge_id) REFERENCES charge (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D1B2F6EF3');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D155284914');
        $this->addSql('ALTER TABLE performance_data_user DROP FOREIGN KEY FK_D85961E394294966');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D12FC0CB0F');
        $this->addSql('ALTER TABLE charge DROP FOREIGN KEY FK_556BA434A76ED395');
        $this->addSql('ALTER TABLE referral DROP FOREIGN KEY FK_73079D00727ACA70');
        $this->addSql('ALTER TABLE referral DROP FOREIGN KEY FK_73079D003D3D2749');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D1A76ED395');
        $this->addSql('DROP TABLE base_performance');
        $this->addSql('DROP TABLE charge');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE contact1');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE performance_data');
        $this->addSql('DROP TABLE performance_data_user');
        $this->addSql('DROP TABLE referral');
        $this->addSql('DROP TABLE referral_config');
        $this->addSql('DROP TABLE transaction');
        $this->addSql('DROP TABLE user');
    }
}
