<?php

namespace App\Repository;

use App\Entity\PerformanceDataUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PerformanceDataUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method PerformanceDataUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method PerformanceDataUser[]    findAll()
 * @method PerformanceDataUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PerformanceDataUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PerformanceDataUser::class);
    }

    // /**
    //  * @return PerformanceDataUser[] Returns an array of PerformanceDataUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PerformanceDataUser
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
