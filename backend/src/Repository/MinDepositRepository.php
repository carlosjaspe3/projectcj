<?php

namespace App\Repository;

use App\Entity\MinDeposit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MinDeposit|null find($id, $lockMode = null, $lockVersion = null)
 * @method MinDeposit|null findOneBy(array $criteria, array $orderBy = null)
 * @method MinDeposit[]    findAll()
 * @method MinDeposit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MinDepositRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MinDeposit::class);
    }

    // /**
    //  * @return MinDeposit[] Returns an array of MinDeposit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MinDeposit
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
