<?php

namespace App\Controller\Admin;

use App\Entity\Transaction;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Dto\SearchDto;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FieldCollection;
use EasyCorp\Bundle\EasyAdminBundle\Collection\FilterCollection;
use Doctrine\ORM\QueryBuilder;
use EasyCorp\Bundle\EasyAdminBundle\Orm\EntityRepository;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Filter\BooleanFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\DateTimeFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\TextFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\ChoiceFilter;
use EasyCorp\Bundle\EasyAdminBundle\Filter\NumericFilter;

class TransactionPerforCrudController extends AbstractCrudController
{
    private $adminUrlGenerator;

    public function __construct(AdminUrlGenerator $adminUrlGenerator)
    {
        $this->adminUrlGenerator = $adminUrlGenerator;
    }

    public static function getEntityFqcn(): string
    {
        return Transaction::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Transacción')
            ->setEntityLabelInPlural('Rendimientos')
            ->setSearchFields(['id', 'date', 'type', 'status', 'amount', 'user.email']);
        ;
    }

    public function configureActions(Actions $actions): Actions
    {
        $changeStatusDeposit = Action::new('changeStatusDeposit', 'Actualizar Estado', 'fa fa-edit')
            ->linkToCrudAction('showModalStatusDeposit');

        return $actions
            ->disable('new', 'edit')
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel('Detalle');
            })
            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setIcon('fa fa-pencil')->setLabel(false);
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setIcon('fa fa-trash')->setLabel('Eliminar');
            })

            ->add(Crud::PAGE_INDEX, $changeStatusDeposit)
            ->reorder(Crud::PAGE_INDEX, [Action::DETAIL, 'changeStatusDeposit'])
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $date = DateField::new('date', 'Fecha');
        $status = TextField::new('status', 'Estado');
        $amount = NumberField::new('amount', 'Monto');
        $type = TextField::new('type', 'Tipo');
        $level = IntegerField::new('level', 'Nivel');
        $chargeUUID = TextField::new('chargeUUID');
        $whitdrawal = BooleanField::new('whitdrawal', 'Retirado');
        $btc = NumberField::new('btc');
        $walletBtcBlockio = TextField::new('walletBtcBlockio');
        $deletedAt = DateTimeField::new('deletedAt', 'Eliminado');
        $walletBtcBlockioOld = TextField::new('walletBtcBlockioOld');
        $createdAt = DateTimeField::new('createdAt', 'Fecha de creación');
        $updatedAt = DateTimeField::new('updatedAt', 'Fecha de actualización');
        $user = AssociationField::new('user', 'Usuario');
        $remitter = AssociationField::new('remitter', 'Remitente');
        $transaction = AssociationField::new('transaction');
        $transactions = AssociationField::new('transactions');
        $basePerformance = AssociationField::new('basePerformance');
        $charge = AssociationField::new('charge');
        $id = IntegerField::new('id', 'ID');
        
        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $user, $type, $status, $amount, $date];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $user, $date, $status, $amount, $type, $whitdrawal, $level, $btc, $remitter, $transaction, $transactions, $basePerformance, $createdAt, $updatedAt, $deletedAt];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$date, $status, $amount, $type, $level, $chargeUUID, $whitdrawal, $btc, $walletBtcBlockio, $deletedAt, $walletBtcBlockioOld, $createdAt, $updatedAt, $user, $remitter, $transaction, $transactions, $basePerformance, $charge];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$date, $status, $amount, $type, $level, $chargeUUID, $whitdrawal, $btc, $walletBtcBlockio, $deletedAt, $walletBtcBlockioOld, $createdAt, $updatedAt, $user, $remitter, $transaction, $transactions, $basePerformance, $charge];
        }
    }

    public function showModalStatusDeposit(AdminContext $context)
    {
        $transaction = $context->getEntity()->getInstance();
        $url = $this->adminUrlGenerator->setRoute('change_status_deposit')->generateUrl();
        
        return $this->render('admin/actions/change-status-deposit.html.twig', [
            'transaction' => $transaction,
            'url' => $url,
        ]);
    }

    public function deleteEntity(EntityManagerInterface $entityManager, $transaction): void
    {
        $charge = $transaction->getCharge();
        $user = $transaction->getUser();
        
        $entityManager->remove($charge);
        $entityManager->remove($transaction);
        $entityManager->flush();

        $user->setDeposited();
        $user->setReferred();
        $user->setPerformance();
        $user->setBalance();

        $entityManager->flush();
    }

    public function createIndexQueryBuilder(SearchDto $searchDto, EntityDto $entityDto, FieldCollection $fields, FilterCollection $filters): QueryBuilder
    {
        parent::createIndexQueryBuilder($searchDto, $entityDto, $fields, $filters);

        $response = $this->get(EntityRepository::class)->createQueryBuilder($searchDto, $entityDto, $fields, $filters);
        $response->andWhere('entity.type = :type OR entity.type = :type2')
                ->setParameter('type', 'referral')
                ->setParameter('type2', 'performance');

        return $response;
    }

    public function configureFilters(Filters $filters): Filters
    {    
        $statusChoises = [
            'Aprovado' => 'success', 
            'Pendiente' => 'pending',
            'Confirmando' => 'confirming', 
            'Expirado' => 'expired',
            'Cancelado' => 'cancel',
            /* 'Fallidas' => 'failed', */
            /* 'Creadas' => 'created', */
            /* 'Incompletas' => 'incomplete', */
        ];
        
        $typeChoises = ['Referidos' => 'referral', 'Rendimiento' => 'performance'];

        return $filters
            ->add(DateTimeFilter::new('date', 'Fecha'))
            ->add(ChoiceFilter::new('status', 'Estado')
                ->setChoices($statusChoises)
                ->renderExpanded(true)
                ->canSelectMultiple(true)
            )
            ->add(ChoiceFilter::new('type', 'Tipo')
                ->setChoices($typeChoises)
                ->renderExpanded(true)
                ->canSelectMultiple(true)
            )
            ->add(NumericFilter::new('amount', 'Monto'))
        ;
    }
}
