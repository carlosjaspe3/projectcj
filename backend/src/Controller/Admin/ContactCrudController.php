<?php

namespace App\Controller\Admin;

use App\Entity\Contact;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ContactCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Contact::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('Soporte')
            ->setEntityLabelInPlural('Soportes')
        ;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->disable('new')
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                return $action->setIcon('fa fa-eye')->setLabel('Detalle');
            })

            ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                return $action->setIcon('fa fa-pencil')->setLabel('Modificar');
            })
            ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                return $action->setIcon('fa fa-trash')->setLabel('Eliminar');
            })
        ;
    }

    public function configureFields(string $pageName): iterable
    {
        $email = TextField::new('email', 'Email');
        $type = TextField::new('type', 'Tipo');
        $status = TextField::new('status', 'Estado');
        $subject = TextField::new('subject', 'Asunto');
        $message = TextareaField::new('message', 'Mensale');
        $createdAt = DateTimeField::new('createdAt', 'Fecha de creación');
        $updatedAt = DateTimeField::new('updatedAt', 'Fecha de actualización');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$email, $type, $subject, $status, $createdAt];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$email, $type, $status, $subject, $message, $createdAt, $updatedAt];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$email, $type, $status, $subject, $message, $createdAt, $updatedAt];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$email, $type, $status, $subject, $message, $createdAt, $updatedAt];
        }
    }
}
