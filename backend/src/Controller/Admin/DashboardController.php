<?php

namespace App\Controller\Admin;

use App\Entity\Contact1;
use App\Entity\Contact;
use App\Entity\Document;
use App\Entity\PerformanceData;
use App\Entity\PerformanceDataUser;
use App\Entity\ReferralConfig;
use App\Entity\Transaction;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\Admin\TransactionCrudController;
use App\Controller\Admin\TransactionPerforCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Config\Assets;
use Doctrine\ORM\EntityManagerInterface;

class DashboardController extends AbstractDashboardController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/admin", name = "dashboard_admin")
     */
    public function index(): Response
    {

        $em = $this->getDoctrine()->getManager();

        // $sql = 'SELECT count(*) as total FROM user';
        // $statement = $em->getConnection()->prepare($sql);
        // $statement->execute();
        // $return ['usersAll'] = $statement->fetchAll()[0]['total'];

        $sql = 'SELECT count(*) as total FROM user WHERE enabled=1 and (is_deleted IS NULL OR is_deleted = 0)';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return ['usersAct'] = $statement->fetchAll()[0]['total'];

        $sql = 'SELECT count(*) as total FROM user WHERE enabled=1 and deposited > 0 and (is_deleted IS NULL OR is_deleted = 0)';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return ['usersActInv'] = $statement->fetchAll()[0]['total'];

        $sql = 'SELECT count(*) as total FROM user WHERE created_at >= NOW() - INTERVAL 1 DAY; ';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return ['users24'] = $statement->fetchAll()[0]['total'];


        $sql = 'SELECT sum(deposited) as total FROM user; ';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return ['totalD'] = $statement->fetchAll()[0]['total'];

        $sql = 'SELECT sum(balance) as total FROM user; ';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return ['totalB'] = $statement->fetchAll()[0]['total'];

        $sql = 'SELECT sum(amount) as total FROM transaction WHERE type="withdrawal" AND status = "success"; ';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return ['totalW'] = $statement->fetchAll()[0]['total'];

        $return['totalDW'] = $return ['totalD'] - $return ['totalW'];


        $sql = 'SELECT sum(balance) as total FROM user WHERE balance >=50; ';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return['totalDispW'] = $statement->fetchAll()[0]['total'];

        $sql = 'SELECT IF(sum(amount) IS NULL,0, sum(amount)) as total FROM transaction WHERE type="deposit" AND whitdrawal IS NULL AND status = "success" AND DATEDIFF(NOW(), transaction.date) >= 180';
        $statement = $em->getConnection()->prepare($sql);
        $statement->execute();
        $return['totalDepoL'] = $statement->fetchAll()[0]['total'];
        

        return $this->render('admin/my-dashboard.html.twig', $return);
        
        /* $routeBuilder = $this->get(AdminUrlGenerator::class);
        return $this->redirect($routeBuilder->setController(UserCrudController::class)->generateUrl()); */
    }


    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('<strong>Karakorum</strong>');
    }

    public function configureCrud(): Crud
    {
        return Crud::new()
            ->setDateFormat('dd/MM/yyyy')
            ->setDateTimeFormat('dd/MM/yyyy HH:mm:ss')
            ->setTimeFormat('HH:mm')
            ->showEntityActionsAsDropdown()
        ;
    }

    public function configureMenuItems(): iterable
    {
        $submenu1 = [
            MenuItem::linkToCrud('Datos de Rendimiento', 'fas fa-fan', PerformanceData::class),
            MenuItem::linkToCrud('Datos Rendi. Usuario', 'fas fa-server', PerformanceDataUser::class),
            MenuItem::linkToCrud('Config. de Referidos', 'fas fa-digital-tachograph', ReferralConfig::class),
        ];
        $submenu2 = [
            MenuItem::linkToCrud('Depósitos y Retiros', 'fas fa-money-check', Transaction::class)
                ->setController(TransactionCrudController::class),
            MenuItem::linkToCrud('Rendimientos', 'fas fa-money-bill-wave', Transaction::class)
                ->setController(TransactionPerforCrudController::class),
        ];

        yield MenuItem::section('Home');
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        
        yield MenuItem::section('Menu');
        yield MenuItem::linkToCrud('Usuarios', 'fas fa-users', User::class);
        yield MenuItem::subMenu('Transacciones', 'fas fa-exchange-alt')->setSubItems($submenu2);

        yield MenuItem::section('Otros');
        yield MenuItem::linkToCrud('Soporte', 'fas fa-ticket-alt', Contact::class);
        yield MenuItem::linkToCrud('Contacto', 'fas fa-phone', Contact1::class);
        yield MenuItem::linkToCrud('Documentos', 'fas fa-file', Document::class);
        yield MenuItem::subMenu('Configuración', 'fas fa-cogs')->setSubItems($submenu1);

        yield MenuItem::section('Salir');
        yield MenuItem::linkToLogout('Cerrar sesión', 'fas fa-sign-out-alt');
    }

    public function configureAssets(): Assets
    {
        return Assets::new()
            ->addCssFile('css/admin.css')
            ->addCssFile('css/icons.min.css')
            ->addCssFile('css/app1.min.css')
        ;
    }
}
