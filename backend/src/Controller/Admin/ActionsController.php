<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use App\Controller\Admin\UserCrudController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Charge as UserCharge;
use App\Entity\User;
use App\Entity\Transaction;
use App\Util\Mailer;

class ActionsController extends AbstractController
{
    private $adminUrlGenerator;

    public function __construct(AdminUrlGenerator $adminUrlGenerator)
    {
        $this->adminUrlGenerator = $adminUrlGenerator;
    }

    /**
    * @Route(path = "/admin/user/deposit/new", name = "user_deposit_new")
    */
    public function newUserDepositAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($request->get("user_id"));

        // Carga creada
        $appCharge = new UserCharge();
        $appCharge->setUser($user);
        $appCharge->setUrl('');
        $appCharge->setChargeId('');
        $appCharge->setCode('');
        $appCharge->setCreatedAt($request->get("date"));
        $appCharge->setExpiresAt($request->get("date"));
        $appCharge->setAmount($request->get("amount"));
        $appCharge->setOrigin('Admin');

        $em->persist($appCharge);
        $em->flush();

        // Transacción creada
        $transaction = new Transaction();
        $transaction->setType(Transaction::TYPE_DEPOSIT)
                    ->setAmount($appCharge->getAmount())
                    ->setDate(new \DateTime($request->get("date")))
                    ->setStatus('created')
                    ->setUser($appCharge->getUser())
                    ->setCharge($appCharge)
                    ->setChargeUUID('')
                    ->setMessage(['Admin'])
                    ->setStatus('success');
        $em->persist($transaction);
        $em->flush();
        
        // Transacción completada
        $transaction->getUser()->setDeposited();
        $em->flush();

        $url = $this->adminUrlGenerator
            ->setController(UserCrudController::class)
            ->setAction(Action::INDEX)
            ->generateUrl();

        return $this->redirect($url);
    }

    /**
    * @Route(path = "/admin/user/deposit/status/change", name = "change_status_deposit")
    */
    public function userStatusDepositAction(Request $request)
    {  
        $em = $this->getDoctrine()->getManager();
        $transaction = $em->getRepository(Transaction::class)->find($request->get("transaction_id"));
        $value = $request->get("transaction_status");
        $oldValue = $transaction->getStatus();

        if ($oldValue != $value) {
            if ($value == 'success') {
                $transaction->setStatus('success');
            }
            elseif ($value == 'cancel') {
                $transaction->setStatus('cancel');
            }

            $transaction->getUser()->setDeposited();
            $transaction->getUser()->setReferred();
            $transaction->getUser()->setPerformance();
            $transaction->getUser()->setBalance();
            
            $em->flush();
        }
        
        $url = $this->adminUrlGenerator
            ->setController(TransactionCrudController::class)
            ->setAction(Action::INDEX)
            ->generateUrl();

        return $this->redirect($url);
    }

    /**
    * @Route(path = "/admin/user/sendemailconfirmation", name = "confirmation_email_send")
    */
    public function sendEmailConfirmationAction(Request $request, Mailer $mailer)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->find($request->get("user"));

        $mailer->send(
            'Valida tu Email', 
            'access/account_verification.html.twig', 
            [
                'token' => $user->getConfirmationToken(), 
                'user' => $user
            ], 
            $user->getEmail()
        );

        $url = $this->adminUrlGenerator
            ->setController(UserCrudController::class)
            ->setAction(Action::INDEX)
            ->generateUrl();

        $this->addFlash('success', 'Email enviado con exito!');
        return $this->redirect($url);
    }
}
