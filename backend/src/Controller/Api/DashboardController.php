<?php

namespace App\Controller\Api;

use App\Entity\BasePerformance;
use App\Entity\Transaction;
use App\Entity\Movements;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class DashboardController extends AbstractController
{
    /**
     * @Route("/api/dashboard", name="api_dashboard")
     */
    public function index(Request $request, SerializerInterface $serializer)
    {
        $user = $this->getUser();
        $balance = $user->getBalance();
        $totalBuy = $user->getDeposited();
        $performanceByBuy = $user->getPerformance();
        $performanceByRefered = $user->getReferred();
        $performanceTotal = $performanceByBuy + $performanceByRefered;

        $now =  new DateTime();
        $days = $request->get('days', 30);
        $dateStartPb = clone $now;
        $dateStartPb->sub(new DateInterval('P' . $days . 'D'));
        $performanceBase = $this
            ->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('pb')
            ->from(BasePerformance::class, 'pb')
            ->where('pb.date >= :dateStart')
            ->andWhere('pb.date <= :dateEnd')
            ->setParameters([
                'dateStart' => $dateStartPb,
                'dateEnd' => $now,
            ])
            ->getQuery()
            ->getResult();
        $perfromanceBaseSerialized = $serializer->normalize($performanceBase, null, ['groups' => ['performance-base']]);

        $performanceUser = $this
            ->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('pu')
            ->from(Transaction::class, 'pu')
            ->where('pu.date >= :dateStart')
            ->andWhere('pu.date <= :dateEnd')
            ->andWhere('pu.user = :user')
            ->andWhere('pu.type = :typePerformance')
            ->setParameters([
                'dateStart' => $dateStartPb,
                'dateEnd' => $now,
                'user' => $user,
                'typePerformance' => Transaction::TYPE_PERFORMANCE
            ])
            ->getQuery()
            ->getResult();

        $performanceUserReferral = $this
            ->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('pu')
            ->from(Transaction::class, 'pu')
            ->where('pu.date >= :dateStart')
            ->andWhere('pu.date <= :dateEnd')
            ->andWhere('pu.user = :user')
            ->andWhere('pu.type = :typePerformance')
            ->setParameters([
                'dateStart' => $dateStartPb,
                'dateEnd' => $now,
                'user' => $user,
                'typePerformance' => Transaction::TYPE_REFERRAL
            ])
            ->getQuery()
            ->getResult();
            $performanceUserAndReferral = [];
            while ($dateStartPb < $now) {
                $performanceUserAmountByDate = 0;
                foreach ($performanceUser as $performanceU) {
                    if ($performanceU->getDate()->format("Ymd") == $dateStartPb->format("Ymd")) {
                        $performanceUserAmountByDate += $performanceU->getAmount();
                    }
                }
                $performanceUserReferralAmountByDate = 0;
                foreach ($performanceUserReferral as $performanceUReferral) {
                    if ($performanceUReferral->getDate()->format("Ymd") == $dateStartPb->format("Ymd")) {
                        $performanceUserReferralAmountByDate += $performanceUReferral->getAmount();
                    }
                }
                $performanceUserAndReferral[] = ["date"=> $dateStartPb->format("Y-m-d"), "performanceUser" => $performanceUserAmountByDate, "performanceUserReferral" => $performanceUserReferralAmountByDate];
                $dateStartPb->add(new DateInterval('P1D'));
            }

        $perfromanceUserSerialized = $serializer->normalize($performanceUser, null, ['groups' => ['performance-user']]);

        $transactions = $this
            ->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('t.id','t.date','t.status','SUM(t.amount) as amount','t.type','t.message')
            ->from(Transaction::class, 't')
            ->where('t.user = :user')
            ->andWhere('t.status = :status')
            ->andWhere('t.type != :buy')
            ->setParameter('buy','buy')
            ->setParameter('status','success')
            ->setParameter('user', $user)
            ->groupBy('t.date, t.type')
            ->orderBy('t.id', 'DESC')
            ->setMaxResults(15)
            ->getQuery()
            ->getResult();
        //return $this->json($transactions);
        $transactionsSerialized = $serializer->normalize($transactions, null, ['groups' => ['performance-user']]);

        foreach($transactionsSerialized as $k => $v){
            if(in_array('Buy with Balance', $v['message'])){
                $transactionsSerialized[$k]['buy'] = true;
            }
        }

        return $this->json([
            'balance' => $balance,
            'totalBuy' => $totalBuy,
            'performanceByBuy' => $performanceByBuy,
            'performanceByRefered' => $performanceByRefered,
            'performanceTotal' => $performanceTotal,
            'performanceBase' => $perfromanceBaseSerialized,
            'performanceUser' => $perfromanceUserSerialized,
            'transactions' => $transactionsSerialized,
            "performanceUserAndReferral" => $performanceUserAndReferral
        ]);
    }

    /**
     * @Route("/api/monitor-operations", name="monitor_operations", methods="GET")
     */
    public function monitorOperations(Request $request)
    {
        $limited = $request->get('limit') ? $request->get('limit') : 40;
        $conn = $this->getDoctrine()->getManager()
            ->getConnection();
        $sql = 'SELECT * FROM monitors ORDER BY id DESC LIMIT :limited';
        $stmt = $conn->prepare($sql);
        $stmt->bindValue('limited', $limited, \PDO::PARAM_INT);

        $stmt->execute();

        return $this->json($stmt->fetchAll());
    }

    /**
     * @Route("/api/monitor-movimientos", name="monitor_movimientos", methods="GET")
     */
    public function monitorMovimientosAction(Request $request, PaginatorInterface $paginator, SerializerInterface $serializer)
    {
        $em = $this->getDoctrine()->getManager();
        $movementsRepository = $em->getRepository(Movements::class);

        $limited = $request->get('limit') ? $request->get('limit') : 10;
        $page = $request->get('page') ? $request->get('page') : 1;
        $user = $this->getUser();

        $allMovementsQuery = $movementsRepository->createQueryBuilder('m')
            ->where('m.user_id = :user')
            ->andWhere('m.type != :deposit')
            ->andWhere('m.type != :withdrawal')
            ->andWhere('m.type != :buy')
            ->setParameter('user', $user)
            ->setParameter('deposit', 'deposit')
            ->setParameter('withdrawal','withdrawal')
            ->setParameter('buy','buy')
            ->orderBy('m.id', 'DESC');

        if($request->get('type')){
            $allMovementsQuery->andWhere('m.type = :type')->setParameter('type', $request->get('type'));
        }
        if($request->get('status')){
            $allMovementsQuery->andWhere('m.status = :status')->setParameter('status', $request->get('status'));
        }
        
        $date = $request->get('date'); 
        
        if($date['after']){
            $allMovementsQuery->andWhere('m.date >= :after')->setParameter('after', $date['after']);
        }
        if($date['before']){
            $allMovementsQuery->andWhere('m.date <= :before')->setParameter('before', $date['before']);
        }

        $movements = $paginator->paginate(
            $allMovementsQuery->getQuery(),
            $page,
            $limited
        );

        $totalItems = $movements->getTotalItemCount();
        $totalPages = ceil($totalItems / $limited);

        /* return $this->json($movements); */
        
        return $this->json([
            'OK' => true,
            'data' => [
                'page' => $page,
                'limited' => $limited,
                'totalPages' => $totalPages,
                'totalItems' => $totalItems,
                'movements' => $movements
            ]
        ]);  
    }

    /**
     * @Route("/api/monitor-depositos", name="monitor_depositos", methods="GET")
     */
     public function monitorDepositosAction(Request $request, PaginatorInterface $paginator, SerializerInterface $serializer)
     {
         $em = $this->getDoctrine()->getManager();
         $movementsRepository = $em->getRepository(Transaction::class);
 
         $limited = $request->get('limit') ? $request->get('limit') : 10;
         $page = $request->get('page') ? $request->get('page') : 1;
         $user = $this->getUser();
         
        
         $allMovementsQuery = $movementsRepository->createQueryBuilder('m')
            ->select('m.id as transactionId', 'm.amount', 'm.date', 'm.status', 'm.type')
            ->where('m.user = :user')
            ->andWhere('m.type IN (:types)')
            ->andwhere('m.message NOT LIKE :message')
            //->andWhere('m.status != :expired')
            ->setParameter('user', $user)
            ->setParameter('message', '%Buy with Balance%')
            //->setParameter('expired', 'expired')
            ->setParameter('types', ['deposit', 'withdrawal', 'buy'])
            ->orderBy('m.id', 'DESC');
        
        
        
        if($request->get('type')){
             $allMovementsQuery->andWhere('m.type = :type')->setParameter('type', $request->get('type'));
         }
         if($request->get('status')){
             $allMovementsQuery->andWhere('m.status = :status')->setParameter('status', $request->get('status'));
         }
         
         $date = $request->get('date'); 
         
         if($date['after']){
             $allMovementsQuery->andWhere('m.date >= :after')->setParameter('after', $date['after']);
         }
         if($date['before']){
             $allMovementsQuery->andWhere('m.date <= :before')->setParameter('before', $date['before']);
         }
 
         $movements = $paginator->paginate(
             $allMovementsQuery->getQuery(),
             $page,
             $limited
         );
 
         $totalItems = $movements->getTotalItemCount();
         $totalPages = ceil($totalItems / $limited);
 
         /* return $this->json($movements); */
         
         return $this->json([
             'OK' => true,
             'data' => [
                 'page' => $page,
                 'limited' => $limited,
                 'totalPages' => $totalPages,
                 'totalItems' => $totalItems,
                 'movements' => $movements
             ]
         ]);  
     }

    /**
     * @Route("/api/details-movimientos", name="details", methods="GET")
     */
    public function detailsMovimientosAction(Request $request, PaginatorInterface $paginator, SerializerInterface $serializer)
    {   

        $user = $this->getUser();
        $transactions = $this
            ->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('t.id, t.level, sum(t.amount) as total, t.date')
            ->from(Transaction::class, 't')
            ->where('t.user = :user')
            ->setParameter('user', $user)
            ->andWhere('t.type = :type')->setParameter('type', $request->get('type'))
            ->andWhere('t.date LIKE :date')->setParameter('date', '%'.$request->get('date').'%')
            ->groupBy('t.level')
            ->orderBy('t.level', 'ASC')
            ->getQuery()->getResult();
            
        $movements =  $this->getDoctrine()
            ->getManager()
            ->createQueryBuilder()
            ->select('t.id, t.date, t.user_id, t.type')
            ->from(Movements::class, 't')
            ->where('t.user_id = :user')->setParameter('user', $user->getId())
            ->andWhere('t.type = :type')->setParameter('type', $request->get('type'))
            ->andWhere('t.date = :date')->setParameter('date', $request->get('date'))
            ->getQuery()->getOneOrNullResult();
        
        return $this->json([
            'OK' => true,
            'data' => $transactions,
            'aux' => $movements
        ]);  
    }

}
