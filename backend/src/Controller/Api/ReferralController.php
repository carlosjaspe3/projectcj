<?php

namespace App\Controller\Api;

use App\Entity\Referral;
use App\Entity\User;
use App\Entity\ReferralConfig;
use App\Entity\PerformanceData;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ReferralController extends AbstractController
{
    /**
     * @Route("/api/levels", name="user-levels")
     */
    public function userLevels(Request $request)
    {
        $user = $this->getUser();
        $iterator = 1;
        $data = [];
        while($iterator <= 10){
            $levelData = ['level'=> $iterator];
            $referrals = $this
                ->getDoctrine()
                ->getManager()
                ->createQueryBuilder()
                ->select('r','rc','rct')
                ->from(Referral::class,'r')
                ->join('r.children', 'rc')
                ->leftJoin('rc.transactions', 'rct')
                ->where('r.level = :level')
                ->andWhere('r.parent = :parent')
                ->setParameters([
                    'level' => $iterator,
                    'parent' => $user
                ])
                ->getQuery()
                ->getResult();
            $referrals = new ArrayCollection($referrals);
            $totalReferrals = $referrals->filter(function ($i) use ($iterator)
            {
                return $i->getLevel() == $iterator;
            })->count();
            $totalReferralsWhitPayments = $referrals->filter(function ($i) use ($iterator)
            {
                return $i->getLevel() == $iterator && $i->getChildren()->getDeposited()>0;
            })->count();
            $totalBuy = 0;
            foreach ($referrals as $referral) {
                if ($referral->getLevel() == $iterator) {
                    $totalBuy += $referral->getChildren()->getDeposited();
                }
            }
            $totalReferred = 0;
            foreach ($referrals as $referral) {
                if ($referral->getLevel() == $iterator) {
                    $totalReferred += $referral->getAmount();
                }
            }

            /************/
            $performanceData = $this->getDoctrine()->getManager()->getRepository(PerformanceData::class)->findOneBy([]);
            if($iterator == 1 || $iterator == 2){
                $condition = $this->getDoctrine()->getManager()->getRepository(ReferralConfig::class)->findOneBy(['level' => $iterator]);
                if($user->getContributeBuyForLevel($iterator, $condition->getMinAmountDeposited(), $performanceData->getMinAmountDeposit())){
                    //Nivel Desbloqueado
                    $levelData['levelUnlocked'] = true;
                }else{
                    //Nivel Bloqueado
                    $levelData['levelUnlocked'] = false;
                }
            }else{
                $condition = $this->getDoctrine()->getManager()->getRepository(ReferralConfig::class)->findOneBy(['level' => $iterator]);
                if($user->getContributeBuyForLevel($iterator, $condition->getMinAmountDeposited(), $performanceData->getMinAmountDeposit())
                || $user->getContributeBuyVolTeam($iterator, $condition->getMinAmountDepositedReferrals(), $performanceData->getMinAmountDeposit())){
                    //Nivel Desbloqueado
                    $levelData['levelUnlocked'] = true;
                }else{
                    //Nivel Bloqueado
                    $levelData['levelUnlocked'] = false;
                }
            }
            /************/
            
            $levelData['totalReferrals'] = $totalReferrals;
            $levelData['totalReferralsWhitPayments'] = $totalReferralsWhitPayments;
            $levelData['totalBuy'] = $totalBuy;
            $levelData['totalReferred'] = $totalReferred;
            $data[] = $levelData;
            $iterator += 1; 
        }
        return $this->json($data);
    }

    /**
     * @Route("/api/tree", name="user-tree")
     */
    public function userTree(Request $request, SerializerInterface $serializerInterface)
    {
        $children = $this->getUser()->getChildrenFirstLevel();
        $childrenSerializer = $serializerInterface->normalize($children, null, ['groups' => ['tree']]);

        return $this->json($childrenSerializer);
    }
    
    /**
     * @Route("/api/validate-referral-code/{code}", name="validate-referral-code", methods="GET")
     */
    public function validateReferrral(Request $request, $code)
    {
        $em = $this->getDoctrine()->getManager();

        $parentUser = $em->getRepository(User::class)->findOneBy(['referralCode'=>$code]);

        if(!$parentUser){
            return $this->json([
                'OK' => false,
                'data' => [
                    'error' => 'Codigo de referido invalido'
                ]
            ]);
        }else{
            return $this->json([
                'OK' => true,
                'data' => [
                    'name' => $parentUser->getName() . " " . $parentUser->getSurname(),
                    'referralCode' => $parentUser->getReferralCode()
                ]
            ]);
        }
    }
}
