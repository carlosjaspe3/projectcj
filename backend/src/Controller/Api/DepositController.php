<?php

namespace App\Controller\Api;

use CoinbaseCommerce\ApiClient;
use CoinbaseCommerce\Resources\Charge;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Entity\Charge as UserCharge;
use App\Entity\Transaction;
use App\Entity\WalletAvailable;
use App\Entity\PerformanceData;
use DateInterval;
use DateTime;

class DepositController extends AbstractController
{
    /**
     * @Route("/api/charge/resolve", name="api_resolve", methods="POST")
     */
    public function resolve(Request $request)
    {
        // $id = $request->get('code');
        // $apiClientObj = ApiClient::init("71e690c9-4fb6-48a3-ac50-a4677f1ca4aa");
        // $apiClientObj->setTimeout(15);

        // $chargeObj = Charge::retrieve($id);

        // if ($chargeObj) {
        //     try{
        //         $chargeObj->resolve();
        //     }catch(\Exception $e){
        //         return new JsonResponse([
        //             'success' => false,
        //             'message' => 'Solo se pueden cancelar cargos en el estado UNRESOLVED.'
        //         ]);
        //     }
            

        //     return new JsonResponse([
        //         'success' => true,
        //         'message' => 'Cargo resuelto.'
        //     ]);
        // }else{
        //     return new JsonResponse([
        //         'success' => false,
        //         'message' => 'Cargo no encontrado.'
        //     ]);
        // }


    }

    /**
     * @Route("/api/deposit", name="api_deposit", methods="POST")
     */
    public function index(Request $request, SerializerInterface $serializer)
    {
        $amount = $request->get("amount");
        #$amount = 5;
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        //Debemos validar que el wallet exista sino la pedimos
        $salida = '';
        $wallet = null;
        $btc = 0;
        //Gerenamos una transaccion

        $now = (new DateTime());
        $now->add(new DateInterval("PT72H"));
        $expire = $now->format('Y-m-d H:i:s');
        
        //Creamos una carga
        $appCharge = new UserCharge();
        $appCharge->setUser($user);
        $appCharge->setCreatedAt(date('Y-m-d H:i:s'));
        $appCharge->setExpiresAt($expire);
        $appCharge->setAmount($amount);
        $appCharge->setChargeId('');
        $appCharge->setUrl('');
        $appCharge->setCode('');
        $appCharge->setData(json_encode(['wallet'=>$wallet, 'btc'=>$btc]));
        $em->persist($appCharge);
        $em->flush();

        //$chg_id = $appCharge->getId();

        $transaction = new Transaction();
        $transaction->setType(Transaction::TYPE_DEPOSIT)
                ->setAmount($appCharge->getAmount())
                ->setDate(new \DateTime(date('Y-m-d')))
                ->setStatus('pending')
                ->setUser($user)
                ->setCharge($appCharge)
                ->setChargeUUID('')
                ->setBtc(0)
                ->setWalletBtcBlockio('');
        $em->persist($transaction);
        $em->flush();
        // Crada la transaccion procedemos a generar la wallet

        // Capturamos si hay wallets creadas disponibles
        $WalletAvailable = $this->getWalletsAvailables($amount);

        // Si hay wallet disponible la utilizamos
        if (isset($WalletAvailable['wallet'])) {

            $wallet = $WalletAvailable['wallet'];
            $btc = $WalletAvailable['btc'];

            $transaction->setWalletBtcBlockio($wallet);
            $transaction->setBtc($btc);
            $transaction->getCharge()->setData(json_encode(['wallet'=>$wallet, 'btc'=>$btc]));
            
            $em->flush();
        
            $end = new DateTime($expire);
            $end = $end->modify('-66 hours');

            $return = [
                'OK' => true,
                'data' => [
                    'wallet' => $wallet,
                    'btc' => $btc,
                    'end' => $end->format('Y-m-d H:i:s'),
                    'transaction_id' => $transaction->getId()
                ]
            ];

        } else { 
            // Sino se genera la wallet
            $trx_id = $transaction->getId();
            $commandString = "/usr/bin/python3 {$_SERVER['DIR_FILE_BOT']}/depositos.py {$trx_id} {$amount}";
            $salida = json_decode(shell_exec($commandString),true);

            if(isset($salida['data']['data']['address'])){
                $wallet = $salida['data']['data']['address'];
                $btc = $salida['btc'];

                $transaction->setWalletBtcBlockio($wallet);
                $transaction->setBtc($btc);
                $transaction->getCharge()->setData(json_encode(['wallet'=>$wallet, 'btc'=>$btc]));
                
                $em->flush();
            
                $end = new DateTime($expire);
                $end = $end->modify('-66 hours');

                $return = [
                    'OK' => true,
                    'data' => [
                        'wallet' => $wallet,
                        'btc' => $btc,
                        'end' => $end->format('Y-m-d H:i:s'),
                        'transaction_id' => $transaction->getId()
                    ]
                ];
            }
        }
        
        if (!isset($return)) {
            $return = [
                'OK' => false,
                'msg' => 'La transacción no pudo ser generada',
                'wallet'=>$wallet,
                'btc' => $btc,
                'transaction_id' => $transaction->getId()
            ];
        }

        return new JsonResponse($return);        
    }


    /**
     * @Route("/coinbase/webhook", name="api_coinbase", methods="POST")
     */
    public function coinbaseWebHook(Request $request)
    {
        // $input = $request->getContent() ? json_decode($request->getContent(), true) : [];
        // $em = $this->getDoctrine()->getManager();

        // // Transacción creada
        // if($input && $input['event']['type'] == 'charge:created'){
        //         $id = $input['event']['id'];
        //         $charge = $em->getRepository('App:Charge')->findOneBy(['chargeId' => $id]);
        //         $transaction = new Transaction();
        //         $transaction->setType(Transaction::TYPE_DEPOSIT)
        //                 ->setAmount($charge->getAmount())
        //                 ->setDate(new \DateTime())
        //                 ->setStatus('created')
        //                 ->setUser($charge->getUser())
        //                 ->setCharge($charge)
        //                 ->setChargeUUID($id);
        //         $em->persist($transaction);
        //         $em->flush();
        // }
        
        // //Transaccion Pendiente
        // if($input && $input['event']['type'] == 'charge:pending'){
        //     $id = $input['event']['id'];
        //     $transaction = $em->getRepository('App:Transaction')->findOneBy(['chargeUUID' => $id]);
            
        //     if($transaction){
        //         $transaction->setStatus('pending');
        //         $em->flush();
        //     }
        // }
        
        // //Transaccion Pagada luego del tiempo de espera
        // if($input && $input['event']['type'] == 'charge:delayed'){
        //     $id = $input['event']['id'];
        //     $transaction = $em->getRepository('App:Transaction')->findOneBy(['chargeUUID' => $id]);
            
        //     if($transaction){
        //         $transaction->setStatus('delayed');
        //         $em->flush();
        //     }
        // }
        
        // //Transaccion fallida
        // if($input && $input['event']['type'] == 'charge:failed'){
        //     $id = $input['event']['id'];
        //     $transaction = $em->getRepository('App:Transaction')->findOneBy(['chargeUUID' => $id]);
            
        //     if($transaction){
        //         $transaction->setStatus('failed');
        //         $em->flush();
        //     }
        // }

        // // Transacción completada
        // if($input && ($input['event']['type'] == 'charge:confirmed' || $input['event']['type'] == 'charge:resolved') ){
        //     $id = $input['event']['id'];
        //     $transaction = $em->getRepository('App:Transaction')->findOneBy(['chargeUUID' => $id]);
            
        //     if($transaction){
        //         $transaction->setStatus('success');
        //         $transaction->getUser()->setDeposited($transaction->getUser()->getDeposited()+$transaction->getAmount());
        //         $em->flush();
        //     }
        // }

        // return new JsonResponse([
        //     'success' => true,
        //     'type' => $input['event']['type'],
        //     'id' => $input['event']['id']
        // ]);

    }

    /**
     * @Route("/api/transaction/{id}", name="transaction-detail", methods="GET")
     */
    public function transactionDetail(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        
        $transaction = $em->getRepository(Transaction::class)->find($id);
        if (!$transaction) {
            return $this->json([
                'OK' => false,
                'data' => [
                    'error' => 'Transaction no found'
                ]
            ]);
        }

        $charge = $transaction->getCharge();
        $chargeData = [];
        $end = new DateTime();
        if($charge){
            $chargeData =  json_decode($charge->getData(), true);
            $end = new DateTime($charge->getExpiresAt());
            $end = $end->modify('-66 hours');
        }

        $remaining = 0;
        $msg = $transaction->getMessage();
        if(isset($msg['remaining'])){
            $remaining = $msg['remaining'];
        }

        $txs = [];
        if(isset($msg['txs'])){
            $txs = $msg['txs'];
        }

        //Dias minimos para el retiro
        $dataBaseApp = $em->getRepository(PerformanceData::class)->findOneBy([]);
        $daysDelayWhitdrawal = $dataBaseApp->getDaysDelayWhitdrawal();
        // Fecha de la transaccion
        $dateTransaction = clone $transaction->getDate();
        $now = new DateTime();
        $diff = $dateTransaction->diff($now);
        // Calculamos dias Restantes
        $remainingDaysDelayWhitdrawal = ($daysDelayWhitdrawal - $diff->days);
        
        $withdrawal = [];

        if($transaction->getType() == 'withdrawal'){
            $msg = $transaction->getMessage();

            if(isset($msg['response']['data']['txid'])){
                $txid = $msg['response']['data']['txid'];
                
                return $this->json([
                    'OK' => true,
                    'data' => [
                        'status' => $transaction->getStatus(),
                        'wallet' => $msg['wallet_destino'],
                        'btc' => $transaction->getBtc(),
                        'transaction_id' => $id,
                        'amount' => $transaction->getAmount(),
                        'date' => $transaction->getDate(),
                        'url' => sprintf('https://www.blockchain.com/es/btc/tx/%s', $txid),
                        'txid' => $txid,
                        'withdrawal' => true
                    ]
                ]);
            }else{

                return $this->json([
                    'OK' => true,
                    'data' => [
                        'status' => $transaction->getStatus(),
                        'btc' => $transaction->getBtc(),
                        'transaction_id' => $id,
                        'amount' => $transaction->getAmount(),
                        'date' => $transaction->getDate(),
                        'withdrawal' => true
                    ]
                ]);

            }
        }elseif($transaction->getType() == 'buy'){
            //Tipo compra
            return $this->json([
                'OK' => true,
                'detail'=> 'buy',
                'data' => [
                    'transaction_id' => $id,
                    'status' => $transaction->getStatus(),
                    'amount' => $transaction->getAmount(),
                    'date' => $transaction->getDate(),
                    'RemainingDaysDelayWhitdrawal' => ($remainingDaysDelayWhitdrawal >= 0) ? $remainingDaysDelayWhitdrawal : 0,
                ]
            ]); 

        }elseif(in_array('Buy with Balance',  $transaction->getMessage())){
            return $this->json([
                'OK' => true,
                'detail'=> 'Deposit - Buy with Balance'
            ]); 
        }
        //Todas las demas
        return $this->json([
            'OK' => true,
            'data' => [
                'status' => $transaction->getStatus(),
                'end' => $end->format('Y-m-d H:i:s'),
                'expire' => $charge->getExpiresAt(),
                'wallet' => isset($chargeData['wallet']) ? $chargeData['wallet'] : null,
                'btc' => isset($chargeData['btc']) ? $chargeData['btc'] : null,
                'remaining' => $remaining,
                'txs'=> $txs,
                'transaction_id' => $id,
                'amount' => $transaction->getAmount(),
                'date' => $transaction->getDate(),
                'RemainingDaysDelayWhitdrawal' => ($remainingDaysDelayWhitdrawal >= 0) ? $remainingDaysDelayWhitdrawal : 0,
            ]
        ]);
    }

    /**
     * @Route("/api/wallets/available", name="wallets-availables", methods="GET")
     */
    private function getWalletsAvailables($amount)
    {
        $em = $this->getDoctrine()->getManager();
        $walletsRepository = $em->getRepository(Transaction::class);

        // Buscamos ls wallets expiradas y que aun no han sido reasignadas.
        $walletsExpired = $walletsRepository->findBy(
            [
                'status' => 'expired', // Expiradas 
            ],
            ['id' => 'ASC']
        );
        foreach($walletsExpired as $wallet){
            if($wallet->getWalletBtcBlockio() != NULL){
                
                // Comprobamos si se puede reutilizar esta wallet
                $commandString = "/usr/bin/python3 {$_SERVER['DIR_FILE_BOT']}/check_addres.py {$wallet->getWalletBtcBlockio()} {$amount}";
                
                $salida = json_decode(shell_exec($commandString),true);

                // Capturamos el valor de la respuesta
                // Si esta disponible
                if(isset($salida['disponible']) && $salida['disponible'] == true ){
                    // Resguardamos el campo wallet en WalletBtcBlockioOld
                    $wallet->setWalletBtcBlockioOld( $wallet->getWalletBtcBlockio() );
                    // Seteamos el campo WalletBtcBlockio como Null
                    $wallet->setWalletBtcBlockio(NULL);
                    $em->flush();

                    // Retornamos la wallet
                    return [
                        "wallet" => $wallet->getWalletBtcBlockioOld(),
                        "btc" => $salida['btc'],
                    ];
                }
            }
        }

        // Retornamos null, si no hay wallets disponibles.
        return Null;
    }

    /**
     * @Route("/api/deposit/pending/delete/{id}", name="delete-deposit", methods="POST")
     */
    public function deleteDeposit(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        #return $this->json($request);
        $transaction = $em->getRepository(Transaction::class)
                        ->findOneBy([
                            'id' => $id,
                            'user' => $user->getId(),
                        ]);

        if (!$transaction) {
            return $this->json([
                'OK' => false,
                'data' => [
                    'error' => 'Transaction no found'
                ]
            ]);
        }
        if ($transaction->getType() != 'deposit') {
            return $this->json([
                'OK' => false,
                'data' => [
                    'error' => 'Transaction is not a deposit'
                ]
            ]);
        }
        if ($transaction->getStatus() != 'pending') {
            return $this->json([
                'OK' => false,
                'data' => [
                    'error' => 'Transaction is not status pending'
                ]
            ]);
        }

        $em->remove($transaction);
        $em->flush();

        return $this->json([
            'OK' => true,
            'data' => []
        ]);
    }

    /**
     * @Route("/api/deposit/pending/validate", name="deposit-pending-validate", methods="GET")
     */
    public function depositPendingValidate(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $transaction = $em->getRepository(Transaction::class)
                        ->findOneBy([
                            'user' => $user->getId(),
                            'type' => 'deposit',
                            'status' => 'pending'
                        ],[
                            'id' => 'desc'
                        ]);
        
        // $transaction = $em->createQueryBuilder()
        //             ->select('t')
        //             ->from(Transaction::class, 't')
        //             ->where('t.user = :user')->setParameter('user', $user)
        //             ->andWhere('t.type = :type')->setParameter('type', 'deposit')
        //             ->andWhere('t.status = :status')->setParameter('status', 'pending')
        //             ->orderBy('t.id', 'asc')
        //             ->getQuery()
        //             ->getResult();

        
        if(empty($transaction)){
            return $this->json([
                'OK' => true,
                'data' => false
            ]);
        }
        
        $charge = $transaction->getCharge();
        $chargeData = json_decode($charge->getData(), true);

        $end = new DateTime($charge->getExpiresAt());
        $end = $end->modify('-66 hours');


        $now = new DateTime();
        if($now > $end){
            return $this->json([
                'OK' => true,
                'data' => false
            ]);
        }


        $remaining = 0;
        $msg = $transaction->getMessage();
        if(isset($msg['remaining'])){
            $remaining = $msg['remaining'];
        }

        $txs = [];
        if(isset($msg['txs'])){
            $txs = $msg['txs'];
        }
        

        return $this->json([
            'OK' => true,
            'data' => [
                'status' => $transaction->getStatus(),
                'end' => $end->format('Y-m-d H:i:s'),
                'expire' => $charge->getExpiresAt(),
                'wallet' => isset($chargeData['wallet']) ? $chargeData['wallet'] : null,
                'btc' => isset($chargeData['btc']) ? $chargeData['btc'] : null,
                'remaining' => $remaining,
                'txs'=> $txs,
                'transaction_id' => $transaction->getId(),
                'amount' => $transaction->getAmount(),
                'date' => $transaction->getDate(),
                'now' => $now->format('Y-m-d H:i:s')
            ]
        ]);

    }
}
