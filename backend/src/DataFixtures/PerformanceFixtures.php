<?php

namespace App\DataFixtures;

use App\Entity\PerformanceData;
use App\Entity\PerformanceDataUser;
use App\Entity\ReferralConfig;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class PerformanceFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $dataPerformance = (new PerformanceData())
            ->setPercentageMin(0.5)
            ->setPercentageMax(0.9)
            ->setPercentageExchangeLimit(0.1)
            ->setDaysDelayPerformance(10)
            ->setMaxAmountDeposit(100000)
            ->setMinAmountDeposit(50)
            ->setMinimunAmountToReinvest(1000);
        $dataPerformanceUser = (new PerformanceDataUser())
            ->setMinAmount(1)
            ->setMaxAmount(100000)
            ->setPercentage(0.9);
        $dataPerformance->addPerformanceDataUser($dataPerformanceUser);
        $manager->persist($dataPerformance);
        $manager->flush();
    }
}
