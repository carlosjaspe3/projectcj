<?php

namespace App\Command;

use App\Entity\BasePerformance;
use App\Entity\Contact;
use App\Entity\PerformanceData;
use App\Entity\PerformanceDataUser;
use App\Entity\Referral;
use App\Entity\ReferralConfig;
use App\Entity\Transaction;
use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Psr\Log\LoggerInterface;
use App\Entity\User;
use Symfony\Component\Validator\Constraints\Date;

class PerformanceBase extends Command
{
    private $em;
    private $usr;
    public function __construct(EntityManagerInterface $em, LoggerInterface $loggerInterface)
    {
        parent::__construct();
        $this->em = $em;
        $this->logger = $loggerInterface;
    }
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:performance-base';

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Starting');
        $output->writeln('Starting the performance base');

        /* 
        * Calcular eficiencia de la plataforma
        */

        $performanceData = $this->em->getRepository(PerformanceData::class)->findOneBy([]);
        if (!$performanceData) {
            $output->writeln('Configure performance data');
            return Command::FAILURE;
        };
        $date = (new DateTime());
       
        $output->writeln($date->format('Y-m-d'));
            
            $days=1;

            $date->sub(new DateInterval('P'.$days.'D'));//Extrae un dia a la fecha actual

            $basePerformance = $this->em->getRepository(BasePerformance::class)->findOneBy(['date' => $date]);//Carga el performance
            
            if ($basePerformance) {//Si existe no podemos volver a repartir
                $output->writeln('Ya se calculo el rendimiento de este dia!! ');
                return Command::FAILURE; ####################### DESCOMENTAR
            }

            $value = rand($performanceData->getPercentageMin() * 100000, $performanceData->getPercentageMax() * 100000) / 100000;

            $lastBasePerformance = $this->em
                ->createQueryBuilder()
                ->select('bp')
                ->from(BasePerformance::class, 'bp')
                ->where('bp.percentage > :amount')
                ->setParameter('amount', 0)
                ->orderBy('bp.date', 'DESC')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

            if ($lastBasePerformance) {
                $oldValue = $lastBasePerformance->getPercentage();
                $exchangeLimit = $performanceData->getPercentageExchangeLimit();
                $valueLimitUp = $oldValue + $exchangeLimit;
                $valueLimitDown = $oldValue - $exchangeLimit;
                if ($valueLimitDown < 0) {
                    $valueLimitDown = $oldValue;
                }

                if ($value == $oldValue) {
                    while ($value == $oldValue) {
                        $value = rand($performanceData->getPercentageMin() * 100000, $performanceData->getPercentageMax() * 100000) / 100000;
                    }
                }
                if ($value > $valueLimitUp) {
                    $value = $valueLimitUp;
                }
                if ($value < $valueLimitDown) {
                    $value = $valueLimitDown;
                }
              
            }

            /* Setear en 0 los fines de semana */
            if ($date->format('N') == '6' || $date->format('N') == '7') {
                $output->writeln('Los fines de semana el rendimiento es 0.');
                $value = 0;
            }
            /* Fin setear en 0 los fines de semana */

            $value = round($value,2);
            $basePerformance = (new BasePerformance())
                ->setDate($date)
                ->setPercentage($value);
            $this->em->persist($basePerformance);
            $this->em->flush();
            $output->writeln('SUCCESS BASE');
            $output->writeln('Rentabilidad del fondo: '.($value).'%');

            $output->writeln('');
            $output->writeln('');

            /* Verificacion del rendimiento */
            if ($value == 0) {
                $output->writeln('Rendimiento en 0, no se calcula la eficiencia para los usuarios.');
                return Command::SUCCESS;
            }
            /* Fin Verificacion del rendimiento */

            /* 
            * Calcular eficiencia para los usuarios
            */
            $users = $this
                ->em
                ->createQueryBuilder()
                ->select('u', 't')
                ->from(User::class, 'u')
                ->join('u.transactions', 't', 'WITH', 't.type = :depositType AND t.status = :success AND t.whitdrawal is NULL AND t.date <= :date')
                ->setParameter('depositType', Transaction::TYPE_DEPOSIT)
                ->setParameter('success', 'success')
                ->setParameter('date', date('Y-m-d'))
                ->getQuery()
                ->getResult();
            
                
            foreach ($users as $user) {
            
                $total_buy_user = 0;

                foreach ($user->getTransactions() as $transaction) {
                    
                    $now = new DateTime("now");
                    $dias_transcurridos = $now->diff($transaction->getDate());

                    //Validamos que hayan pasado X dias de acuerdo a la fecha de la transaccion, a fecha de hoy
                    if($dias_transcurridos->days >= $performanceData->getDaysDelayPerformance()){
                        $total_buy_user += $transaction->getAmount();
                    }
                
                }

                $output->writeln("User: ".$user->getEmail()." -> Inversion: ".$total_buy_user);

                if($total_buy_user > 0){

                    $performanceDataUser = $this
                        ->em
                        ->createQueryBuilder()
                        ->select('bc')
                        ->from(PerformanceDataUser::class, 'bc')
                        ->where('bc.minAmount <= :amount')
                        ->andWhere('bc.maxAmount >= :amount')
                        ->setParameter('amount', $total_buy_user)
                        ->getQuery()
                        ->getOneOrNullResult();
                    
                    $amount = $performanceDataUser->getPercentageCal() * $basePerformance->getPercentageCal() * $total_buy_user;
                    
                    $output->writeln("% x Inv: ".($performanceDataUser->getPercentageCal() * 100)."%"." De: ".($basePerformance->getPercentageCal()*100)."%"." Sobre Inv: ".$amount."$");

                    //Guardamos el retorno por inversion del usuario
                    
                    $userPerformance = (new Transaction())
                        ->setType(Transaction::TYPE_PERFORMANCE)
                        ->setBasePerformance($basePerformance)
                        ->setChargeUUID('')
                        ->setTransaction(NULL)
                        ->setStatus('success')
                        ->setDate($date)
                        ->setAmount($amount)
                        ->setUser($user);
                   
                    #$user->setBalance($user->getBalance() + $amount);
                    #$user->setPerformance($user->getPerformance() + $amount);
                    
                    $this->em->persist($userPerformance);
                    $this->em->flush();

                    //Asignamos comisiones de referidos
                    $parents = $user->getParentsLimitedByLevelToBuy();
                    $master_extra_comision = true;
                    $users_ids = [2];//el ID => 2 corresponde al master
                    
                    foreach ($parents as $parentReferral) { 
                        $parent = $parentReferral->getParent();
                        $level = $parentReferral->getLevel();
                        $output->writeln('Level: '.$level.' UserID: '.$parent->getId().' Email: '.$parent->getEmail());
                        
                        $condition = $this->em->getRepository(ReferralConfig::class)->findOneBy(['level' => $level]);
                        
                        //Repartimos comision segun nivel, al usuario en el array no pasa por mas condiciones, recibe directamente
                        $save_comission = false;
                        $data_comission = [];
                        if(in_array($parent->getId(), $users_ids)){
                            $perc = $condition->getPercentage();
                            $comision_recibe = $amount*($perc/100);
                            //Exite en el array, recibie comision
                            $output->writeln('MASTER -> Recibe Comision de '.$perc.'%: '. $parent->getEmail().' de Nivel: '.$level.' De: '.$comision_recibe.'$');
                            //Si el usuario Master validado ya recibio comision, entonces no vuelve a recibir dentro de esta estructura/red
                            $master_extra_comision = false;
                            
                            ### GUARDAMOS COMISION ###
                            $save_comission = true;
                            $data_comission['amount'] = $comision_recibe;
                            
                        }else{
                            //Aqui recibe comision los usuarios que no tienen privilegio, su respectiva comision por nivel

                            ### GUARDAMOS COMISION ###

                            #Aqui debemos validar que el usuario que recibe comision, cumpla con uno de los siguientes criterios
                            # Tomar en cuenta que como minimo 2 lineas de referidos deben satisfacer la evaluacion
                            # 1. COMPRAS NIVEL 1: Debemos sacar el total de inversion de sus referidos en nivel 1, de forma individual,
                            # de donde solo usaremos el 50% de lo que ha invertido cada uno, y a la vez no puede superar el 50% necesario
                            # 2. VOLUMEN EQUIPO: Debemos sacar el total de inversion de toda la linea de cada referido tomando en cuenta el nivel 1
                            # hasta el nivel 10, donde usaremos el 50% del total invertido , y a la vez no puede superar el 50% necesario
                            # 
                            # Una vez tengamos los valores liberamos hasta el nivel que satisfaga.

                            if($level == 1 || $level == 2){
                                
                                if($parent->getContributeBuyForLevel($level, $condition->getMinAmountDeposited(), $performanceData->getMinAmountDeposit(), $output)){ #linea comentada el 25-02-2021
                                //if($parent->getContributeBuyForLevelOneUser($level, $condition->getMinAmountDeposited(), $performanceData->getMinAmountDeposit(), $output)){  #Nuevo criterio creado el 25-02-2021
                                    $perc = $condition->getPercentage();
                                    $comision_recibe = $amount*($perc/100);
                                    //Exite en el array, recibie comision
                                    $output->writeln('Recibe Comision de '.$perc.'%: '. $parent->getEmail().' de Nivel: '.$level.' De: '.$comision_recibe.'$');
                                    //Si el usuario Master validado ya recibio comision, entonces no vuelve a recibir dentro de esta estructura/red
                                   
                                    ### GUARDAMOS COMISION ###
                                    $save_comission = true;
                                    $data_comission['amount'] = $comision_recibe;
                                }

                            }else{
                                
                                if($parent->getContributeBuyForLevel($level, $condition->getMinAmountDeposited(), $performanceData->getMinAmountDeposit(), $output)
                                    || $parent->getContributeBuyVolTeam($level, $condition->getMinAmountDepositedReferrals(), $performanceData->getMinAmountDeposit(), $output)){

                                    $perc = $condition->getPercentage();
                                    $comision_recibe = $amount*($perc/100);
                                    //Exite en el array, recibie comision
                                    $output->writeln('Recibe Comision de '.$perc.'%: '. $parent->getEmail().' de Nivel: '.$level.' De: '.$comision_recibe.'$');
                                    //Si el usuario Master validado ya recibio comision, entonces no vuelve a recibir dentro de esta estructura/red
                                   
                                    ### GUARDAMOS COMISION ###
                                    $save_comission = true;
                                    $data_comission['amount'] = $comision_recibe;

                                }
            
                            }

                        }

                        if($save_comission == true && !empty($data_comission)){
                            $transactionReferral = (new Transaction())
                                ->setType(Transaction::TYPE_REFERRAL)
                                ->setAmount($data_comission['amount'])
                                ->setBasePerformance($basePerformance)
                                ->setDate($date)
                                ->setStatus('success')
                                ->setUser($parent)
                                ->setChargeUUID('')
                                ->setRemitter($user)
                                ->setLevel($level);
                            
                            $this
                                ->em
                                ->persist($transactionReferral);

                            $referral = $this
                                    ->em
                                    ->getRepository(Referral::class)
                                    ->findOneBy(['parent' => $parent, 'children' => $user]);
                            $referral->setAmount($referral->getAmount() + $data_comission['amount']);

                            /* DEBEMO REVISAR CODIGO DE REINVERSION
                            if ($parent->getCompoundInterest() && $parent->getBalance() >= $performanceData->getMinimunAmountToReinvest()) {
                                $reinvestTransaction = (new Transaction())
                                    ->setStatus("success")
                                    ->setType(Transaction::TYPE_DEPOSIT)
                                    ->setAmount($performanceData->getMinimunAmountToReinvest())
                                    ->setDate(new DateTime())
                                    ->setUser($parent)
                                    ->setChargeUUID('');

                                
                                $this->em->persist($reinvestTransaction);

                                $parent->setBalance($parent->getBalance() - $performanceData->getMinimunAmountToReinvest());
                                $output->writeln('reinvest');

                            }
                            */
                            #$parent->setReferred($parent->getReferred() + $data_comission['amount']);
                            $this->em->flush();
                        }
                    }

                    //Damos comision a master para este ciclo 1 vez

                    if($master_extra_comision){
                        $userMaster = $this->em
                                ->createQueryBuilder()
                                ->select('u')
                                ->from(User::class, 'u')
                                ->where('u.id = :id')
                                ->setParameter('id', 2)
                                ->getQuery()
                                ->getOneOrNullResult();
                        
                        #$output->writeln('#######    '.$userMaster->getId().'    #######');
                        
                        $percMaster = 1;
                        $comision_recibe_master = round(($amount*($percMaster/100)),16);
                        $output->writeln('MASTER -> Recibe Comision EXTRA de '.$percMaster.'%: Red Mayor de Nivel: >10 De: '.$comision_recibe_master.'$');
                        ### GUARDAMOS COMISION ###

                        $transactionReferral = (new Transaction())
                            ->setType(Transaction::TYPE_REFERRAL)
                            ->setAmount($comision_recibe_master)
                            ->setDate($date)
                            ->setStatus('success')
                            ->setUser($userMaster)
                            ->setRemitter($user)
                            ->setChargeUUID('');
                        
                        $this
                            ->em
                            ->persist($transactionReferral);

                        // $referral = $this
                        //     ->em
                        //     ->getRepository(Referral::class)
                        //     ->findOneBy(['parent' => $master, 'children' => $user]);

                        #$userMaster->setBalance($userMaster->getBalance() + $comision_recibe_master);
                        $this->em->flush();
                    }
                }
                $output->writeln('');
                $output->writeln('');   
            }

            $dateNow = (new DateTime());
            if ($dateNow->format('N') == '1'){
           
                $dateNow->sub(new DateInterval('P2D'));
                $basePerformance = (new BasePerformance())
                    ->setDate($dateNow)//Sabado
                    ->setPercentage(0);
                $this->em->persist($basePerformance);
                $this->em->flush();

                $dateNow = (new DateTime());
                $dateNow->sub(new DateInterval('P1D'));
                $basePerformance = (new BasePerformance())
                    ->setDate($dateNow)//Domingo
                    ->setPercentage(0);
                $this->em->persist($basePerformance);
                $this->em->flush();

                $output->writeln('SUCCESS BASE');
            }

        //Finalmente recorremos todos los usuarios y le actualizamos sus saldos
        
        // $users = $this->em
        //     ->createQueryBuilder()
        //     ->select('u')
        //     ->from(User::class, 'u')
        //     ->getQuery()
        //     ->getResult();
        // foreach($users as $usr){
            
        //     $usr->setReferred();
        //     $usr->setPerformance();
        //     $usr->setBalance();
            
        //     $this->em->flush();
        // }
          
        $output->writeln('Finished');
        $output->writeln('Finished performance task');
        return Command::SUCCESS;
    }

}
