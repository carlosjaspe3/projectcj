<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class UpdateBalance extends Command
{
    protected static $defaultName = 'app:update-balance';

    private $em;
 
    public function __construct(EntityManagerInterface $em, LoggerInterface $loggerInterface)
    {
        parent::__construct();
        $this->em = $em;
        $this->logger = $loggerInterface;
    }

    
    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        
        $users = $this->em
            ->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->getQuery()
            ->getResult();
        foreach($users as $usr){
            
            #if($usr->getId() == 80){
                $usr->setReferred();
                $usr->setPerformance();
                $usr->setBalance();
                $usr->setDeposited();
                
                $this->em->flush();
            #}
        }

        return Command::SUCCESS;
    }
}
