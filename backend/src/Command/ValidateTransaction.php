<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use App\Entity\Transaction;
use App\Entity\User;
use App\Entity\Referral;
use App\Util\Mailer;
use DateInterval;
use DateTime;


class ValidateTransaction extends Command
{
    private $em;
    private $mailer;
 
    public function __construct(EntityManagerInterface $em, LoggerInterface $loggerInterface, Mailer $mailer)
    {
        parent::__construct();
        $this->em = $em;
        $this->logger = $loggerInterface;
        $this->mailer = $mailer;
    }

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:validate-transaction';

    protected function configure()
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        
        // pending: { title: "Pendiente" },
        // confirming: {title: "Confirmando"},
        // success: { title: "Aprobado" },
        // expired: { title: "Expirado" },
        // incomplete: {title: "Incompleto"}

        //Cargo todas las transacciones existentes del tipo deposit y distintas a success

        $transactions = $this->em
                ->createQueryBuilder()
                ->select('t')
                ->from(Transaction::class, 't')
                ->where('t.type = :type')
                ->andWhere('t.status != :status')
                ->andWhere('t.status != :status_expired')
                ->setParameter('type', 'deposit')
                ->setParameter('status', 'success')
                ->setParameter('status_expired', 'expired')
                ->getQuery()
                ->getResult();
        
        foreach($transactions as $transaction){

            $wallet = $transaction->getWalletBtcBlockio();
            $btc = $transaction->getBtc();//BTC solicitados
            $status = $transaction->getStatus();
            #var_dump($transaction->getId());
            $commandString = "/usr/bin/python3 {$_SERVER['DIR_FILE_BOT']}/valid_depositos.py {$wallet}";
            
            $salida = json_decode(shell_exec($commandString),true);
            
            $balance_confirmado = $salida['balance']['available_balance'];
            $pendiente_confirmar = $salida['balance']['pending_received_balance'];
            
            $msg = [];
            $txs = $salida['txs'];
            if(count($txs) > 0){
                //Guardamos los ID de las transacciones hechas para esta wallet, asi como las confirmaciones
                $msg['txs'] = $txs;
            }
            if($balance_confirmado == $btc ){
                //Marcamos la trasaccion como success
                $transaction->setStatus('success');
                $output->writeln('BTC: '.$btc.' Status: success');
                
                // Envio de email a parentUser
                $mailParentUser = $this->sendMailParentUser(
                    $transaction->getUser()
                );

            }elseif($balance_confirmado > $btc ){
                //Marcamos la trasaccion como success ?? posterioremnte se marcara de otra forma por ser mayor
                $transaction->setStatus('success');
                $output->writeln('BTC: '.$btc.' Status: success');
            }elseif($pendiente_confirmar == $btc){
                //Marcamos la transaccion como confirming
                $transaction->setStatus('confirming');
                $output->writeln('BTC: '.$btc.' Status: confirming');
            }elseif($pendiente_confirmar > $btc){
                //Marcamos la transaccion como confirming ?? posterioremnte se marcara de otra forma por ser mayor
                $transaction->setStatus('confirming');
                $output->writeln('BTC: '.$btc.' Status: confirming');
            }elseif( 
                ($pendiente_confirmar > 0 && $pendiente_confirmar < $btc)
                || ($balance_confirmado > 0 && $balance_confirmado < $btc) 
            ){
                //Marcamos la transaccion como incomplete
                $transaction->setStatus('incomplete');
                $restante = ($btc - $pendiente_confirmar - $balance_confirmado);
                $restante = number_format($restante, 8, '.', '');
                $msg['remaining'] = $restante;
                $output->writeln('BTC: '.$btc.' Status: incomplete'.' Restan: '.$restante);

            }elseif(($pendiente_confirmar == 0 || $balance_confirmado == 0) && $status == 'pending'){
                //Validamos la fecha de expiracion para cancelar la transaccion y ponerla expired
                
                $date = new DateTime($transaction->getCharge()->getExpiresAt());
                $now = new DateTime("now");
                
                if($now > $date){
                    $output->writeln('BTC: '.$btc.' Status: expired');
                    $transaction->setStatus('expired');
                }
                
            }
        
            //Guardamos los ID de las transacciones hechas para esta wallet, asi como las confirmaciones
            $transaction->setMessage($msg);
            $this->em->persist($transaction);
            $this->em->flush();    

            $transaction->getUser()->setDeposited();
            $this->em->persist($transaction);
            $this->em->flush();
        }

        // $users = $this->em
        //     ->createQueryBuilder()
        //     ->select('u')
        //     ->from(User::class, 'u')
        //     ->getQuery()
        //     ->getResult();
        // foreach($users as $usr){
        //     $usr->setReferred();
        //     $usr->setPerformance();
        //     $usr->setBalance();
            
        //     $this->em->flush();
        // }
        

        return Command::SUCCESS;
    }

    /**
     * @Route("/search/parents", name="search-parent-user", methods="GET")
     */
    private function sendMailParentUser($user)
    {
        // Validamos que es la primera transaccion del usuario
        $transactions = $this->em->getRepository(Transaction::class)->findBy([
            'user' => $user,
            'type' => 'deposit',
            'status' => 'success'
        ]);

        if (count($transactions) > 0) {
            return Null;
        }

        // Ubicamos su padre si es la primera transaccion
        $parentUser = $this->em->getRepository(Referral::class)->findOneBy([
            'children' => $user,
            'level' => 1,
        ]);
        $parentUser = $parentUser->getParent();
        
        // Le enviamos un correo al padre
        $this->mailer->send('Referido de primer nivel realiza inversión', 
                        'email/congratulations.html.twig', 
                        [
                            'parentUser' => $parentUser,
                            'user' => $user
                        ], 
                        $parentUser->getEmail());

        return true;
    }
}
