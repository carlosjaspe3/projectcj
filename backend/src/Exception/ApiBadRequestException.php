<?php

namespace App\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @author Ben Ramsey <ben@benramsey.com>
 */
class ApiBadRequestException extends HttpException
{
    /**
     * @param string     $message  The internal exception message
     * @param \Exception $previous The previous exception
     * @param int        $code     The internal exception code
     * @param array      $headers
     */
    public function __construct($message = null, \Exception $previous = null, int $code = 400, array $headers = [])
    {
        $this->statusCode = $code;
        $this->headers = $headers;
        $this->message = $message;
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }
}