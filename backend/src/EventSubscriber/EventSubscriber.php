<?php
namespace App\EventSubscriber;

use App\Controller\TokenAuthenticatedController;
use App\Exception\ApiBadRequestException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Security\Core\Security;

class EventSubscriber implements EventSubscriberInterface
{
    private $security;

    public function __construct(Security $security) {
        $this->security = $security;
    }
    public function onKernelController(ControllerEvent $event)
    {
        if (!$event->isMasterRequest()) {
            // don't do anything if it's not the master request
            return;
        }
        if(preg_match("/^(\/api\/users\/[1-9]+)$/", $event->getRequest()->getPathInfo()) && $_SERVER['REQUEST_METHOD'] == 'PUT' && $this->security->getUser() && $this->security->getUser()->getTwoAF()){
            $g = new \Sonata\GoogleAuthenticator\GoogleAuthenticator();
            if (!$event->getRequest()->get('google-code')) throw new ApiBadRequestException(['code' => 'Còdigo no vàlido']);
            if (!$g->checkCode($this->security->getUser()->getHash2FA(), $event->getRequest()->get('google-code'))) throw new ApiBadRequestException(['code' => 'Còdigo no vàlido']);
        }
        if(preg_match("/^(\/api\/change-passwords)$/", $event->getRequest()->getPathInfo()) && $_SERVER['REQUEST_METHOD'] == 'POST' && $this->security->getUser() && $this->security->getUser()->getTwoAF()){
            $g = new \Sonata\GoogleAuthenticator\GoogleAuthenticator();
            if (!$event->getRequest()->get('google-code')) throw new ApiBadRequestException(['code' => 'Còdigo no vàlido']);
            if (!$g->checkCode($this->security->getUser()->getHash2FA(), $event->getRequest()->get('google-code'))) throw new ApiBadRequestException(['code' => 'Còdigo no vàlido']);
        }

    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }
}