<?php

namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use App\Exception\ApiBadRequestException;
use App\Exception\ApiBadRequestInfoException;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();
        if ($exception instanceof ApiBadRequestException) {
            $response = new JsonResponse(['errors' =>  $exception->getMessage()]);
            $response->setStatusCode($exception->getStatusCode());
            $event->setResponse($response);
        }
        if ($exception instanceof ApiBadRequestInfoException) {
            $response = new JsonResponse([ 'info' => $exception->getMessage()]);
            $response->setStatusCode($exception->getStatusCode());
            $event->setResponse($response);
        }
    }
}