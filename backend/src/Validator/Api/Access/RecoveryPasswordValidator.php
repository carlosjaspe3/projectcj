<?php

namespace App\Validator\Api\Access;

use App\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;
use App\Util\Validator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Exception\ApiBadRequestException;
use App\Exception\ApiBadRequestInfoException;

class RecoveryPasswordValidator extends Validator
{
    protected $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    public function validateInput($input)
    {
        $constraint = [new Assert\Collection([
            'fields' => ['email' => [new Assert\Email(['message' => 'El campo debe contener un email válido.']),new Assert\Callback(['callback'=>[self::class, 'validateEmail'],'payload'=>$this->em])]],
            'missingFieldsMessage' => 'El campo es requerido.'
        ])];
        
        parent::validateRequest($input, $constraint);
    }
    public  function validateEmail($object, ExecutionContextInterface $context, $payload)
    {
        if (!$object) return;
        $user = $payload->getRepository(User::class)->findOneByEmail($object);
        if (!$user){
            $context->buildViolation('No existe una cuenta asociada al email.')
            ->addViolation();
        }
        
    }
}
