<?php

namespace App\Validator\Api\Access;

use App\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;
use App\Util\Validator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Exception\ApiBadRequestException;
use App\Exception\ApiBadRequestInfoException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LoginValidator extends Validator
{
    protected $em;
    protected $encoder;

    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder) {
        $this->em = $em;
        $this->encoder = $encoder;
    }

    public function validate($input) 
    {
        $email = isset($input['email']) ? $input['email'] : '';
        $user = $this->em->getRepository(User::class)->findOneByEmail($email);
        $payload = [$user,$this->encoder];
        $constraint = [new Assert\Collection([
            'fields' => [
                'email' => [new Assert\NotBlank(), new Assert\Email(['message' => 'Debe contener una dirección de email válida.']), new Assert\Callback(['callback'=>[self::class, 'validateEmail'],'payload'=>$payload])],
                // 'fcmToken' => [new Assert\NotBlank()],
                'password' => [new Assert\NotBlank(), new Assert\Callback(['callback'=>[self::class, 'validatePassword'],'payload'=>$payload])],
            ],
            'missingFieldsMessage' => 'El campo es requerido.'
        ]), new Assert\Callback(['callback'=>[self::class, 'validateStatus'],'payload'=>$payload])];
        
        parent::validateRequest($input, $constraint);
    }


    public  function validateEmail($object, ExecutionContextInterface $context, $payload)
    {
        if (!$payload[0]) {
            $context->buildViolation('No existe una cuenta asociada al email.')
                ->addViolation();        
            }
    }


    public  function validatePassword($object, ExecutionContextInterface $context, $payload)
    {
        if ($payload[0] && !$payload[1]->isPasswordValid($payload[0], $object)) {
            $context->buildViolation('Contraseña invalida.')
                ->addViolation();        
            }
    }

    public  function validateStatus($object, ExecutionContextInterface $context, $payload)
    {
        if ($payload[0] && $payload[0]->getConfirmationToken()) throw new ApiBadRequestInfoException(['Debe verificar la cuenta ingresando al correo.']);
        if ($payload[0] && !$payload[0]->isEnabled()) throw new ApiBadRequestInfoException(['Usuario deshabilitado.']);
    }

}
