<?php

namespace App\Validator\Api\Access;

use App\Entity\User;
use App\Util\Validator;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class RecoveryPasswordVerificationValidator extends Validator
{
    protected $em;
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    public function validate($input)
    {
        $constraint = new Assert\Collection([
            'fields' => ['token' => [new Assert\NotBlank(), new Assert\Callback(['callback'=>[self::class, 'validateToken'],'payload'=>$this->em]), new Assert\NotBlank()],
            'password' => [new Assert\NotBlank()],],
            'missingFieldsMessage' => 'El campo es requerido.'
        ]);
        
        parent::validateRequest($input, $constraint);
    }

    public  function validateToken($object, ExecutionContextInterface $context, $payload)
    {
        $user = $payload->getRepository(User::class)->findOneByResetPasswordToken($object);
        if (!$user) {
            $context->buildViolation('Token no valido.')
            ->addViolation();
        }
    }
}
