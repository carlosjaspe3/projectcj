<?php

namespace App\Entity;

use App\Repository\BuyRepository;
use App\Traits\TimestampableEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\DateFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ApiResource(
 *     attributes={"order"={"id": "DESC"},"normalization_context"={"groups"={"transaction"}}},
 *     itemOperations={
 *         "get",
 *     },
 *     collectionOperations={
 *         "get",
 *     },
 * )
 * @ApiFilter(DateFilter::class, properties={"date"})
 * @ApiFilter(SearchFilter::class, properties={"status": "partial", "type":"partial", "date":"partial"}) 
 * @ORM\HasLifecycleCallbacks
 */
class Transaction
{
    use TimestampableEntity;


    const TYPE_DEPOSIT = "deposit";
    const TYPE_WITHDRAWAL = "withdrawal";
    const TYPE_REFERRAL = "referral";
    const TYPE_PERFORMANCE = "performance";
    const TYPE_BUY = "buy";

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"performance-user"})
     * @Groups({"transaction"})
     */
    private $id;

    /**
     * @ORM\Column(type="date", nullable=true)
     * @Groups({"performance-user"})
     * @Groups({"transaction"})
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"performance-user"})
     * @Groups({"transaction"})
     */
    private $status;

    /**
     * @ORM\Column(type="decimal", precision=65, scale=5, nullable=true)
     * @Groups({"performance-user"})
     * @Groups({"transaction"})
     */
    private $amount;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="transactions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"performance-user"})
     * @Groups({"transaction"})
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     */
    private $remitter;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"performance-user"})
     * @Groups({"transaction"})
     */
    private $level;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $chargeUUID;

    /**
     * @ORM\ManyToOne(targetEntity=Transaction::class, inversedBy="transactions")
     * @Groups({"performance-user"})
     * @Groups({"transaction"})
     */
    private $transaction;

    /**
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="transaction")
     */
    private $transactions;

    /**
     * @ORM\ManyToOne(targetEntity=BasePerformance::class, inversedBy="transactions")
     * @Groups({"performance-user"})
     * @Groups({"transaction"})
     */
    private $basePerformance;

    /**
     * @ORM\ManyToOne(targetEntity=Charge::class, inversedBy="transactions")
     */
    private $charge;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"transaction"})
     */
    private $whitdrawal;

    /**
     * @ORM\Column(type="json", nullable=true)
     * @Groups({"transaction"})
     */
    private $message = [];

    /**
     * @ORM\Column(type="decimal", precision=14, scale=10, nullable=true)
     */
    private $btc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $walletBtcBlockio;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $walletBtcBlockioOld;


    public function __construct()
    {
        $this->transactions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getAmount(?int $decimales = 2)
    {
        return round($this->amount, $decimales);
    }

    public function setAmount(?string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getRemitter(): ?User
    {
        return $this->remitter;
    }

    public function setRemitter(?User $remitter): self
    {
        $this->remitter = $remitter;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(?int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getTransaction(): ?self
    {
        return $this->transaction;
    }

    public function setTransaction(?self $transaction): self
    {
        $this->transaction = $transaction;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(self $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setTransaction($this);
        }

        return $this;
    }

    public function removeTransaction(self $transaction): self
    {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
            // set the owning side to null (unless already changed)
            if ($transaction->getTransaction() === $this) {
                $transaction->setTransaction(null);
            }
        }

        return $this;
    }

    public function getBasePerformance(): ?BasePerformance
    {
        return $this->basePerformance;
    }

    public function setBasePerformance(?BasePerformance $basePerformance): self
    {
        $this->basePerformance = $basePerformance;

        return $this;
    }

    public function getCharge(): ?Charge
    {
        return $this->charge;
    }

    public function setCharge(?Charge $charge): self
    {
        $this->charge = $charge;

        return $this;
    }

    public function getWhitdrawal(): ?bool
    {
        return $this->whitdrawal;
    }

    public function setWhitdrawal(?bool $whitdrawal): self
    {
        $this->whitdrawal = $whitdrawal;

        return $this;
    }

    public function getMessage(): ?array
    {
        return $this->message;
    }

    public function getMessageString(): ?string
    {
        //dd(json_decode($this->message));
        return json_encode($this->message);
        //return json_decode($this->message);
    }

    public function setMessage(?array $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getChargeUUID(): ?string
    {
        return $this->chargeUUID;
    }

    public function setChargeUUID(string $chargeUUID): self
    {
        $this->chargeUUID = $chargeUUID;

        return $this;
    }

    public function __toString(): ?string
    {
        return $this->type .' '. $this->id;
    }

    public function getBtc(): ?string
    {
        return $this->btc;
    }

    public function setBtc(?string $btc): self
    {
        $this->btc = $btc;

        return $this;
    }

    public function getWalletBtcBlockio(): ?string
    {
        return $this->walletBtcBlockio;
    }

    public function setWalletBtcBlockio(?string $walletBtcBlockio): self
    {
        $this->walletBtcBlockio = $walletBtcBlockio;

        return $this;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    public function getWalletBtcBlockioOld(): ?string
    {
        return $this->walletBtcBlockioOld;
    }

    public function setWalletBtcBlockioOld(?string $walletBtcBlockioOld): self
    {
        $this->walletBtcBlockioOld = $walletBtcBlockioOld;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
    */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));    
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
    
}
