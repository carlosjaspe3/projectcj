<?php

namespace App\Entity;

use App\Repository\PerformanceDataRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PerformanceDataRepository::class)
 * @ORM\HasLifecycleCallbacks
 */
class PerformanceData
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $percentageMin;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $percentageMax;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=5)
     */
    private $percentageExchangeLimit;

    /**
     * @ORM\OneToMany(targetEntity=PerformanceDataUser::class, mappedBy="performanceData", orphanRemoval=true, cascade={"persist"})
     */
    private $performanceDataUser;

    /**
     * @ORM\Column(type="decimal", precision=65, scale=5, nullable=true)
     */
    private $minimunAmountToReinvest;

    /**
     * @ORM\Column(type="decimal", precision=65, scale=5, nullable=true)
     */
    private $minAmountDeposit;

    /**
     * @ORM\Column(type="decimal", precision=65, scale=5, nullable=true)
     */
    private $maxAmountDeposit;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $daysDelayPerformance;

    /**
     * @ORM\Column(type="decimal", precision=65, scale=5, nullable=true)
     */
    private $minAmountWhitdrawal;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $daysDelayWhitdrawal;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->performanceDataUser = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPercentageMin(): ?string
    {
        return $this->percentageMin;
    }

    public function setPercentageMin(string $percentageMin): self
    {
        $this->percentageMin = $percentageMin;

        return $this;
    }

    public function getPercentageMax(): ?string
    {
        return $this->percentageMax;
    }

    public function setPercentageMax(string $percentageMax): self
    {
        $this->percentageMax = $percentageMax;

        return $this;
    }

    public function getPercentageExchangeLimit(): ?string
    {
        return $this->percentageExchangeLimit;
    }

    public function setPercentageExchangeLimit(string $percentageExchangeLimit): self
    {
        $this->percentageExchangeLimit = $percentageExchangeLimit;

        return $this;
    }

    public function getMinimunAmountToReinvest(): ?string
    {
        return $this->minimunAmountToReinvest;
    }

    public function setMinimunAmountToReinvest(?string $minimunAmountToReinvest): self
    {
        $this->minimunAmountToReinvest = $minimunAmountToReinvest;

        return $this;
    }

    public function getMinAmountDeposit(): ?string
    {
        return $this->minAmountDeposit;
    }

    public function setMinAmountDeposit(?string $minAmountDeposit): self
    {
        $this->minAmountDeposit = $minAmountDeposit;

        return $this;
    }

    public function getMaxAmountDeposit(): ?string
    {
        return $this->maxAmountDeposit;
    }

    public function setMaxAmountDeposit(?string $maxAmountDeposit): self
    {
        $this->maxAmountDeposit = $maxAmountDeposit;

        return $this;
    }

    public function getDaysDelayPerformance(): ?int
    {
        return $this->daysDelayPerformance;
    }

    public function setDaysDelayPerformance(?int $daysDelayPerformance): self
    {
        $this->daysDelayPerformance = $daysDelayPerformance;

        return $this;
    }

    /**
     * @return Collection|PerformanceDataUser[]
     */
    public function getPerformanceDataUser(): Collection
    {
        return $this->performanceDataUser;
    }

    public function addPerformanceDataUser(PerformanceDataUser $performanceDataUser): self
    {
        if (!$this->performanceDataUser->contains($performanceDataUser)) {
            $this->performanceDataUser[] = $performanceDataUser;
            $performanceDataUser->setPerformanceData($this);
        }

        return $this;
    }

    public function removePerformanceDataUser(PerformanceDataUser $performanceDataUser): self
    {
        if ($this->performanceDataUser->contains($performanceDataUser)) {
            $this->performanceDataUser->removeElement($performanceDataUser);
            // set the owning side to null (unless already changed)
            if ($performanceDataUser->getPerformanceData() === $this) {
                $performanceDataUser->setPerformanceData(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return "rendimiento";
    }

    public function getMinAmountWhitdrawal(): ?string
    {
        return $this->minAmountWhitdrawal;
    }

    public function setMinAmountWhitdrawal(?string $minAmountWhitdrawal): self
    {
        $this->minAmountWhitdrawal = $minAmountWhitdrawal;

        return $this;
    }

    public function getDaysDelayWhitdrawal(): ?int
    {
        return $this->daysDelayWhitdrawal;
    }

    public function setDaysDelayWhitdrawal(?int $daysDelayWhitdrawal): self
    {
        $this->daysDelayWhitdrawal = $daysDelayWhitdrawal;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
    */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));    
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }
}
