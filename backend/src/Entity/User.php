<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false, hardDelete=true)
 * @ApiResource(
 *     normalizationContext={"groups"={"user"}},
 *     denormalizationContext={"groups"={"write-user"}},
 *     itemOperations={
 *         "get",
 *         "put"={"security"="is_granted('ROLE_USER') and object == user"},
 *     },
 * )
 * @UniqueEntity("email")
 * @ORM\HasLifecycleCallbacks
 */
class User implements UserInterface, EquatableInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"login"})
     * @Groups({"user"})
     * @Groups({"tree"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"login"})
     * @Groups({"user"})
     * @Groups({"tree"})
     */
    private $email;

    /**
     * @ORM\Column(type="array")
     * @Groups({"login"})
     * @Groups({"user"})
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"login"})
     * @Groups({"user"})
     * @Groups({"write-user"})
     */
    private $enabled;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $confirmationToken;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $resetPasswordToken;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"login"})
     * @Groups({"user"})
     * @Groups({"write-user"})
     * @Groups({"tree"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"login"})
     * @Groups({"user"})
     * @Groups({"write-user"})
     * @Groups({"tree"})
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"login"})
     * @Groups({"user"})
     * @Groups({"write-user"})
     * @Groups({"tree"})
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"login"})
     * @Groups({"user"})
     * @Groups({"write-user"})
     * @Groups({"tree"})
     */
    private $address;

    private $plainPassword;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"login"})
     * @Groups({"user"})
     * @Groups({"write-user"})
     */
    private $antiPhishingPassword;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"login"})
     * @Groups({"user"})
     */
    private $twoAF;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $hash2FA;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"login"})
     * @Groups({"user"})
     * @Groups({"write-user"})
     */
    private $access2Fa;

    /**
     * @ORM\Column(type="decimal", precision=65, scale=5, nullable=true)
     * @Groups({"login"})
     * @Groups({"user"})
     * @Groups({"tree"})
     */
    private $balance;

    /**
     * @ORM\OneToMany(targetEntity=Transaction::class, mappedBy="user")
     */
    private $transactions;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"login"})
     * @Groups({"user"})
     * @Groups({"write-user"})
     */
    private $compoundInterest;

    /**
     * @ORM\Column(type="decimal", precision=65, scale=5, nullable=true)
     * @Groups({"login"})
     * @Groups({"user"})
     * @Groups({"tree"})
     */
    private $deposited;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"login"})
     * @Groups({"tree"})
     * @Groups({"user"})
     */
    private $referralCode;

    /**
     * @ORM\Column(type="decimal", precision=65, scale=5, nullable=true)
     * @Groups({"login"})
     * @Groups({"user"})
     * @Groups({"tree"})
     */
    private $referred;

    /**
     * @Groups({"login"})
     * @Groups({"user"})
     */
    private $levelReferral;

    /**
     * @ORM\OneToMany(targetEntity=Referral::class, mappedBy="parent", orphanRemoval=true)
     */
    private $childrens;

    /**
     * @ORM\OneToMany(targetEntity=Referral::class, mappedBy="children")
     */
    private $parents;

    /**
     * @ORM\Column(type="decimal", precision=65, scale=5, nullable=true)
     * @Groups({"tree"})
     */
    private $performance;

    /**
     * @ORM\OneToMany(targetEntity=Charge::class, mappedBy="user")
     * @ApiSubresource()
     */
    private $charges;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $walletBtcBlockio;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDeleted;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $tour;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $locked;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hashLocked;


    public function __construct() {
        $this->enabled = true;
        $this->transactions = new ArrayCollection();
        $this->childrens = new ArrayCollection();
        $this->parents = new ArrayCollection();
        $this->charges = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(?string $confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;

        return $this;
    }

    public function getResetPasswordToken(): ?string
    {
        return $this->resetPasswordToken;
    }

    public function setResetPasswordToken(?string $resetPasswordToken): self
    {
        $this->resetPasswordToken = $resetPasswordToken;

        return $this;
    }
    public function isEqualTo(UserInterface $user)
    {
        if (!$this->enabled) {
            return false;
        }
        if ($user->getPassword() != $this->getPassword() || $user->getUsername() != $this->getEmail()){
            return false;
        }
        else {
            return true;
        }
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(?string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function __toString()
    {
        return $this->email;
    }

    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function getAntiPhishingPassword(): ?string
    {
        return $this->antiPhishingPassword;
    }

    public function setAntiPhishingPassword(?string $antiPhishingPassword): self
    {
        $this->antiPhishingPassword = $antiPhishingPassword;

        return $this;
    }

    public function getTwoAF(): ?bool
    {
        return $this->twoAF == 0 ? NULL: true;
    }

    public function setTwoAF(?bool $twoAF): self
    {
        $this->twoAF = $twoAF;

        return $this;
    }

    public function getHash2FA(): ?string
    {
        return $this->hash2FA;
    }

    public function setHash2FA(?string $hash2FA): self
    {
        $this->hash2FA = $hash2FA;

        return $this;
    }

    public function getAccess2Fa(): ?bool
    {
        return $this->access2Fa == 0 ? NULL : true;
    }

    public function setAccess2Fa(?bool $access2Fa): self
    {
        $this->access2Fa = $access2Fa;

        return $this;
    }

    public function getBalance(): ?string
    {
        return $this->balance;
    }

    public function setBalance()
    {

        $transactions = $this->transactions;
        $balance = 0;
        $entrada = 0;
        $salida = 0;

        $type = ['performance', 'referral', 'withdrawal', 'buy'];

        foreach ($transactions as $transaction) {
           
            if (in_array($transaction->getType(), $type) 
                    && $transaction->getStatus() == 'success' 
                    && $transaction->getDeletedAt() == null) {
                
                if($transaction->gettype() == 'withdrawal' ||
                    $transaction->gettype() == 'buy'){

                    $salida += $transaction->getAmount(5); 

                }else{

                    $entrada += $transaction->getAmount(5);

                }
            }
        }
        $balance = $entrada - $salida;

        $this->balance = $balance;

        return $this;
    }

    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transaction $t): self
    {
        if (!$this->transactions->contains($t)) {
            $this->transactions[] = $t;
            $t->setUser($this);
        }

        return $this;
    }

    public function removeTransaction(Transaction $t): self
    {
        if ($this->transactions->contains($t)) {
            $this->transactions->removeElement($t);
            // set the owning side to null (unless already changed)
            if ($t->getUser() === $this) {
                $t->setUser(null);
            }
        }

        return $this;
    }

    public function getCompoundInterest(): ?bool
    {
        return $this->compoundInterest;
    }

    public function setCompoundInterest(?bool $compoundInterest): self
    {
        $this->compoundInterest = $compoundInterest;

        return $this;
    }

    public function getDeposited(): ?string
    {
        return ($this->deposited == null)? 0 : $this->deposited;
    }

    public function setDeposited()
    {
        $transactions = $this->transactions;
        $deposited = 0;
        
        foreach ($transactions as $transaction) {
            if ($transaction->getType() == 'deposit' && $transaction->getWhitdrawal() != true && 
                $transaction->getStatus() == 'success' && $transaction->getDeletedAt() == null) {

                $deposited += $transaction->getAmount(5);
            }
        }

        $this->deposited = $deposited;
        return $this;
    }

    public function getReferralCode(): ?string
    {
        return $this->referralCode;
    }

    public function setReferralCode(?string $referralCode): self
    {
        $this->referralCode = $referralCode;

        return $this;
    }

    
    public function getTotalAmountOfReferral($level)
    {
        $amount = 0;
        $children = $this->childrens->filter(function ($i) use ($level)
        {
            return $i->getLevel() == $level;
        });
        foreach ($children as $child) {
            foreach ($child->getChildren()->getTransactions() as $transaction) {
                if($transaction->getStatus() == 'success' && $transaction->getType() == Transaction::TYPE_DEPOSIT){
                    $amount = $amount + $transaction->getAmount(5);           
                }            
            }
        }
        return $amount;
    }

    /**
     * Retorna true si solo 1 referido ha invertido el minimo
     * Creado el 25-02-2021
     */
    public function getContributeBuyForLevelOneUser($level, $amount, $min_deposit, $cmd = null){
        $max_for_user = $amount;
        $total = 0;
        
        $childrens = $this->getChildrens();
        if(!is_null($cmd)){
        //Proceso para nivel 1
            $cmd->writeln('Verificando al Usuario Beneficiario por COMPRAS EN TU NIVEL 1: '.$this->getEmail().' ID: '.$this->getId().' <- que pueda recibir comision del nivel: '.$level.' y que el valor resultante sea >= '.$amount);
        }
      
        if($this->getDeposited() < $min_deposit){
            if(!is_null($cmd)){
                $cmd->writeln('NO ESTA HABILITADO ');
            }
            return false;
        }

        //Recorro los hijos en busca solo de los de nivel uno            
        foreach($childrens as $child ){
            $lvl = $child->getLevel();
            if($lvl == 1){
                $userAux = $child->getChildren();
                $aporta = $userAux->getDeposited();
                if($aporta >= $max_for_user){
                    $aporta = $max_for_user;
                }
                $total+=$aporta;
                if(!is_null($cmd)){
                    $cmd->writeln('----------------- USER_ID -----> '.$userAux->getEmail().' - '.' LEVEL ---> '.$lvl.' -> Depositado: '.$userAux->getDeposited().' -> Aporta: '.$aporta);    
                }
            }
        }
        if($total >= $amount){
            if(!is_null($cmd)){
                $cmd->writeln('----------------- TOTAL APORTADO: '. $total. ' HABILITADO: SI');
                $cmd->writeln('');
            }
            return true;
        }else{
            if(!is_null($cmd)){
                $cmd->writeln('----------------- TOTAL APORTADO: '. $total. ' HABILITADO: NO');
                $cmd->writeln('');
            }
            return false;
        }
         
    }

    /**
     * Esta funcion retorna true, cuando 2 usuarios superan con su inversion el requerido de la paltaforma en base al 50%
     */
    public function getContributeBuyForLevel($level, $amount, $min_deposit, $cmd = null){
        $max_for_user = $amount/2; #50%
        $total = 0;
        
        $childrens = $this->getChildrens();
        if(!is_null($cmd)){
        //Proceso para nivel 1
            $cmd->writeln('Verificando al Usuario Beneficiario por COMPRAS EN TU NIVEL 1: '.$this->getEmail().' ID: '.$this->getId().' <- que pueda recibir comision del nivel: '.$level.' y que el valor resultante sea >= '.$amount);
        }
        // if($this->getId() == 2){
        //     return true;
        // }

        if($this->getDeposited() < $min_deposit){
            if(!is_null($cmd)){
                $cmd->writeln('NO ESTA HABILITADO ');
            }
            return false;
        }

        //Recorro los hijos en busca solo de los de nivel uno            
        foreach($childrens as $child ){
            $lvl = $child->getLevel();
            if($lvl == 1){
                $userAux = $child->getChildren();
                $aporta = $userAux->getDeposited()/2;
                if($aporta >= $max_for_user){
                    $aporta = $max_for_user;
                }
                $total+=$aporta;
                if(!is_null($cmd)){
                    $cmd->writeln('----------------- USER_ID -----> '.$userAux->getEmail().' - '.' LEVEL ---> '.$lvl.' -> Depositado: '.$userAux->getDeposited().' -> Aporta: '.$aporta);    
                }
            }
        }
        if($total >= $amount){
            if(!is_null($cmd)){
                $cmd->writeln('----------------- TOTAL APORTADO: '. $total. ' HABILITADO: SI');
                $cmd->writeln('');
            }
            return true;
        }else{
            if(!is_null($cmd)){
                $cmd->writeln('----------------- TOTAL APORTADO: '. $total. ' HABILITADO: NO');
                $cmd->writeln('');
            }
            return false;
        }
         
    }

    public function getContributeBuyVolTeam($level, $amount, $min_deposit, $cmd = null){
        $max_for_user = $amount/2; #50%
        $total = 0;
        
        $childrens = $this->getChildrens();
        if(!is_null($cmd)){
            $cmd->writeln('Verificando al Usuario Beneficiario por VOLUMEN DE EQUIPO '.$this->getEmail().' ID: '.$this->getId().' <- que pueda recibir comision del nivel: '.$level.' y que el valor resultante sea >= '.$amount);
        }
        // if($this->getId() == 2){
        //     return true;
        // }

        if($this->getDeposited() < $min_deposit){
            if(!is_null($cmd)){
                $cmd->writeln('NO ESTA HABILITADO ');
            }
            return false;
        }

        //Recorro los hijos en busca de todos los de niveles para tener el volumen de equipo          
        foreach($childrens as $child ){
            $lvl = $child->getLevel();
            $linea = $child->getChildren();//Usuario nivel 1, representa una linea
            $aporta = $linea->getDeposited();
            $equipo_acumula = 0;
            if($lvl == 1){
                //Tomo todos los hijos de nivel 1, para calcular el volumen interno de esa linea, hijo x hijo
                $equipo = $linea->getChildrens();
               
                foreach($equipo as $integrante){
                    if($integrante->getLevel() < 10){
                        $equipo_acumula += $integrante->getChildren()->getDeposited();
                    }
                }

                $volumen = $aporta+$equipo_acumula;
                $aporta = $volumen/2;
                if($aporta >= $max_for_user){
                    $aporta = $max_for_user;
                }
                $total+=$aporta;
                if(!is_null($cmd)){
                    $cmd->writeln('----------------- USER_ID -----> '.$linea->getEmail().' - '.' LEVEL ---> '.$lvl.' -> Volumen: '.$volumen.' -> Aporta: '.$aporta);    
                }
            }   
        }

        if($total >= $amount){
            if(!is_null($cmd)){
                $cmd->writeln('----------------- TOTAL APORTADO: '. $total. ' HABILITADO: SI');
                $cmd->writeln('');
            }
            return true;
        }else{
            if(!is_null($cmd)){
                $cmd->writeln('----------------- TOTAL APORTADO: '. $total. ' HABILITADO: NO');
                $cmd->writeln('');
            }
            return false;
        }
    }

    public function getLevelReferral()
    {
        return $this->levelReferral;
    }

    public function setReferred()
    {
        $transactions = $this->transactions;
        $referred = 0;
        foreach ($transactions as $transaction) {
            if ($transaction->getType() == 'referral'  && 
                $transaction->getStatus() == 'success' && 
                $transaction->getDeletedAt() == null) {

                $referred += $transaction->getAmount(5);
            }
        }
        $this->referred = $referred;
        return $this;
    }


    public function getReferred()
    {
        return $this->referred == null ? 0 : $this->referred;
    }

    /**
     * @ORM\PostLoad
     */
    public function fetchEntityManager(LifecycleEventArgs $args)
    {
        $this->levelReferral = 0;
        $conditions = $args
            ->getObjectManager()
            ->createQueryBuilder()
            ->select('rc')
            ->from(ReferralConfig::class,'rc')
            ->where('rc.level <= :limitLevel')
            ->setParameter('limitLevel', 10)
            ->orderBy('rc.level','ASC')
            ->getQuery()
            ->getResult();
        foreach ($conditions as $condition) {
            $amountOfReferralCondition = $this->getTotalAmountOfReferral($condition->getLevel()) >= $condition->getMinAmountDepositedReferrals();
            $transactionThatSatisfiesTheCondition = $args
                ->getObjectManager()
                ->createQueryBuilder()
                ->select('t')
                ->from(Transaction::class,'t')
                ->where('t.type = :typeDeposit')
                ->andWhere('t.status = :successStatus')
                ->andWhere('t.amount >= :minAmount')
                ->andWhere('t.user = :user')
                ->setParameters([
                    'user' => $this,
                    'typeDeposit' => Transaction::TYPE_DEPOSIT,
                    'successStatus' => 'success',
                    'minAmount' => $condition->getMinAmountDeposited()
                ])
                ->getQuery()
                ->getResult();
            
            if($amountOfReferralCondition || $transactionThatSatisfiesTheCondition){
                $this->levelReferral = $condition->getLevel();
            }
        }
    }

    /**
     * @return Collection|Referral[]
     */
    public function getChildrens(): Collection
    {
        return $this->childrens;
    }

    public function addChildren(Referral $children): self
    {
        if (!$this->childrens->contains($children)) {
            $this->childrens[] = $children;
            $children->setParent($this);
        }

        return $this;
    }

    public function removeChildren(Referral $children): self
    {
        if ($this->childrens->contains($children)) {
            $this->childrens->removeElement($children);
            // set the owning side to null (unless already changed)
            if ($children->getParent() === $this) {
                $children->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Referral[]
     */
    public function getParents(): Collection
    {
        return $this->parents;
    }
    public function getParentsLimitedByLevelToBuy()
    {
        return $this->parents->filter(function ($i)
        {
            return $i->getLevel() <= 10;
        });
    }

    public function addParent(Referral $parent): self
    {
        if (!$this->parents->contains($parent)) {
            $this->parents[] = $parent;
            $parent->setChildren($this);
        }

        return $this;
    }

    public function removeParent(Referral $parent): self
    {
        if ($this->parents->contains($parent)) {
            $this->parents->removeElement($parent);
            // set the owning side to null (unless already changed)
            if ($parent->getChildren() === $this) {
                $parent->setChildren(null);
            }
        }

        return $this;
    }

    public function getPerformance(): ?string
    {
        return $this->performance == null ? 0 : $this->performance ;
    }

    public function setPerformance()
    {
        $transactions = $this->transactions;
        $performance = 0;
        
        foreach ($transactions as $transaction) {
            if ($transaction->getType() == 'performance'  && 
                $transaction->getStatus() == 'success' && 
                $transaction->getDeletedAt() == null) {

                $performance += $transaction->getAmount(5);
            }
        }
    
        $this->performance = $performance;

        return $this;
    }

    /**
     * @Groups({"tree"})
     */
    public function getChildrenFirstLevel()
    {
        return $this->childrens->filter(function($i){
            return $i->getLevel() == 1 && $i->getChildren()->getIsDeleted() == false;
        })->getValues();
    }

    public function getParentFirstLevel()
    {
        $parent = $this->getParents();
        if ($parent->last()) {
            return $this->getParents()->last()->getParent();
        } else {
            return null;
        }
    }

    /**
     * @return Collection|Charge[]
     */
    public function getCharges(): Collection
    {
        return $this->charges;
    }

    public function addCharge(Charge $charge): self
    {
        if (!$this->charges->contains($charge)) {
            $this->charges[] = $charge;
            $charge->setUser($this);
        }

        return $this;
    }

    public function removeCharge(Charge $charge): self
    {
        if ($this->charges->removeElement($charge)) {
            // set the owning side to null (unless already changed)
            if ($charge->getUser() === $this) {
                $charge->setUser(null);
            }
        }

        return $this;
    }
    public function fullName()
    {
        return $this->name." ".$this->surname;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
    */
    public function updatedTimestamps(): void
    {
        $this->setUpdatedAt(new \DateTime('now'));    
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    public function getWalletBtcBlockio(): ?string
    {
        return $this->walletBtcBlockio;
    }

    public function setWalletBtcBlockio(?string $walletBtcBlockio): self
    {
        $this->walletBtcBlockio = $walletBtcBlockio;

        return $this;
    }

    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(?bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getTour(): ?bool
    {
        return $this->tour;
    }

    public function setTour(?bool $tour): self
    {
        $this->tour = $tour;

        return $this;
    }

    public function getLocked(): ?bool
    {
        return $this->locked;
    }

    public function setLocked(?bool $locked): self
    {
        $this->locked = $locked;

        return $this;
    }

    public function getHashLocked(): ?string
    {
        return $this->hashLocked;
    }

    public function setHashLocked(?string $hashLocked): self
    {
        $this->hashLocked = $hashLocked;

        return $this;
    }
}