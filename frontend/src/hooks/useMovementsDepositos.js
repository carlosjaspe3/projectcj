import { useState, useEffect, useCallback } from "react";
import API from "./../api";
export function useMovementsDepositos(
  page = 1,
  limit = 10,
  type = null,
  status = null,
  date_init = null,
  date_end = null
) {
  const [movements, setMovements] = useState({});
  const [isLoggingInMovements, setIsLoggingInMovements] = useState(false);

  const getMovements = useCallback(
    async () => {
      try {
        setIsLoggingInMovements(true);
        const { data } = await API.get(
          `/monitor-depositos?page=${
            page + 1
          }&limit=${limit}&type=${type}&status=${status}&date[after]=${date_init}&date[before]=${date_end}`
        );
        if (data && data.data && data.data.movements) {
          setMovements(data.data);
        }
        setIsLoggingInMovements(false);
      } catch (error) {
        console.log(error);
        setIsLoggingInMovements(false);
      }
    },
    [limit, page, type, status, date_init, date_end],
  );
  
  useEffect(() => {
    getMovements();
  }, [limit, page, type, status, date_init, date_end, getMovements]);

  return { movements, isLoggingInMovements, getMovements };
}
