import React from "react";
import styled from "styled-components";

const CardRoadMap = ({ month, year, description }) => {
  return (
    <Container>
      <Data>
        {month} <span>{year}</span>
      </Data>
      <div className="total-center">
        <Point />
        <Line />
      </div>
      <Description>{description}</Description>
    </Container>
  );
};

const Container = styled.div`
  max-width: 800px;
  min-height: 100px;
  padding: 25px;
  box-shadow: 0 3px 20px 0 rgba(0, 0, 0, 0.05);
  background-color: #ffffff;
  display: grid;
  grid-template-columns: 200px auto 1fr;
  grid-gap: 10px;
  justify-items: center;
  align-items: center;
  position: relative;
  margin: 20px 0px;
  @media (max-width: 768px) {
    grid-template-columns: 150px auto 1fr;
  }
  @media (max-width: 520px) {
    grid-template-columns: 1fr;
  }
`;
const Point = styled.div`
  width: 10px;
  height: 10px;
  box-shadow: 0 0 10px 0 #399de5;
  background-color: #399de5;
  border-radius: 5px;
  @media (max-width: 520px) {
    display: none;
  }
`;
const Line = styled.div`
  position: absolute;
  top: -15px;
  bottom: -15px;
  width: 1px;
  border: solid 1px #399de5;
  height: ${(props) => props.heigth};
  @media (max-width: 520px) {
    display: none;
  }
`;
const Data = styled.p`
  font-family: Poppins;
  font-size: 16px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.56;
  letter-spacing: normal;
  text-align: right;
  color: #1d2b45;
  margin-bottom: 0px;
  span {
    font-weight: normal;
    color: #808080;
  }
  @media (max-width: 768px) {
    font-size: 12px;
  }
`;
const Description = styled.p`
  width: 100%;
  max-width: 500px;
  height: 98px;
  margin: 0 0 0 21px;
  font-family: Poppins;
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.56;
  letter-spacing: normal;
  text-align: left;
  color: #1d2b45;
  display: contents;
  @media (max-width: 768px) {
    font-size: 12px;
  }
`;
export default CardRoadMap;
