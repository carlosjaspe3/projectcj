import React, { useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";
import Button from "./Button";
const ModalCookies = () => {
  const [close, setClose] = useState(
    localStorage.getItem("cookies") === "true" ? true : false
  );
  React.useState(() => {
    if (localStorage.getItem("cookies") === null) {
      localStorage.setItem("cookies", true);
    }
  }, []);
  const handleClickClose = () => {
    localStorage.setItem("cookies", false);
    setClose(false);
  };
  return (
    <div>
      {close ? (
        <div className="total-center">
          <Container>
            <Modal>
              <Text>
                Utilizamos cookies para asegurarnos de que obtenga la mejor
                experiencia en nuestro sitio web, aunque las cookies que
                utilizamos no contienen información de identificación personal.
                Al continuar en este sitio web o al hacer clic en{" "}
                <span> "Acepto cookies" </span>, acepta el almacenamiento de
                cookies en su dispositivo. Puede obtener más información sobre
                nuestra política de privacidad, cómo usamos las cookies o cómo
                deshabilitar las cookies haciendo clic en el enlace{" "}
                <span> "Más información" </span> al final de esta declaración.
                <Link to="/cookies">
                  <span style={{ color: " #399DE5" }}> Más información</span>
                </Link>
              </Text>
              <div className="total-center">
                <ContainerButton>
                  <Button
                    
                    onClick={handleClickClose}
                    className="white-color"
                  >
                    Aceptar
                  </Button>
                </ContainerButton>
              </div>
            </Modal>
          </Container>
        </div>
      ) : null}
    </div>
  );
};

const Container = styled.div`
  position: fixed;
  bottom: 20px;
`;
const Modal = styled.div`
  width: 90vw;
  max-width: 1200px;
  min-height: 120px;
  background-color: #fff;
  box-shadow: 0 10px 22px 0 rgba(0, 0, 0, 0.16);
  padding: 10px;
  border-radius: 5px;
  /* display: flex;
  flex-direction: column; */
`;
const Text = styled.p`
  font-family: Poppins;
  font-size: 12px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.8;
  letter-spacing: normal;
  text-align: left;
  color: #1d2b45;
  span {
    font-weight: bold;
  }
`;
const ContainerButton = styled.div`
  width: 100%;
  max-width: 200px;
  margin-top: 10px;
`;
export default ModalCookies;
