import React, { useState } from "react";
import styled from "styled-components";

const Collapse = ({ title, description }) => {
  const [open, setOpen] = useState(false);
  return (
    <Container>
      <TitleBox onClick={() => setOpen(!open)}>
        <Title>{title}</Title>
        <IconArrow
          src={
            open
              ? "/assets/images/up-arrow.svg"
              : "/assets/images/down-arrow.svg"
          }
          alt="down-arrow"
        />
      </TitleBox>
      {open ? <InfoBox>{description}</InfoBox> : null}
    </Container>
  );
};

const Container = styled.div`
  max-width: 550px;
  min-height: 40px;
  background-color: #ffffff;
  margin: 20px 0px;
`;
const TitleBox = styled.div`
  padding: 10px;
  max-width: 550px;
  min-height: 40px;
  display: grid;
  grid-template-columns: 1fr 50px;
  -webkit-align-items: center;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  grid-gap: 10px;
  :hover {
    cursor: pointer;
  }
`;
const Title = styled.p`
  font-family: Poppins;
  font-size: 16px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.56;
  letter-spacing: normal;
  text-align: left;
  color: #1d2b45;
  margin-bottom: 0px;
`;
const InfoBox = styled.p`
  width: 100%;
  border-top: solid 0.5px #d9d9d9;
  font-family: Poppins;
  font-size: 16px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.56;
  letter-spacing: normal;
  text-align: left;
  padding: 20px;
  color: #1d2b45;
`;

const IconArrow = styled.img`
  width: 50px;
  height: auto;
`;
export default Collapse;
