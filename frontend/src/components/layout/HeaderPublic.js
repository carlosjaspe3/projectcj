import React from "react";
import styled from "styled-components";
import Button from "../Button";
import { useSelector } from "react-redux";

const HeaderPublic = (props) => {
  const token = useSelector((state) => state.accessStore.token);
  function changeRoute(route) {
    if (props.history.location.pathname !== "/") {
      props.history.push("/", {
        ancla: route,
      });
    } else if (props.history.location.pathname === "/") {
      const elmnt = document.getElementById(route);
      if (elmnt !== null) {
        elmnt.scrollIntoView({ behavior: "smooth" });
      }
    } else {
      props.history.push("/");
    }
  }

  return (
    <Container>
      <Logo
        src="assets/images/logo-completo.png"
        alt="karakorum"
        className="opacity"
        onClick={() => {
          changeRoute("home");
        }}
      />
      <ContainerItems>
        <Items
          onClick={() => {
            changeRoute("aboutus");
          }}
        >
          Sobre nosotros
        </Items>
        <Items
          onClick={() => {
            changeRoute("sherpa");
          }}
        >
          Nuestro sistema
        </Items>
        <Items
          onClick={() => {
            changeRoute("Advantage");
          }}
        >
          Ventajas
        </Items>
        <Items
          onClick={() => {
            changeRoute("contact");
          }}
        >
          Contacto
        </Items>
        {token ? null : (
          <Button
            outline
            className="btn-login"
            link="/login"
            small="120px"
            height="30px"
            onClick={() => props.history.push("/login")}
          >
            Iniciar Sesión
          </Button>
        )}
        {token ? null : (
          <Button
            link="/register"
            small="120px"
            height="30px"
            onClick={() => props.history.push("/register")}
          >
            Registrate
          </Button>
        )}
        {token && (
          <Button
            link="/register"
            small="120px"
            height="30px"
            onClick={() => props.history.push("/home")}
          >
            Regresar
          </Button>
        )}
      </ContainerItems>
      <ContainerMenuResponisve>
        <div className="dropdown profile-dropdown">
          <div
            className="nav-link pr-0 pl-2 leading-none"
            data-toggle="dropdown"
          >
            <div className="opacity">
              <i
                className="fa fa-navicon"
                data-toggle="tooltip"
                title="fa fa-flag"
                style={{ fontSize: "20px" }}
              ></i>
            </div>
          </div>
          <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow animated p-0">
            <div className="dropdown-item border-bottom">
              <Items
                onClick={() => {
                  changeRoute("aboutus");
                }}
              >
                Sobre nosotros
              </Items>
            </div>
            <div className="dropdown-item border-bottom">
              <Items
                onClick={() => {
                  changeRoute("sherpa");
                }}
              >
                Nuestro sistema
              </Items>
            </div>
            <div className="dropdown-item border-bottom">
              <Items
                onClick={() => {
                  changeRoute("Advantage");
                }}
              >
                Ventajas
              </Items>
            </div>
            <div className="dropdown-item border-bottom">
              <Items
                onClick={() => {
                  changeRoute("contact");
                }}
              >
                Contacto
              </Items>
            </div>
            <div className="dropdown-item border-bottom">
              <Button
                outline
                small
                className="btn-login"
                link="/login"
                onClick={() => props.history.push("/login")}
              >
                Iniciar Sesión
              </Button>
            </div>
            <div
              className="dropdown-item border-bottom"
              onClick={() => props.history.push("/register")}
            >
              <Button small link="/register">
                Registrate
              </Button>
            </div>
          </div>
        </div>
      </ContainerMenuResponisve>
    </Container>
  );
};

const Container = styled.div`
  width: 100%;
  height: 60px;
  background-color: #fff;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0px 20px;
  position: fixed;
  z-index: 99999;
`;
const Logo = styled.img`
  width: auto;
  height: 40px;
  object-fit: contain;
  @media (max-width: 1080px) {
    height: 25px;
  }
`;
const ContainerItems = styled.div`
  width: 700px;
  height: 40px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  @media (max-width: 1080px) {
    width: 580px;
  }
  @media (max-width: 768px) {
    display: none;
  }
`;
const Items = styled.p`
  width: auto;
  height: 20px;
  margin: 0px;
  font-family: "Poppins", sans-serif;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: center;
  color: #1d2b45;
  border-bottom: 0.5px solid transparent;
  cursor: pointer;
  transition-duration: 0.3s;
  transition-timing-function: ease-out;
  padding: 0px 2px;
  :hover {
    color: #3f768a;
    border-bottom: 0.5px solid #3f768a;
  }
  @media (max-width: 1080px) {
    font-size: 12px;
  }
`;
const ContainerMenuResponisve = styled.div`
  display: none;

  @media (max-width: 768px) {
    display: block;
    .profile-dropdown.show .dropdown-menu {
      left: -155px !important;
      top: 40px !important;
    }
  }
`;
export default HeaderPublic;
