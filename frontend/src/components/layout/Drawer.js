import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";
import { profileSelector } from "../../ducks/profile/selectors";
import styled from "styled-components";
import { formatCurrency } from "./../../utils";

const menu = [
  {
    title: "Inicio",
    icon: "fa fa-sign-in",
    router: "/home",
    reactour: "reactour__dashboard",
  },
  {
    title: "Operaciones",
    icon: "fa fa-cogs",
    router: "/operations",
    reactour: "reactour__operations_monitor",
  },
  {
    title: "Referidos",
    icon: "fa fa-user-plus",
    router: "/referrals",
    reactour: "reactour__referral_program",
  },
  {
    title: "Movimientos",
    icon: "fa fa-search",
    router: "/movements",
    reactour: "reactour__movements",
    childeres: [
      {
        title: "Depósitos y Retiros",
        icon: "fa fa-search",
        router: "/movements-depositos",
      },
      {
        title: "Rendimientos",
        icon: "fa fa-search",
        router: "/movements-rendimientos",
      },
    ],
  },
  {
    title: "Documentos",
    icon: "fa fa-file",
    router: "/document",
    reactour: "reactour__documents",
  },
  {
    title: "Soporte",
    icon: "fa fa-cog",
    router: "/contact",
    reactour: "reactour__support",
  },
];

const Drawer = () => {
  const [activeCopy, setActiveCopy] = useState(false);
  const history = useHistory();
  const profile = useSelector(profileSelector);

  const handleCopyToClipboard = (content) => () => {
    const el = document.createElement("textarea");
    el.value = content;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
    setActiveCopy(true);
  };

  const toggleSubMenu = (key) => {
    const father = document.querySelector(`[data-sub='sub-${key}']`);
    if (!father) return;
    father.classList.toggle("is-expanded");
  };

  useEffect(() => {
    const perfectScrollbar = () => {
      const container = document.querySelector("#app-sidebar");
      if (!container) return;
      new window.PerfectScrollbar(container);
    };
    perfectScrollbar();
  }, []);

  return (
    <aside id="app-sidebar" className="app-sidebar ps ps--active-y">
      <div className="app-sidebar__logo">
        <span
          className="header-brand"
          onClick={() => history.push("/")}
          style={{ cursor: "pointer" }}
        >
          <img
            src="../../assets/images/logo-completo-white.png"
            className="header-brand-img desktop-lgo"
            alt="Covido logo"
          />
          <div className="show-logo">
            <div className="total-center">
              <img
                src="../../assets/images/logo-completo-white.png"
                className="header-brand-img"
                alt="Covido logo"
              />
            </div>
          </div>
          <div className="hidden">
            <img
              src="../../assets/images/logo-white.png"
              className="header-brand-img mobile-logo hidden"
              alt="Covido logo"
            />
          </div>
        </span>
      </div>
      <div className="app-sidebar3">
        <div className="section-balance " id="balance" data-tut="reactour__balance_link_referred">
          <WrapBalance>
            <ContainerBalance>
              <p className="title-balance">Mi saldo actual</p>
              <p className="secondary-text-balance">
                {" "}
                {profile && profile.balance
                  ? formatCurrency(profile.balance)
                  : 0}
                {"$"}
              </p>
            </ContainerBalance>
            <ContainerBalance>
              <p className="title-balance">Mis compras</p>
              <p className="secondary-text-balance">
                {profile && profile.deposited
                  ? formatCurrency(profile.deposited)
                  : 0}
                {"$"}
              </p>
            </ContainerBalance>
          </WrapBalance>
          <p className="title-referral-link">Tu enlace de referido:</p>
          <div
            onMouseLeave={() => setActiveCopy(false)}
            className="highLighted opacity"
            onClick={handleCopyToClipboard(
              `https://www.karakorumcorp.com/register/${
                profile && profile.referralCode
              }`
            )}
            style={{ background: "red !important" }}
          >
            <p className="referral-link">
              {profile && profile.referralCode}{" "}
              <span>
                {activeCopy ? (
                  <i
                    className="fa fa-check"
                    data-toggle="tooltip"
                    title="copiar"
                    style={{ fontSize: "15px", marginLeft: "5px" }}
                  ></i>
                ) : (
                  <i
                    className="fa fa-copy"
                    data-toggle="tooltip"
                    title="copiar"
                    style={{ fontSize: "15px", marginLeft: "5px" }}
                  ></i>
                )}
              </span>
            </p>
          </div>
        </div>
        <ul className="side-menu">
          {menu.map((item, key) => {
            if (Array.isArray(item.childeres) && item.childeres.length > 0) {
              return (
                <li
                  className={
                    history.location.pathname.match(item.router)
                      ? `slide is-expanded`
                      : `slide`
                  }
                  key={key}
                  data-sub={`sub-${key}`}
                  data-tut={item.reactour}
                >
                  <span
                    className={
                      history.location.pathname.match(item.router)
                        ? `side-menu__item active`
                        : `side-menu__item`
                    }
                    data-toggle="slide"
                    style={{ cursor: "pointer" }}
                    onClick={(event) => {
                      event.preventDefault();
                      toggleSubMenu(key);
                    }}
                  >
                    <span className="shape1"></span>
                    <span className="shape2"></span>
                    <i className={item.icon} style={{ fontSize: "25px" }} />
                    <span
                      className="side-menu__label"
                      style={{ marginLeft: "10px" }}
                    >
                      {item.title}
                    </span>
                    <i
                      className="angle fa fa-angle-right"
                      style={{ marginRight: "10px" }}
                    ></i>
                  </span>
                  <ul className="slide-menu">
                    {item.childeres.map((child, _key) => (
                      <li key={_key}>
                        <span
                          style={{ cursor: "pointer" }}
                          onClick={(event) => {
                            event.preventDefault();
                            history.push(child.router);
                          }}
                          className={
                            history.location.pathname === child.router
                              ? `slide-item active`
                              : `slide-item`
                          }
                        >
                          {child.title}
                        </span>
                      </li>
                    ))}
                  </ul>
                </li>
              );
            } else {
              return (
                <li className="slide" key={key}>
                  <span
                    className={
                      history.location.pathname === item.router
                        ? `side-menu__item active`
                        : `side-menu__item`
                    }
                    data-tut={item.reactour}
                    onClick={() => history.push(item.router)}
                    style={{ cursor: "pointer" }}
                  >
                    <span className="shape1" />
                    <div className="total-center">
                      <i className={item.icon} style={{ fontSize: "25px" }} />
                      <span
                        className="side-menu__label"
                        style={{ marginLeft: "10px" }}
                      >
                        {item.title}
                      </span>
                    </div>
                    <span className="shape2" />
                  </span>
                </li>
              );
            }
          })}
        </ul>
      </div>
    </aside>
  );
};
const ContainerBalance = styled.div`
  border: 1px solid rgba(255, 255, 255, 0.05);
  width: 100%;
  height: 60px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
const WrapBalance = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;
export default Drawer;
