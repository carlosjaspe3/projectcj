import React from "react";
import ClickAwayListener from "react-click-away-listener";
import Drawer from "./Drawer";
import Header from "./Header";

const Layout = ({ children }) => {
  const handleClickAway = () => {
    if (window.innerWidth < 768) {
      var element = document.getElementById("AppSidebar");
      if (element !== null) {
        element.classList.remove("sidenav-toggled");
      }
    }
  };
  return (
    <div className="app-content">
      <div className="side-app" id="myDIV">
        <div className="page-main">
          <ClickAwayListener onClickAway={handleClickAway}>
            <Drawer />
            <Header />
          </ClickAwayListener>
          <div style={{ padding: "10px" }}>{children}</div>
        </div>
      </div>
    </div>
  );
};

export default Layout;
