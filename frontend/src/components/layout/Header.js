import React from "react";
import { useHistory } from "react-router-dom";
import "rc-slider/assets/index.css";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import Slider, { SliderTooltip } from "rc-slider";
import { useSelector, useDispatch } from "react-redux";
import { profileSelector } from "../../ducks/profile/selectors";
import { Link } from "react-router-dom";
import { logout } from "../../ducks/access/actions";
import Modal from "../Modal";
import MakePayment from "./../MakePayment";
import {
  depositCoinBaseInit,
  retireBalanceInit,
  getRetireAvailabel,
} from "../../ducks/general/actions";
import Loader from "../Loader";
import ModalQrVerification from "../ModalQrVerification";
import { activeAuthenticatorGoogleInit } from "../../ducks/access/actions";
import { getProfileInit } from "../../ducks/profile/actions";
import { formatCurrency } from "./../../utils";
import Swal from "sweetalert2";
import API from "./../../api";

// Tour
import Tour from "reactour";
// Tour END

const Header = () => {
  const { Handle } = Slider;
  const [depositPending, setDepositPending] = React.useState(false);
  const [isLoadingDepositDelete, setIsLoadingDepositDelete] = React.useState(
    false
  );

  const [historyPush, setHistoryPush] = React.useState(null);
  const [show, setShow] = React.useState(false);
  const [showMakePayment, setShowMakePayment] = React.useState(false);
  const [formMakePayment, setFormMakePayment] = React.useState({
    amount: null,
    btc: null,
    wallet: null,
    date: null,
    end: null,
    status: null,
    remaining: null,
    txs: [],
    transaction_id: null,
  });
  const [formRe, setFormRe] = React.useState({
    context: "",
  });
  const [showBuy, setShowBuy] = React.useState(false);
  const [errorDeposit, setErrorDeposit] = React.useState("");
  const history = useHistory();
  const dispatch = useDispatch();
  const profile = useSelector(profileSelector);
  const isLoadingDeposit = useSelector(
    (state) => state.generalStore.isLoadingDeposit
  );
  const isLoadingRetire = useSelector(
    (state) => state.generalStore.isLoadingRetire
  );
  const retireAvailabel = useSelector(
    (state) => state.generalStore.retireAvailabel
  );
  const [deposit, setDeposit] = React.useState(50);
  const [retire, setRetire] = React.useState(false);
  const [openModal, setOpenModal] = React.useState({
    open: false,
    context: "",
  });
  const [isLoadingModal, setIsLoadingModal] = React.useState(false);
  const [code, setCode] = React.useState("");
  const [body, setBody] = React.useState({});
  React.useEffect(() => {
    dispatch(getRetireAvailabel(() => {}));
    dispatch(getProfileInit());
  }, [dispatch]);
  const handleClose = () => {
    setErrorDeposit("");
    setShow(false);
    setShowBuy(false);
    setRetire(false);
    setErrorDeposit("");
  };

  // Tour
  const [isTourOpen, setIsTourOpen] = React.useState(false);
  const [goToStep, setGoToStep] = React.useState(0);
  // let goToStep = useState(0);
  const accentColor = "#40768A";

  const disableBody = target => {
    document.querySelector('html').style.overflowY = 'hidden'
  };
  const enableBody = target => {
    document.querySelector('html').style.overflowY = 'auto'
  };

  const closeTour = () => {
    setGoToStep(0);
    setIsTourOpen(false);
  };

  // const prevStep = (target, v) => {
  //   console.log('prevStep', target, v)
  //   goToStep -= 1
  // };

  // const nextStep = (target, v) => {
  //   console.log('nextStep', target, v)
  //   goToStep += 1
  // };
  const getCurrentStep = current => {
    setGoToStep(current);
  };
  // Tour END

  const handleRenderModal = () => {
    if (showBuy) {
      return handleRenderBuyUSD();
    } else if (retire) {
      return handleRenderRetire();
    } else {
      return handleRenderInfoOne();
    }
  };

  const handleSendRetire = (values) => {
    let body = {};
    if (values.context === "balance") {
      body.context = values.context;
      body.amount = values.amount;
      body.wallet = values.wallet;
    } else {
      body.context = values.context;
      body.amount = values.amount;
      body.wallet = values.wallet;
      body.idTransaction = values.idTransaction;
    }
    if (code) {
      body.code = code;
    }
    dispatch(
      retireBalanceInit(body, (Response) => {
        setIsLoadingModal(false);
        setCode("");
        handleClose();
        handleCloseModal();
        if (Response.bool) {
          dispatch(getProfileInit());
          document.getElementById("title").value = "Retiro exitoso!";
          document.getElementById("message").value = "";
          document.getElementById("type").value = "success";
          document.getElementById("click").click();
          history.push("/movements-depositos");
        } else {
          document.getElementById("title").value = Response.message;
          document.getElementById("message").value = "";
          document.getElementById("type").value = "error";
          document.getElementById("click").click();
        }
      })
    );
  };

  const handleClickRetire = (values, { resetForm }) => {
    if (profile && profile.twoAF) {
      handleClose();
      setBody(values);
      setOpenModal({ open: true, context: true });
    } else {
      handleSendRetire(values, resetForm);
    }
  };

  const textRenderRetireDeposit = () => {
    return (
      <>
        <h3>Retirar Depósitos</h3>
        <h6>Estas solicitando realizar un retiro de depósitos.</h6>
        <p>
          Para procesar el reembolso debes seleccionar un depósito liberado de
          la lista. Una vez realizada la solicitud, el depósito pasará al estado
          de devolución y dejará de generar rentabilidad. Las solicitudes
          procesadas antes del día 15 de cada mes, serán reembolsadas entre los
          días 5 y 10 del mes siguiente.
          <b>
            Debido a que las operaciones en Blockchain no pueden ser modificadas
            una vez realizado el envío,
          </b>
          es muy importante que prestes la máxima atención al poner la dirección
          del wallet donde deseas recibir la transacción y revisar que sea la
          correcta.
        </p>
      </>
    );
  };

  const textRenderRetireBalance = () => {
    return (
      <>
        <h3>Retirar Balance</h3>
        <h6>Estás solicitando realizar un retiro de balance.</h6>
        <p>
          Para procesar el retiro de balance, debes indicar el wallet de destino
          en el que quieres recibir el equivalente en Bitcoin del importe que
          vas a retirar.
          <b>
            Debido a que las operaciones en Blockchain no pueden ser modificadas
            una vez realizado el envío,
          </b>
          es muy importante que se realice con la máxima precaución al
          seleccionar el importe a transferir y la dirección del wallet donde se
          desea recibir la transacción, ya que una vez realizada se procesa de
          manera inmediata y de forma automática, por lo que no sería posible
          realizar ninguna modificación.
        </p>
      </>
    );
  };

  const textInfoOne = () => {
    return (
      <>
        <h3>Pasarela Blockchain</h3>
        <p>
          A través de nuestra pasarela Blockchain, puedes realizar los depósitos
          y retiros de tus rendimientos de forma totalmente automatizada.
        </p>
      </>
    );
  };

  const renderText = (context) => {
    if (context === "deposit") {
      return textRenderRetireDeposit();
    } else if (context === "balance") {
      return textRenderRetireBalance();
    } else {
      return textInfoOne();
    }
  };

  const handleRenderRetire = () => {
    return (
      <div className="container-modal">
        <button className="btn_back" type="button" onClick={handleBack}>
          <span aria-hidden="true" className="fa fa-arrow-left"></span>
        </button>
        <button className="close" type="button" onClick={handleClose}>
          <span aria-hidden="true">&times;</span>
        </button>
        <div className="total-center">
          <img
            src="../assets/images/logo-completo.png"
            className="header-brand-img "
            alt="Covido logo"
          />
        </div>
        <div className="total-center container-logo-modal">
          <div className="center-info-modal">{renderText(formRe.context)}</div>
        </div>
        <Formik
          initialValues={{
            context: "",
            idTransaction: "",
            amount: 50,
            wallet: "",
            code: "",
            codeActive: false,
          }}
          validationSchema={Yup.object({
            context: Yup.string().required("El campo es requerido*"),
            amount: Yup.string().required("El campo es requerido*"),
            wallet: Yup.string().required("El campo es requerido*"),
            idTransaction: Yup.string().when("context", {
              is: "deposit",
              then: Yup.string().required("El campo es requerido*"),
            }),
          })}
          onSubmit={handleClickRetire}
        >
          {({ values, handleChange, setFieldValue }) => {
            const balanceFloat = parseFloat(profile.balance);
            setFormRe(values);
            const balanceInt = parseInt(profile.balance);
            return (
              <Form>
                <div style={{ marginTop: "10px" }}>
                  <select
                    name="context"
                    id="context"
                    placeholder="Tipo de retiro"
                    className="form-control custom-select select2"
                    onChange={handleChange}
                    value={values.context}
                  >
                    <option value="">--Tipo de retiro--</option>
                    <option value="deposit">Depósitos</option>
                    <option value="balance">Balance</option>
                  </select>
                  <ErrorMessage
                    name="context"
                    component="div"
                    className="textErrorLabel"
                  />
                </div>
                {values.context === "deposit" &&
                retireAvailabel &&
                Array.isArray(retireAvailabel) &&
                retireAvailabel.length ? (
                  <div style={{ marginTop: "10px" }}>
                    <select
                      name="idTransaction"
                      id="idTransaction"
                      placeholder="Tipo de retiro"
                      className="form-control custom-select select2"
                      onChange={(e) => {
                        if (
                          retireAvailabel &&
                          Array.isArray(retireAvailabel) &&
                          retireAvailabel.length
                        ) {
                          retireAvailabel.forEach((item) => {
                            if (item.id === parseInt(e.target.value, 10)) {
                              setFieldValue("amount", item.amount);
                            }
                          });
                          setFieldValue("idTransaction", e.target.value);
                        }
                      }}
                      value={values.idTransaction}
                    >
                      <option value="">--Seleccione el deposito--</option>
                      {retireAvailabel &&
                      Array.isArray(retireAvailabel) &&
                      retireAvailabel.length
                        ? retireAvailabel.map((item, key) => {
                            return (
                              <option value={item.id} key={key}>
                                {item && formatCurrency(item.amount)}
                              </option>
                            );
                          })
                        : null}
                    </select>
                    <ErrorMessage
                      name="idTransaction"
                      component="div"
                      className="textErrorLabel"
                    />
                  </div>
                ) : null}
                {(values.context === "deposit" &&
                  retireAvailabel &&
                  Array.isArray(retireAvailabel) &&
                  retireAvailabel.length) ||
                (values.context === "balance" &&
                  profile.balance &&
                  balanceFloat > 50) ? (
                  <div style={{ marginTop: "10px" }}>
                    <input
                      id="wallet"
                      className="form-control"
                      placeholder="Billetera"
                      onChange={handleChange}
                      value={values.wallet}
                    />
                    <ErrorMessage
                      name="wallet"
                      component="div"
                      className="textErrorLabel"
                    />
                  </div>
                ) : null}
                {values.context === "balance" &&
                profile.balance &&
                balanceFloat > 50 ? (
                  <div style={{ marginTop: "10px" }}>
                    <input
                      id="amount"
                      className="form-control"
                      placeholder="Monto $"
                      onChange={(event) => {
                        if (/^\d*(\.\d+)?$/.test(event.target.value)) {
                          if (!(event.target.value > 100000)) {
                            setFieldValue("amount", event.target.value);
                          }
                        }
                      }}
                      value={values.amount}
                    />
                    <ErrorMessage
                      name="amount"
                      component="div"
                      className="textErrorLabel"
                    />
                  </div>
                ) : null}
                {(values.context === "deposit" &&
                  retireAvailabel &&
                  Array.isArray(retireAvailabel) &&
                  retireAvailabel.length) ||
                (values.context === "balance" &&
                  profile.balance &&
                  balanceFloat > 50) ? (
                  <div className="heigth-slider">
                    <Slider
                      max={balanceInt}
                      min={1}
                      step={1}
                      onChange={(e) => setFieldValue("amount", e)}
                      value={values.amount}
                      disabled={values.context === "deposit" ? true : false}
                      handle={(props) => {
                        const { value, dragging, index, ...restProps } = props;
                        return (
                          <SliderTooltip
                            prefixCls="rc-slider-tooltip"
                            visible={dragging}
                            placement="top"
                            key={index}
                          >
                            <Handle {...restProps}>
                              <div className="tooltip-label">{`${formatCurrency(
                                props.value
                              )}`}</div>
                            </Handle>
                          </SliderTooltip>
                        );
                      }}
                    />
                  </div>
                ) : null}
                {!(
                  (values.context === "deposit" &&
                    retireAvailabel &&
                    Array.isArray(retireAvailabel) &&
                    retireAvailabel.length) ||
                  values.context === "balance" ||
                  values.context === ""
                ) ? (
                  <p className="textErrorLabel">
                    No posee depósito para retirar.
                  </p>
                ) : null}
                {values.context === "balance" &&
                  (profile.balance === null || balanceFloat < 50) && (
                    <p className="textErrorLabel">
                      No posee balance para retirar.
                    </p>
                  )}
                <div>
                  {isLoadingRetire ? (
                    <Loader />
                  ) : (
                    <button
                      type="submit"
                      className="button-buy-modal total-center"
                      style={{ marginTop: "15px" }}
                    >
                      Retirar
                    </button>
                  )}
                </div>
              </Form>
            );
          }}
        </Formik>
      </div>
    );
  };

  const [isLoadingBtnDepositar, setIsLoadingBtnDepositar] = React.useState(
    false
  );
  const handleRenderInfoOne = () => {
    const btnoOnDepositar = async () => {
      try {
        setIsLoadingBtnDepositar(true);
        const { data } = await API.get(`/deposit/pending/validate`);
        if (
          data &&
          data.data &&
          data.data.status &&
          data.data.status === "pending"
        ) {
          handleClose();
          setShowMakePayment(true);
          setDepositPending(true);
          setFormMakePayment(data.data);
        } else {
          setDepositPending(false);
          setShowBuy(true);
        }
        setIsLoadingBtnDepositar(false);
      } catch (error) {
        console.log(error);
        setIsLoadingBtnDepositar(false);
      }
    };

    return (
      <div className="container-modal">
        <button className="close" type="button" onClick={handleClose}>
          <span aria-hidden="true">&times;</span>
        </button>
        <div className="total-center">
          <img
            src="../assets/images/logo-completo.png"
            className="header-brand-img "
            alt="Covido logo"
          />
        </div>
        <div className="total-center container-logo-modal">
          <div className="center-info-modal">{textInfoOne()}</div>
        </div>
        <div>
          <button
            className="button-buy-modal total-center"
            disabled={isLoadingBtnDepositar}
            onClick={btnoOnDepositar}
          >
            {isLoadingBtnDepositar ? "Cargando..." : "Depositar"}
          </button>
        </div>
        <div>
          <button
            className="button-modal-sell total-center"
            onClick={() => setRetire(true)}
          >
            Retirar
          </button>
        </div>
      </div>
    );
  };

  const handleCloseMakePayment = () => {
    setShowMakePayment(false);
    if (historyPush !== null) {
      history.push(historyPush);
    }
  };

  const pendingDelete = async () => {
    try {
      setIsLoadingDepositDelete(true);
      const { data } = await API.post(
        `/deposit/pending/delete/${formMakePayment.transaction_id}`
      );
      if (data && data.OK) {
        setShowMakePayment(false);
        setDepositPending(false);
        setFormMakePayment({
          amount: null,
          btc: null,
          wallet: null,
          date: null,
          end: null,
          status: null,
          remaining: null,
          txs: [],
          transaction_id: null,
        });
        setShow(true);
        setShowBuy(true);
      }
      setIsLoadingDepositDelete(false);
    } catch (error) {
      console.log(error);
      setIsLoadingDepositDelete(false);
    }
  };

  const pendingDeleteConfirm = () => {
    Swal.fire({
      title: "¿Estas seguro?",
      text: "¡No podrás revertir esto!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "¡Sí, bórralo!",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.isConfirmed) {
        pendingDelete();
      }
    });
  };

  const handleRenderMakePayment = () => {
    return (
      <div className="container-modal make-payment">
        <button
          className="close"
          type="button"
          onClick={handleCloseMakePayment}
        >
          <span aria-hidden="true">&times;</span>
        </button>
        <div className="total-center container-logo-modal">
          <img
            src="../assets/images/logo-completo.png"
            className="header-brand-img "
            alt="Covido logo"
          />
        </div>
        <MakePayment
          {...formMakePayment}
          handleClose={handleCloseMakePayment}
          showOK={!depositPending}
        />

        {depositPending && (
          <div style={{ display: "flex", justifyContent: "center" }}>
            <button
              className="button-k-primary total-center"
              disabled={isLoadingDepositDelete}
              style={{ margin: "10px 0 0 0", padding: "0", width: "100%" }}
              onClick={pendingDeleteConfirm}
            >
              {isLoadingDepositDelete ? (
                <>Eliminando...</>
              ) : (
                <>Click aquí para eliminar</>
              )}
            </button>
          </div>
        )}
      </div>
    );
  };

  const handleSendAmount = () => {
    const formData = new FormData();
    formData.append("amount", deposit + ".00");
    dispatch(
      depositCoinBaseInit(formData, (Response) => {
        setCode("");
        if (Response.bool && Response.data) {
          setFormMakePayment(Response.data);
          setIsLoadingModal(false);
          setShow(false);
          setShowMakePayment(true);
          handleCloseModal();
          setHistoryPush("/movements-depositos");
        }
      })
    );
  };

  const handleClickDeposit = () => {
    if (parseInt(deposit, 10) >= 50) {
      handleSendAmount();
    } else {
      setErrorDeposit("El monto mínimo es de 50$");
    }
  };

  const handleBack = () => {
    setErrorDeposit("");
    setShowBuy(false);
    setRetire(false);
    setErrorDeposit("");
    setShow(true);
  };

  const handleRenderBuyUSD = () => {
    return (
      <div className="container-modal">
        <button className="btn_back" type="button" onClick={handleBack}>
          <span aria-hidden="true" className="fa fa-arrow-left"></span>
        </button>
        <button className="close" type="button" onClick={handleClose}>
          <span aria-hidden="true">&times;</span>
        </button>
        <div className="total-center">
          <img
            src="../assets/images/logo-completo.png"
            className="header-brand-img "
            alt="Covido logo"
          />
        </div>
        <div className="total-center container-logo-modal">
          <div className="center-info-modal">
            <h3>Depósitos</h3>
            <h6>Estás solicitando realizar un nuevo depósito.</h6>
            <p>
              Para procesar el nuevo depósito primero debes seleccionar la
              cantidad a depositar, una vez solicitado el depósito se genera un
              código QR con la dirección de Bitcoin en la que debes realizar el
              depósito y la cantidad de Bitcoin a depositar.
              <b>
                Debido a que las operaciones en Blockchain no pueden ser
                modificadas una vez realizado el envío,
              </b>
              es muy importante que se realice con la máxima precaución y envié
              la cantidad exacta de Bitcoin.
            </p>
          </div>
        </div>
        <div style={{ marginTop: "10px" }}>
          <input
            className="form-control"
            placeholder="Monto $"
            value={deposit}
            onChange={(event) => {
              if (/^\d*(\.\d+)?$/.test(event.target.value)) {
                if (!(event.target.value > 100000)) {
                  setErrorDeposit("");
                  setDeposit(event.target.value);
                }
              }
            }}
          />
          <div className="textErrorLabel">
            {errorDeposit && <p>{errorDeposit}</p>}
          </div>
        </div>
        <div className="heigth-slider">
          <Slider
            max={100000}
            min={1}
            step={1}
            onChange={(value) => setDeposit(value)}
            value={deposit}
            handle={(props) => {
              const { value, dragging, index, ...restProps } = props;
              return (
                <SliderTooltip
                  prefixCls="rc-slider-tooltip"
                  visible={dragging}
                  placement="top"
                  key={index}
                >
                  <Handle {...restProps}>
                    <div className="tooltip-label">{`${formatCurrency(
                      props.value
                    )}`}</div>
                  </Handle>
                </SliderTooltip>
              );
            }}
          />
        </div>
        <div>
          {isLoadingDeposit ? (
            <Loader />
          ) : (
            <div
              className="button-buy-modal total-center"
              onClick={handleClickDeposit}
              style={{ marginTop: "15px" }}
            >
              Depositar
            </div>
          )}
        </div>
      </div>
    );
  };

  const handleClickAway = () => {
    if (window.innerWidth < 768) {
      var element = document.getElementById("AppSidebar");
      if (element !== null) {
        element.classList.remove("sidenav-toggled");
      }
    }
  };

  const handleCloseModal = () => {
    setOpenModal({ open: false, context: "" });
  };

  const handleChangeCode = (event) => {
    setCode(event.target.value);
  };

  const handleClickSendCode = () => {
    setIsLoadingModal(true);
    const formData = new FormData();
    formData.append("code", code);
    dispatch(
      activeAuthenticatorGoogleInit(formData, (Response) => {
        if (Response.bool) {
          if (openModal && openModal.context) {
            handleSendRetire(body);
          } else {
            handleSendAmount();
          }
        } else {
          setIsLoadingModal(false);
          handleCloseModal();
          document.getElementById("title").value = Response.message;
          document.getElementById("message").value = "";
          document.getElementById("type").value = "error";
          document.getElementById("click").click();
        }
      })
    );
  };

  return (
    <div id="AppSidebar">
      <ModalQrVerification
        isOpen={openModal.open}
        close={handleCloseModal}
        isLoading={isLoadingModal}
        code={code}
        handleChangeCode={handleChangeCode}
        send={handleClickSendCode}
      />
      <Modal isOpen={show}>{handleRenderModal()}</Modal>
      <Modal isOpen={showMakePayment}>{handleRenderMakePayment()}</Modal>
      <div className="app-header header">
        <div className="container-fluid">
          <div className="d-flex">
            <div className="app-sidebar__toggle" data-toggle="sidebar">
              <span className="open-toggle">
                <svg
                  className="header-icon mt-1"
                  xmlns="http://www.w3.org/2000/svg"
                  height="24"
                  viewBox="0 0 24 24"
                  width="24"
                  style={{ cursor: "pointer" }}
                >
                  <path d="M0 0h24v24H0V0z" fill="none"></path>
                  <path d="M21 11.01L3 11v2h18zM3 16h12v2H3zM21 6H3v2.01L21 8z"></path>
                </svg>
              </span>
            </div>
            <div
              className="d-flex order-lg-2 ml-auto"
              onClick={handleClickAway}
            >
              <div className="section-icon-header">
                <div className="opacity" data-tut="reactour__runway_blockchain" onClick={() => setShow(true)}>
                  <i
                    className="fa fa-shopping-bag "
                    data-toggle="tooltip"
                    style={{ fontSize: "20px" }}
                  ></i>
                </div>
                <div className="opacity">
                  <i
                    className="fa fa-user-plus"
                    data-toggle="tooltip"
                    style={{ fontSize: "20px" }}
                    onClick={() => history.push("/referrals")}
                  ></i>
                </div>
                <div className="opacity">
                  <i
                    className="fa fa-bar-chart"
                    data-toggle="tooltip"
                    style={{ fontSize: "20px" }}
                    onClick={() => history.push("/home")}
                  ></i>
                </div>
              </div>
              <div className="dropdown header-fullscreen" />
              <div className="dropdown profile-dropdown">
                <div
                  onClick={handleClickAway}
                  className="nav-link pr-0 pl-2 leading-none"
                  data-toggle="dropdown"
                >
                  <div className="logo-letter background-primary" data-tut="reactour__settings_profile">
                    {`${profile.name.charAt(0)}${profile.surname.charAt(0)}`}
                  </div>
                </div>
                <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow animated p-0">
                  <div className="text-center border-bottom pb-4 pt-4">
                    <div className="text-center user pb-0 font-weight-bold">
                      {`${profile && profile.name} ${
                        profile && profile.surname
                      }`}
                    </div>
                  </div>
                  <Link to={`/profile`}>
                    <div className="dropdown-item border-bottom">
                      <i className="dropdown-icon mdi mdi-account-outline"></i>
                      Perfil
                    </div>
                  </Link>
                  <div style={{ cursor: "pointer" }} onClick={() => setIsTourOpen(true)}>
                    <div className="dropdown-item border-bottom">
                      <i className="dropdown-icon mdi mdi-chevron-double-right"></i>
                      Iniciar Tour
                    </div>
                  </div>
                  <Link to={`/login`} onClick={() => dispatch(logout())}>
                    <div className="dropdown-item border-bottom">
                      <i className="dropdown-icon mdi mdi-logout-variant"></i>
                      Cerrar sesión
                    </div>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <Tour
        onRequestClose={closeTour}
        steps={tourConfig}
        isOpen={isTourOpen}
        goToStep={goToStep}
        maskClassName="mask"
        className="helper"
        rounded={5}
        accentColor={accentColor}
        onAfterOpen={disableBody}
        onBeforeClose={enableBody}
        getCurrentStep={getCurrentStep}
      />
    </div>
  );
};

const tourConfig = [
  {
    selector: '[data-tut="reactour__balance_link_referred"]',
    content: () => (
      <div style={{ display: "flex", width: "380px" }}>
        <div style={{ width: "160px", marginRight: "15px", flex: "1 0 160px" }}>
          <img src="/assets/images/reactour/1.png" alt="1" style={{ width: "100%" }} />
        </div>
        <div style={{ width: "200px", flex: "1 0 200px" }}>
          <h5>1. Balances y Link de Referido</h5>
          <p style={{ marginTop: "10px" }}>En este apartado, te mostramos el balance de tu cuenta de usuario. El balance de tu saldo disponible y el balance de los depósitos activos.</p>
          <p style={{ marginTop: "10px" }}>También te facilitamos tu link de referido. Pincha sobre el botón para copiarlo y envíaselo a otras personas que quieran registrarse en nuestra plataforma y genera beneficios diarios sobre la rentabilidad de tus invitados.</p>
        </div>
      </div>
    )
  },
  {
    selector: '[data-tut="reactour__runway_blockchain"]',
    content: () => (
      <div style={{ display: "flex", width: "380px" }}>
        <div style={{ width: "160px", marginRight: "15px", flex: "1 0 160px" }}>
          <img src="/assets/images/reactour/2.png" alt="2" style={{ width: "100%" }} />
        </div>
        <div style={{ width: "200px", flex: "1 0 200px" }}>
          <h5>2. Pasarela Blockchain</h5>
          <p style={{ marginTop: "10px" }}>A través de nuestra pasarela Blockchain, puedes realizar los depósitos y retiros de tus compras.</p>
          <p style={{ marginTop: "10px" }}>Selecciona Depositar y realiza tu compra desde 50$ y comienza a generar beneficios diarios.</p>
          <p style={{ marginTop: "10px" }}>Selecciona retirar y podrás hacer retiros instantáneos de tus beneficios desde 50$ o realiza el retiro de tus depósitos liberados.</p>
        </div>
      </div>
    )
  },
  {
    selector: '[data-tut="reactour__settings_profile"]',
    content: () => (
      <div style={{ display: "flex", width: "380px" }}>
        <div style={{ width: "160px", marginRight: "15px", flex: "1 0 160px" }}>
          <img src="/assets/images/reactour/3.png" alt="3" style={{ width: "100%" }} />
        </div>
        <div style={{ width: "200px", flex: "1 0 200px" }}>
          <h5>3. Ajustes de Perfil</h5>
          <p style={{ marginTop: "10px" }}>
            En esta sección puedes modificar tus datos personales, editar contraseña, activar la opción de interés compuesto y ampliar las opciones de seguridad, estableciendo tus claves 2FA y Anti-phishing.
          </p>
        </div>
      </div>
    )
  },
  {
    selector: '[data-tut="reactour__dashboard"]',
    content: () => (
      <div style={{ display: "flex", width: "380px" }}>
        <div style={{ width: "160px", marginRight: "15px", flex: "1 0 160px" }}>
          <img src="/assets/images/reactour/4.png" alt="4" style={{ width: "100%" }} />
        </div>
        <div style={{ width: "200px", flex: "1 0 200px" }}>
          <h5>4. Dashboard</h5>
          <p style={{ marginTop: "10px" }}>En esta área,  puedes realizar un seguimiento detallado de los rendimientos diarios obtenidos por el fondo, y además un resumen de los beneficios obtenidos por tus compras.</p>
        </div>
      </div>
    )
  },
  {
    selector: '[data-tut="reactour__operations_monitor"]',
    content: () => (
      <div style={{ display: "flex", width: "380px" }}>
        <div style={{ width: "160px", marginRight: "15px", flex: "1 0 160px" }}>
          <img src="/assets/images/reactour/5.png" alt="5" style={{ width: "100%" }} />
        </div>
        <div style={{ width: "200px", flex: "1 0 200px" }}>
          <h5>5. Monitor de Operaciones</h5>
          <p style={{ marginTop: "10px" }}>En este apartado, puedes comprobar en tiempo real las operaciones de arbitraje que realiza nuestro Bot.</p>
          <p style={{ marginTop: "10px" }}>Pulsa encima de las operaciones realizadas y obtén el detalle de las operaciones.</p>
        </div>
      </div>
    )
  },
  {
    selector: '[data-tut="reactour__referral_program"]',
    content: () => (
      <div style={{ display: "flex", width: "380px" }}>
        <div style={{ width: "160px", marginRight: "15px", flex: "1 0 160px" }}>
          <img src="/assets/images/reactour/6.png" alt="6" style={{ width: "100%" }} />
        </div>
        <div style={{ width: "200px", flex: "1 0 200px" }}>
          <h5>6. Programa de Referidos</h5>
          <p style={{ marginTop: "10px" }}>Aquí encontraras todos los detalles sobre tus invitados, aumenta el volumen en tu estructura, desbloquea los niveles y genera comisiones diarias por tus referidos.</p>
        </div>
      </div>
    )
  },
  {
    selector: '[data-tut="reactour__movements"]',
    content: () => (
      <div style={{ display: "flex", width: "380px" }}>
        <div style={{ width: "160px", marginRight: "15px", flex: "1 0 160px" }}>
          <img src="/assets/images/reactour/7.png" alt="7" style={{ width: "100%" }} />
        </div>
        <div style={{ width: "200px", flex: "1 0 200px" }}>
          <h5>7. Movimientos</h5>
          <p style={{ marginTop: "10px" }}>En este apartado, encuentras todos los detalles de tus movimientos. Pulsa en el botón detalles del movimiento y accede a la información sobre tus depósitos, retiros y rendimientos.</p>
        </div>
      </div>
    )
  },
  {
    selector: '[data-tut="reactour__documents"]',
    content: () => (
      <div style={{ display: "flex", width: "380px" }}>
        <div style={{ width: "160px", marginRight: "15px", flex: "1 0 160px" }}>
          <img src="/assets/images/reactour/8.png" alt="8" style={{ width: "100%" }} />
        </div>
        <div style={{ width: "200px", flex: "1 0 200px" }}>
          <h5>8. Documentos</h5>
          <p style={{ marginTop: "10px" }}>Aquí encontraras los documentos oficiales de la compañía. Pulsa encima del botón y accede al documento, podrás descargarlo para compartirlo.</p>
        </div>
      </div>
    )
  },
  {
    selector: '[data-tut="reactour__support"]',
    content: () => (
      <div style={{ display: "flex", width: "380px" }}>
        <div style={{ width: "160px", marginRight: "15px", flex: "1 0 160px" }}>
          <img src="/assets/images/reactour/9.png" alt="9" style={{ width: "100%" }} />
        </div>
        <div style={{ width: "200px", flex: "1 0 200px" }}>
          <h5>8. Soporte</h5>
          <p style={{ marginTop: "10px" }}>A través de este canal, puedes contactar con nuestro equipo de soporte. Si tienes alguna consulta, no dudes en contactar, estaremos encantados de ayudarte.</p>
        </div>
      </div>
    )
  },
];

export default Header;
