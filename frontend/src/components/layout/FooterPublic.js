import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const FooterPublic = () => {
  return (
    <div>
      <Container>
        <Logo src="/assets/images/logoSecondary.svg" alt="logo karakorum" />
        <Networks>
          <a
            href="https://www.youtube.com/channel/UCK3yUo6Zhhdahlk5dlYb7sg"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img src="/assets/images/youtube.svg" alt="youtube" height="35px" />
          </a>
          <a
            href="https://t.me/karakorumcorp"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              src="/assets/images/telegram.svg"
              alt="telegram"
              height="35px"
            />
          </a>
          <a
            href="https://www.instagram.com/karakorumcorp"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              src="/assets/images/instagram.svg"
              alt="instagram"
              height="35px"
            />
          </a>
        </Networks>
        <ContainerItems>
          <Link to="/privacy">
            <Items>Privacidad</Items>
          </Link>
          <Link to="/cookies">
            <Items>Cookies</Items>
          </Link>
          <Link to="/terms-and-conditions">
            <Items>Términos y Condiciones</Items>
          </Link>
        </ContainerItems>
      </Container>
      <EndStrip>
        <p>
          COPIRYGHT - 2021 KARAKORUMCORP.COM - TODOS LOS DERECHOS RESERVADOS
        </p>
      </EndStrip>
    </div>
  );
};

const Container = styled.div`
  width: 100%;
  min-height: 200px;
  background-color: #d9d9d9;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-bottom: 30px;
  /* justify-content: space-around; */
`;
const Logo = styled.img`
  width: auto;
  height: 80px;
  object-fit: contain;
  margin-top: 30px;
`;
const Networks = styled.div`
  width: 100%;
  max-width: 200px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 30px;
  margin-bottom: 30px;
`;
const Items = styled.p`
  font-family: Poppins;
  font-size: 14px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: left;
  color: #1d2b45;
  transition-duration: 0.3s;
  transition-timing-function: ease-out;
  :hover {
    cursor: pointer;
    opacity: 0.5;
  }
`;
const ContainerItems = styled.div`
  width: 100%;
  max-width: 350px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  @media (max-width: 420px) {
    flex-direction: column;
  }
`;
const EndStrip = styled.div`
  width: 100%;
  min-height: 100px;
  background-color: #1d2b45;
  display: flex;
  align-items: center;
  justify-content: center;
  p {
    width: 95vw;
    font-family: Poppins;
    font-size: 12px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.5;
    letter-spacing: normal;
    text-align: center;
    color: #ffffff;
  }
`;
export default FooterPublic;
