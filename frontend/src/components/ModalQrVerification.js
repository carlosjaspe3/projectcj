import React from "react";
import Modal from "react-modal";
import Button from "./Button";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    padding: 0,
    marginRight: "-50%",
    borderRadius: 5,
    boxShadow: "0 20px 20px 0 rgba(0, 0, 0, 0.1)",
    border: "none",
    transform: "translate(-50%, -50%)",
    overflow: "visible",
    zIndex: "9999999999999999999999 !important",
  },
};

Modal.setAppElement("#root");

const ModalQrVerification = ({
  isOpen,
  close,
  isLoading,
  code,
  handleChangeCode,
  send,
}) => {
  return (
    <Modal
      isOpen={isOpen}
      style={customStyles}
      closeTimeoutMS={300}
      contentLabel="Detalle del producto"
    >
      <div className="modal-query">
        <div className="modal-header">
          <h6 className="modal-title">Google Authenticator</h6>
          <button
            aria-label="Close"
            className="close"
            data-dismiss="modal"
            type="button"
            onClick={close}
          >
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="modal-body">
          <h6>Debes introducir el código 2FA.</h6>
        </div>
        <div className="modal-body">
          <div>
            <input
              className="code_2FA"
              maxLength="6"
              onChange={handleChangeCode}
              value={code}
            />
          </div>
        </div>
        <div className="modal-footer">
          <Button
            className={
              isLoading
                ? "btn btn-loading"
                : "btn text-btn"
            }
            type="button"
            small="100%"
            height="50px"
            onClick={send}
          >
            {isLoading ? <div style={{ height: "35px" }} /> : "Enviar"}
          </Button>
        </div>
      </div>
    </Modal>
  );
};

export default ModalQrVerification;
