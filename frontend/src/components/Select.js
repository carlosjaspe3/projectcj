import React from "react";
import styled from "styled-components";

const Select = ({ placeholder, children }) => {
  return (
    <Container>
      <SelectStyles>{children}</SelectStyles>
    </Container>
  );
};

const Container = styled.div`
  width: 100%;
  max-width: 270px;
  border: 1px solid #c8cfd8;
  background-color: #fff;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  border-radius: 5px;
  display: flex;
  justify-content: center;

  @media (max-width: 420px) {
    width: 100%;
    max-width: 95vw;
  }
`;

const SelectStyles = styled.select`
  width: calc(100% - 10px);
  padding: 0px 5px;
  max-width: 250px;
  height: 37px;
  display: block;
  padding: 0px;
  font-size: 14px;
  outline: 0;
  color: #424e79;
  opacity: 1;
  border-color: transparent;
`;
export default Select;
