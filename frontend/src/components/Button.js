import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

const Botton = ({
  children,
  outline,
  small,
  type,
  link,
  onClick,
  uppercase,
  marginTop,
  marginBottom,
  className,
  height,
  disabled,
}) => {
  const btnLogin = className === "btn-login" ? className : "";
  return (
    <Button
      outline={outline}
      disabled={disabled}
      className={btnLogin}
      small={small}
      type={type}
      onClick={onClick}
      uppercase={uppercase}
      marginTop={marginTop}
      marginBottom={marginBottom}
      height={height}
    >
      {link && link ? (
        <Link to={link && link}>{children}</Link>
      ) : (
        <p className={className}>{children}</p>
      )}
    </Button>
  );
};

const Button = styled.button`
  width: 100%;
  max-width: ${(props) => (props.small ? props.small : "200px")};
  height: ${(props) => (props.height ? props.height : "40px")};
  outline: 0;
  cursor: pointer;
  border: none;

  text-transform: ${(props) => (props.uppercase ? "uppercase" : null)};
  border-radius: 5px;
  background: ${(props) =>
    props.outline
      ? props.outline
      : `linear-gradient(
    130deg,
    rgba(29, 43, 69, 1) 0%,
    rgba(63, 118, 138, 1) 60%,
    rgba(196, 254, 255, 1) 100%
  );`};
  white-space: nowrap;
  margin-top: ${(props) => props.marginTop};
  margin-bottom: ${(props) => props.marginBottom};

  transition-duration: 0.3s;
  transition-timing-function: ease-out;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  @media (max-width: 420px) {
    width: 100%;
    max-width: 95vw;
  }
  :hover {
    opacity: 0.8;
  }
  a {
    color: ${(props) => (props.outline ? "#3f768a" : "#fff")};
    font-size: ${(props) => (props.small ? "13px" : "16px")};
    font-family: "Poppins", sans-serif;
    text-align: center;
    font-weight: 400;
  }
  :disabled {
    opacity: 0.5;
    cursor: not-allowed;
  }
`;

export default Botton;
