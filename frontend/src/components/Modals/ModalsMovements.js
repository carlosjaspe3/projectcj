import React, { useEffect, useState } from "react";
import Modal from "../Modal";
import { Badge, Table } from "reactstrap";
import { formatCurrency } from "./../../utils";
import Loader from "../../components/Loader";
import moment from "moment";

export const ModalsMovements = ({ isOpen, handleClose, movement, loading }) => {
  let total = 0;

  const [title, setTitle] = useState("MOVIMIENTO");

  useEffect(() => {
    if (movement && movement.aux && movement.aux.id) {
      setTitle(`MOVIMIENTO: ID-${movement.aux.id}`);
    }
  }, [movement]);

  return (
    <Modal isOpen={isOpen} size="md">
      <div className="container-modal">
        <button className="close" type="button" onClick={handleClose}>
          <span aria-hidden="true">&times;</span>
        </button>
        <div
          className="total-center container-logo-modal"
          style={{ flexDirection: "column" }}
        >
          <img
            src="../assets/images/logo-completo.png"
            className="header-brand-img "
            alt="Covido logo"
          />
        </div>
        <div className="body-modal">
          {loading ? (
            <div className="total-center">
              <Loader />
            </div>
          ) : (
            <>
              {movement && (
                <>
                  <h3>{title}</h3>

                  <div className="content_item-table mt-30">
                    <div className="item-table">
                      <div>Importe</div>
                      <div>{formatCurrency(movement.amount)}$</div>
                    </div>
                    <div className="item-table">
                      <div>Fecha</div>
                      <div>{moment(movement.date).format("DD-MM-YYYY")}</div>
                    </div>
                    <div className="item-table">
                      <div>Tipo</div>
                      <div>
                        {movement.type !== "performance" && (
                          <Badge color="success">
                            Rendimiento obtenido por referidos
                          </Badge>
                        )}

                        {movement.type === "performance" && (
                          <Badge color="success">Rendimiento diario</Badge>
                        )}
                      </div>
                    </div>
                    <div className="item-table">
                      <div>Contenido</div>
                      {movement.type !== "performance" && (
                        <div>
                          Este es el detalle correspondiente a los rendimientos
                          obtenidos gracias a lo generado por tus referidos el
                          día {moment(movement.date).format("DD-MM-YYYY")}
                        </div>
                      )}
                      {movement.type === "performance" && (
                        <div>
                          Este movimiento corresponde al rendimiento generado el
                          día {moment(movement.date).format("DD-MM-YYYY")}
                        </div>
                      )}
                    </div>
                  </div>

                  {movement.type !== "performance" && (
                    <div className="container-breakdown mt-40">
                      <h4>
                        Desglose del día{" "}
                        {moment(movement.date).format("DD-MM-YYYY")}
                      </h4>
                      <Table responsive hover className="mt-10">
                        <thead>
                          <tr>
                            <th>Nivel</th>
                            <th>Fecha</th>
                            <th>Compras</th>
                          </tr>
                        </thead>
                        <tbody>
                          {movement.data && Array.isArray(movement.data) && (
                            <>
                              {movement.data.map((item, key) => {
                                total = total + parseFloat(item.total);
                                return (
                                  <tr key={key}>
                                    <th scope="row">
                                      {item.level ? item.level : "-"}
                                    </th>
                                    <td>
                                      {moment(item.date).format("DD-MM-YYYY")}
                                    </td>
                                    <td>{formatCurrency(item.total)}$</td>
                                  </tr>
                                );
                              })}
                            </>
                          )}
                        </tbody>
                        <tfoot>
                          <tr>
                            <td>
                              <b>TOTALES</b>
                            </td>
                            <td></td>
                            <td>{formatCurrency(total)}$</td>
                          </tr>
                        </tfoot>
                      </Table>
                    </div>
                  )}
                </>
              )}
            </>
          )}

          <div className="mt-10">
            <button
              onClick={handleClose}
              className="button-k-primary total-center"
            >
              OK
            </button>
          </div>
        </div>
      </div>
    </Modal>
  );
};
