import React, { useRef, useEffect, useState } from "react";
import QRCode from "react-qr-code";
import moment from "moment-timezone";
import { formatCurrency, updateCountdown } from "./../../utils";
import { Badge, Alert } from "reactstrap";

const Status = {
  pending: { color: "info", title: "Pendiente" },
  confirming: { color: "primary", title: "Confirmando" },
  success: { color: "success", title: "Aprobado" },
  expired: { color: "light", title: "Expirado" },
  incomplete: { color: "light", title: "Incompleto" },
  failed: { color: "danger", title: "Fallido" },
};

const MakePayment = ({
  btc,
  wallet,
  end,
  status,
  remaining,
  txs,
  date,
  amount,
  transaction_id,
  RemainingDaysDelayWhitdrawal,
  withdrawal,
  txid,
  handleClose,
  showOK,
  expire,
  isBuy,
}) => {
  const elementDiv = useRef(null);
  const elementWallet = useRef(null);
  const [confirmingFecha, setConfirmingFecha] = useState("");
  const [timeEnded, setTimeEnded] = useState(false);
  useEffect(() => {
    let interval = null;

    if (end && btc && wallet) {
      interval = setInterval(() => {
        const callFinalized = () => {
          setTimeEnded(true);
          clearInterval(interval);
        };

        const callRunning = ({
          REMAINING_DAYS,
          REMAINING_HOURS,
          REMAINING_MINUTES,
          REMAINING_SECONDS,
        }) => {
          if (elementDiv.current) {
            elementDiv.current.innerHTML = `Tienes: <br/>${
              REMAINING_DAYS ? `${REMAINING_DAYS} Dias,` : ""
            } ${REMAINING_HOURS} Horas, ${REMAINING_MINUTES} Minutos y ${REMAINING_SECONDS} Segundos, <b class="text-highlighted">para enviar los BTC</b>`;
          }
        };
        updateCountdown({ endDate: end, callFinalized, callRunning });
      }, 1000);
    }

    return () => clearInterval(interval);
  }, [btc, wallet, end, status, remaining, txs]);

  const copyWallet = () => {
    if (elementWallet && elementWallet.current) {
      const aux = document.createElement("input");
      aux.setAttribute("value", elementWallet.current.textContent);
      document.body.appendChild(aux);
      aux.select();
      document.execCommand("copy");
      document.body.removeChild(aux);
    }
  };

  useEffect(() => {
    if (date && date.date) {
      setConfirmingFecha(moment(date.date).format("DD-MM-YYYY"));
    } else {
      setConfirmingFecha(moment(date).format("DD-MM-YYYY"));
    }
  }, [date]);

  const handleRenderHead = (status) => {
    if (status === "incomplete") {
      return (
        <>
          <h3>DEPÓSITO: ID-{transaction_id}</h3>
          <div className="content_item-table mt-30">
            <div className="item-table">
              <div>FECHA:</div>
              <div>{confirmingFecha}</div>
            </div>
            <div className="item-table">
              <div>ESTADO:</div>
              <div>
                <Badge color={status && Status[status] && Status[status].color}>
                  {status &&
                    Status[status] &&
                    Status[status].title.toUpperCase()}
                </Badge>
              </div>
            </div>
          </div>

          <div className="mt-20">
            <h4>
              Este depósito se encuentra en estado{" "}
              <b className="text-highlighted">INCOMPLETO</b> debido a que se ha
              realizado una transacción por un importe menor al solicitado{" "}
              <b className="text-highlighted">{btc} BTC</b>
            </h4>
          </div>

          <div className="mt-5 mb-6">
            <h4>
              Para poder validar la transacción, es necesario que realices una
              nueva transferencia de:{" "}
              <b className="text-highlighted">{remaining} BTC</b> a la siguiente
              dirección:
            </h4>
          </div>
        </>
      );
    } else if (status === "pending") {
      return (
        <>
          <h3>DEPÓSITO: {transaction_id ? `ID-${transaction_id}` : ""}</h3>
          {timeEnded === false && (
            <div className="mt-20 mb-20">
              <h4>
                Por favor envia <b className="text-highlighted">{btc}</b> BTC a
                la siguiente dirección:
              </h4>
            </div>
          )}
        </>
      );
    } else if (
      status === "confirming" ||
      (status === "success" && !withdrawal)
    ) {
      return (
        <>
          <h3>{ !isBuy ? "DEPOSITO" : "COMPUESTO" }: ID-{transaction_id}</h3>

          <div className="content_item-table mt-30">
            <div className="item-table">
              <div>FECHA:</div>
              <div>{confirmingFecha}</div>
            </div>
            <div className="item-table">
              <div>IMPORTE:</div>
              <div>{formatCurrency(amount)}$</div>
            </div>
            <div className="item-table">
              <div>ESTADO:</div>
              <div>
                <Badge color={status && Status[status] && Status[status].color}>
                  {status &&
                    Status[status] &&
                    Status[status].title.toUpperCase()}
                </Badge>
              </div>
            </div>
          </div>

          <div className="mt-20">
            <h4>
              {
                !isBuy
                  ? `Su transacción
                    ${status === "confirming" ? "esta siendo confirmada" : "fue aprobada"}
                    , puedes comprobar su estado en el siguiente listado:`
                  : 'Compra por Interés Compuesto'
              }
            </h4>
          </div>
          {Array.isArray(txs) ? (
            <ul className="list-group word-wrap">
              {txs.map((item) => (
                <a
                  href={`https://www.blockchain.com/es/btc/tx/${item.txid}`}
                  rel="noopener noreferrer"
                  target="_blank"
                  key={item.txid}
                  className="list-group-item"
                >
                  {item.txid}
                </a>
              ))}
            </ul>
          ) : null}

          {status === "success" && (
            <>
              <Alert color="success" className="mt-20">
                Este deposito podrá ser retirado en{" "}
                {RemainingDaysDelayWhitdrawal} dias.
              </Alert>
            </>
          )}
        </>
      );
    } else if (withdrawal) {
      return (
        <>
          <h3>RETIRO: ID-{transaction_id}</h3>

          <div className="content_item-table mt-30">
            <div className="item-table">
              <div>FECHA:</div>
              <div>{confirmingFecha}</div>
            </div>
            <div className="item-table">
              <div>IMPORTE:</div>
              <div>{formatCurrency(amount)}$</div>
            </div>
            <div className="item-table">
              <div>ESTADO:</div>
              <div>
                <Badge color={status && Status[status] && Status[status].color}>
                  {status &&
                    Status[status] &&
                    Status[status].title.toUpperCase()}
                </Badge>
              </div>
            </div>
          </div>

          <div style={{ marginTop: "30px", display: status !== "success" ? "none" : "flex" }} className="wallet-code-copy">
            <code className="wallet" ref={elementWallet}>
              {wallet}
            </code>

            <div className="copy-wallet" onClick={copyWallet}>
              Copiar
            </div>
          </div>
          <div className="mt-20">
            <h4>
              {
                status !== "success"
                  ? ''
                  : `Su transacción
                    ${status === "confirming" ? "esta siendo confirmada" : "fue aprobada"}
                    , puedes comprobar su estado en el siguiente enlace:`
              }
            </h4>
          </div>

          <ul style={{ display: status !== "success" ? "none" : "block" }} className="list-group word-wrap mt-20">
            <a
              href={`https://www.blockchain.com/es/btc/tx/${txid}`}
              rel="noopener noreferrer"
              target="_blank"
              key={txid}
              className="list-group-item"
            >
              {txid}
            </a>
          </ul>
        </>
      );
    } else if (status === "expired") {
      return (
        <>
          <h3>DEPÓSITO: ID-{transaction_id}</h3>
          <div className="content_item-table mt-30">
            <div className="item-table">
              <div>FECHA:</div>
              <div>{confirmingFecha}</div>
            </div>
            <div className="item-table">
              <div>IMPORTE:</div>
              <div>{formatCurrency(amount)}$</div>
            </div>
            <div className="item-table">
              <div>ESTADO:</div>
              <div>
                <Badge color={status && Status[status] && Status[status].color}>
                  {status &&
                    Status[status] &&
                    Status[status].title.toUpperCase()}
                </Badge>
              </div>
            </div>
          </div>
        </>
      );
    } else if (status === "failed") {
      return (
        <>
          <h3>DEPÓSITO: ID-{transaction_id}</h3>
          <div className="content_item-table mt-30">
            <div className="item-table">
              <div>FECHA:</div>
              <div>{confirmingFecha}</div>
            </div>
            <div className="item-table">
              <div>IMPORTE:</div>
              <div>{formatCurrency(amount)}$</div>
            </div>
            <div className="item-table">
              <div>ESTADO:</div>
              <div>
                <Badge color={status && Status[status] && Status[status].color}>
                  {status &&
                    Status[status] &&
                    Status[status].title.toUpperCase()}
                </Badge>
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <>
          <h3>DEPÓSITO: {transaction_id ? `ID-${transaction_id}` : ""}</h3>
          <div className="mt-20 mb-20">
            <h4>
              Por favor envia <b className="text-highlighted">{btc}</b> BTC a la
              siguiente dirección:
            </h4>
          </div>
        </>
      );
    }
  };

  return (
    <>
      <div className="body-modal">
        {btc === null && status !== "failed" ? (
          <>
            <h3>DEPÓSITO REALIZADO POR LA ADMINISTRACIÓN</h3>

            <div className="content_item-table mt-30">
              <div className="item-table">
                <div>IMPORTE:</div>
                <div>{formatCurrency(amount)}$</div>
              </div>
              <div className="item-table">
                <div>FECHA:</div>
                <div>{confirmingFecha}</div>
              </div>
            </div>
          </>
        ) : (
          <>
            {handleRenderHead(status)}
            {status !== "confirming" &&
            status !== "success" &&
            status !== "expired" &&
            status !== "failed" ? (
              <>
                {timeEnded === false && (
                  <>
                    <div className="text-center">
                      {wallet ? <QRCode size={200} value={wallet} /> : null}
                    </div>
                    <br />
                    <div className="wallet-code-copy">
                      <code className="wallet" ref={elementWallet}>
                        {wallet}
                      </code>

                      <div className="copy-wallet" onClick={copyWallet}>
                        Copiar
                      </div>
                    </div>
                    <div className="mt-10 mb-10" ref={elementDiv}></div>
                    {status === "pending" && (
                      <div>
                        <Alert color="danger">
                          Esta es una transacción pendiente, puede procesarla
                          enviado la cantidad solicitada o eliminarla sí desea
                          realizar otra.
                        </Alert>
                      </div>
                    )}
                  </>
                )}

                {timeEnded === true && (
                  <>
                    <p>
                      El periodo de tiempo estimado para realizar la transacción
                      se ha terminado. Si usted ya realizó la transferencia,
                      espere a que el sistema la detecte, para ser procesada.
                    </p>{" "}
                    <br />
                    <p>
                      Si no realizó la transacción, elimine este depósito y
                      realice uno nuevamente.
                    </p>
                    <br />
                    <Alert color="danger">
                      Advertencia: No elimine este depósito si ya realizó la
                      transferencia o es posible que el sistema no la detecte.
                    </Alert>
                    <p>
                      Este depósito expira el:{" "}
                      {expire
                        ? `${moment(expire).format(
                            "DD-MM-YYYY"
                          )} a las ${moment(expire).format("HH:mm:ss")}`
                        : ""}
                    </p>
                  </>
                )}

                {typeof handleClose === "function" && showOK && (
                  <div className="mt-10 mb-10">
                    <button
                      onClick={handleClose}
                      className="button-k-primary total-center"
                    >
                      OK
                    </button>
                  </div>
                )}
              </>
            ) : null}
          </>
        )}
      </div>
    </>
  );
};

export default MakePayment;
