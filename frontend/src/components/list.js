import React from "react";

const List = ({ children }) => {
  return (
    <div className="table-responsive">
      <table className="table table-striped card-table table-vcenter text-nowrap mb-0">
        {children}
      </table>
    </div>
  );
};

export default List;
