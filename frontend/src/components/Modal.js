import React, { useEffect } from "react";
import { Modal } from "reactstrap";

const InfoModal = ({ isOpen, children, size }) => {
  useEffect(() => {
    document.querySelector("html").style.overflowY = isOpen ? "hidden" : "auto";
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen} size={size}>
      {children}
    </Modal>
  );
};

export default InfoModal;
