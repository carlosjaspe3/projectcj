import React from "react";
import styled from "styled-components";

const Closed = () => (
  <Container>
    <Logo src="/assets/images/logo-sherpa.svg" alt="logo-sherpa" />
    <Text>
      Debido a que los mercados de divisas permanecen cerrados los fines de
      semana, nuestro sistema no está realizando operaciones en este momento.
    </Text>
  </Container>
);

const Container = styled.div`
  background-image: url("assets/images/bg-closed.jpg");
  background-position: center;
  background-size: cover;
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
const Logo = styled.img`
  width: auto;
  height: 150px;
  object-fit: contain;
  margin-bottom: 50px;
  @media (max-width: 520px) {
    width: 80vw;
    height: auto;
    margin-bottom: 10px;
  }
`;
const Text = styled.p`
  width: 100%;
  max-width: 800px;
  margin-top: 20px;
  font-family: "Poppins";
  font-weight: 600;
  font-size: 18px;
  line-height: 1.5;
  color: #1d2b45;
  text-align: center;
  @media (max-width: 768px) {
    width: 95vw;
    font-size: 14px !important;
  }
  @media (max-width: 420px) {
    font-size: 12px !important;
    width: 80vw;
  }
`;

export default Closed;
