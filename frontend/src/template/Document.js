import React from "react";
import { useSelector, useDispatch } from "react-redux";
import Layout from "../components/layout/Index";
import { documentInit } from "../ducks/general/actions";
import {
  documentInSelector,
  isLoggingDocumentInSelector,
} from "../ducks/general/selectors";
import Loader from "../components/Loader";
import { BASE_URL_DOCUMENT } from "../constants";

const setDocument = (Response) => {
  if (!Response.bool) {
    document.getElementById("title").value = Response.message;
    document.getElementById("message").value = "";
    document.getElementById("type").value = "error";
    document.getElementById("click").click();
  }
};

const Document = () => {
  const dispatch = useDispatch();
  const document = useSelector(documentInSelector);
  const isLoggingDocument = useSelector(isLoggingDocumentInSelector);

  React.useEffect(() => {
    dispatch(
      documentInit((Response) => {
        setDocument(Response);
      })
    );
  }, [dispatch]);

  return (
    <div className="app sidebar-mini" id="AppSidebar">
      <Layout>
        <div className="page-header">
          <div className="page-leftheader">
            <h4 className="page-title">Documentos</h4>
          </div>
        </div>
        <div className="card padding-20">
          <div className="page-leftheader">
            {isLoggingDocument && document ? (
              <div
                className="card-header total-center"
                style={{ height: "200px" }}
              >
                <Loader />
              </div>
            ) : (
              <div className="row ">
                {document && Array.isArray(document) && document.length ? (
                  document.map((item, key) => {
                    return (
                      <div className="col-xl-6 col-lg-12" key={key}>
                        <a
                          className="car-document"
                          href={`${BASE_URL_DOCUMENT}${item.doc}`}
                          rel="noreferrer"
                          target="_blank"
                        >
                          <div>
                            <div
                              className="total-center"
                              style={{ marginTop: "10px" }}
                            >
                              <i className="fa fa-file-pdf-o container-icon" />
                            </div>
                            <div>
                              <p className="title-document">
                                {item && item.description}
                              </p>
                            </div>
                          </div>
                        </a>
                      </div>
                    );
                  })
                ) : (
                  <div className="col-12 total-center">
                    <p className="font-weight-bold">
                      Actualmente no hay documentos cargados.
                    </p>
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </Layout>
    </div>
  );
};

export default Document;
