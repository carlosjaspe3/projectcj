import React from "react";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { Line, Doughnut } from "react-chartjs-2";
import Layout from "../../components/layout/Index";
import {
  getPerformanceInit,
  getPerformanceDashboardInit,
} from "../../ducks/graphics/actions";
import {
  isLoggingInSelector,
  graphicPlatformPerformanceSelector,
  isLoggingDashboardInSelector,
  performanceLast30DaysSelector,
  infoTotalSelector,
} from "../../ducks/graphics/selectors";
import List from "../../components/list";
import Loader from "../../components/Loader";
import ChartBar from "./ChartBar";
import { formatCurrency } from "./../../utils";

const Home = () => {
  const graphicPlatformPerformance = useSelector(
    graphicPlatformPerformanceSelector
  );
  const isLoadingIn = useSelector(isLoggingInSelector);
  const isLoadingDashboardIn = useSelector(isLoggingDashboardInSelector);
  const performanceLast30Days = useSelector(performanceLast30DaysSelector);
  const infoTotal = useSelector(infoTotalSelector);
  const percentagePlatformReferral = useSelector(
    (state) => state.graphicsStore.percentageRefered
  );
  const [showList, setShowList] = React.useState(false);
  const dispatch = useDispatch();
  React.useEffect(() => {
    const date = {
      initial: moment(new Date()).format("YYYY-MM-DD"),
      end: moment(new Date()).subtract(10, "days").format("YYYY-MM-DD"),
    };
    dispatch(
      getPerformanceDashboardInit(
        moment(new Date(), "YYYY-MM").daysInMonth(),
        (Response) => {
          if (!Response.bool) {
            document.getElementById("title").value = Response.message;
            document.getElementById("message").value = "";
            document.getElementById("type").value = "error";
            document.getElementById("click").click();
          }
        }
      )
    );
    dispatch(
      getPerformanceInit(date, (Response) => {
        if (!Response.bool) {
          document.getElementById("title").value = Response.message;
          document.getElementById("message").value = "";
          document.getElementById("type").value = "error";
          document.getElementById("click").click();
        }
      })
    );
  }, [dispatch]);

  const handleClickShowList = () => {
    setShowList(!showList);
  };
  // const porcentageRendimiento =
  //   infoTotal && infoTotal.performanceTotal
  //     ? infoTotal.performanceTotal /
  //       parseFloat(infoTotal.performanceByBuy).toFixed(2)
  //     : 0;
  
  const porcentageRendimiento =
      infoTotal && infoTotal.totalBuy
        ? (parseFloat(infoTotal.totalBuy) / parseFloat(infoTotal.performanceTotal))*100
        : 0;
  //const porcentageInvestment = 100;
  const porcentageInvestment = 100-porcentageRendimiento;

  const getTextTransaction = (transaction) => {
    let text = "";
    if (transaction.buy) {
      text = "Compuesto";
    } else if (transaction.type === "performance") {
      text = "Rendimiento diario";
    } else if (transaction.type === "referral") {
      text = "Rendimiento por referidos";
    } else if (transaction.type === "deposit") {
      text = "Deposito";
    } else if (transaction.type === "withdrawal") {
      text = "Retiro";
    }
    return text;
  };

  return (
    <div className="app sidebar-mini" id="AppSidebar">
      <Layout>
        <div>
          <div className="wrap-chart card">
            <div className="container-card-performance">
              <div className="card-performance card-performance-total">
                <p className="card-font-size">Rendimientos totales</p>
                <div className="">
                  <p className="card-text-size">
                    {infoTotal && infoTotal.performanceTotal
                      ? formatCurrency(infoTotal.performanceTotal)
                      : "0"}{" USD"}
                  </p>
                </div>
                <img
                  src="/assets/images/chart-3.svg"
                  alt="chart"
                  width="100%"
                  className="chart-1-card-performance"
                />
              </div>
              <div className="card-performance card-performance-shopping">
                <p className="card-font-size">Rendimientos de mis compras</p>
                <div className="">
                  <p className="card-text-size">
                    {infoTotal && infoTotal.performanceByBuy
                      ? formatCurrency(infoTotal.performanceByBuy)
                      : "0"}{" USD"}
                  </p>
                </div>
                <img
                  src="/assets/images/chart-2.svg"
                  alt="chart"
                  width="100%"
                  className="chart-1-card-performance"
                />
              </div>
              <div className="card-performance card-performance-referido">
                <p className="card-font-size">Rendimientos de mis referidos</p>
                <div className="">
                  <p className="card-text-size">
                    {infoTotal &&
                    infoTotal.performanceByRefered &&
                    infoTotal.performanceByRefered.length > 6
                      ? formatCurrency(infoTotal.performanceByRefered)
                      : "0"}{" USD"}
                  </p>
                </div>
                <img
                  src="/assets/images/chart-1.svg"
                  alt="chart"
                  width="100%"
                  className="chart-1-card-performance"
                />
              </div>
            </div>
            <div>
              <div className="card-header">
                <h4 className="page-title">Rendimiento diario del fondo</h4>
              </div>
              <div className="total-center container-graphics">
                {isLoadingIn || isLoadingIn === undefined ? (
                  <Loader />
                ) : (
                  <Line
                    data={{
                      datasets: [
                        {
                          label: "Porcentaje repartido",
                          data:
                            graphicPlatformPerformance &&
                            graphicPlatformPerformance.value
                              ? graphicPlatformPerformance.value
                              : [],
                          borderColor: "#E5007E",
                          backgroundColor: "#e5007e87",
                          borderWidth: 0.7,
                        },
                      ],
                      labels:
                        graphicPlatformPerformance &&
                        graphicPlatformPerformance.labels
                          ? graphicPlatformPerformance.labels
                          : [],
                    }}
                    options={{
                      tooltips: {
                        callbacks: {
                          title: (items, data) => data.labels[items[0].index],
                          label: (items, data) =>
                            `Porcentaje repartido: ${data.datasets[
                              items.datasetIndex
                            ].data[items.index]
                              .toString()
                              .substring(0, 5)}%`,
                        },
                      },
                      responsive: true,
                      title: {
                        display: false,
                      },
                      legend: {
                        display: false,
                      },
                      scales: {
                        xAxes: [
                          {
                            ticks: {
                              display: false,
                            },
                            gridLines: {
                              display: false,
                              drawBorder: false,
                            },
                          },
                        ],
                        yAxes: [
                          {
                            ticks: {
                              min: 0,
                              max: 1.6,
                              callback: function (label) {
                                const labelValue = label;
                                if (label === 0) {
                                  return label;
                                } else if (labelValue.toString().length > 4) {
                                  const labels = labelValue
                                    .toString()
                                    .substring(0, 4);
                                  return labels;
                                } else {
                                  return labelValue;
                                }
                              },
                            },
                            gridLines: {
                              display: false,
                              drawBorder: false,
                            },
                          },
                        ],
                      },
                    }}
                  />
                )}
              </div>
            </div>
          </div>
          <div className="card">
            <div>
              <div className="card-header">
                <div>
                  <h4 className="page-title">{`Rendimientos últimos ${moment(
                    new Date(),
                    "YYYY-MM"
                  ).daysInMonth()} días`}</h4>
                  <p>
                    Rendimientos generadas por mis compras durante los últimos{" "}
                    {moment(new Date(), "YYYY-MM").daysInMonth()} días.
                  </p>
                </div>
              </div>
              {isLoadingDashboardIn || isLoadingDashboardIn === undefined ? (
                <Loader />
              ) : (
                <div style={{ padding: "20px" }}>
                  {performanceLast30Days &&
                  performanceLast30Days.data.length ? (
                    <ChartBar />
                  ) : (
                    <div className="total-center">
                      <p className="font-weight-bold">
                        No has realizado ninguna compra.
                      </p>
                    </div>
                  )}
                </div>
              )}
            </div>
          </div>
          {performanceLast30Days && performanceLast30Days.data.length ? (
            <div className="card">
              <div className="card-header">
                <div className="row" style={{ width: "100%" }}>
                  <div className="col-xl-6 col-lg-12">
                    <div>
                      <h4 className="page-title">Rendimiento de mis compras</h4>
                      <Doughnut
                        data={{
                          datasets: [
                            {
                              data: [
                                parseFloat(porcentageInvestment).toFixed(2),
                                parseFloat(porcentageRendimiento).toFixed(2),
                              ],
                              backgroundColor: ["#1d2b45", "#3f768a"],
                            },
                          ],
                          labels: ["Mis rendimientos", "Mis compras"],
                        }}
                        options={{
                          tooltips: {
                            callbacks: {
                              title: (items, data) =>
                                data.labels[items[0].index],
                              label: (items, data) =>
                                `${
                                  data.labels[
                                    items.index
                                  ] === "Mis compras"
                                    ? `${
                                        infoTotal && infoTotal.totalBuy
                                          ? formatCurrency(infoTotal.totalBuy)
                                          : "0"
                                      }`
                                    : `${
                                        infoTotal && infoTotal.performanceTotal
                                          ? formatCurrency(
                                              infoTotal.performanceTotal
                                            )
                                          : "0"
                                      }`
                                }`,
                            },
                          },
                          responsive: true,
                        }}
                      />
                    </div>
                  </div>
                  <div className="col-xl-6 col-lg-12">
                    <div>
                      <h4 className="page-title">
                        Procedencias de los rendimientos
                      </h4>
                      <Doughnut
                        data={{
                          datasets: [
                            {
                              data: [
                                parseFloat(infoTotal.performanceByBuy).toFixed(2),
                                parseFloat(percentagePlatformReferral).toFixed(2),
                              ],
                              backgroundColor: ["#1d2b45", "#e5007e"],
                            },
                          ],
                          labels: [
                            "Rendimiento de mis compras",
                            "Rendimiento por mis referidos",
                          ],
                        }}
                        options={{
                          tooltips: {
                            callbacks: {
                              title: (items, data) =>
                                data.labels[items[0].index] ===
                                "Rendimiento de mis compras"
                                  ? `${data.labels[items[0].index]}`
                                  : `${data.labels[items[0].index]}`,
                              label: (items, data) =>
                                `${formatCurrency(
                                  data.datasets[items.datasetIndex].data[
                                    items.index
                                  ]
                                )}`,
                            },
                          },
                          responsive: true,
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          ) : null}
          {infoTotal &&
          infoTotal.performanceUser &&
          Array.isArray(infoTotal.performanceUser) &&
          infoTotal.performanceUser.length ? (
            <div className="card" style={{ marginBottom: "25px" }}>
              <div className="show-table">
                <p style={{ cursor: "pointer" }} onClick={handleClickShowList}>
                  {!showList ? "Ver todos" : "Mostrar menos"}
                </p>
              </div>
              <div className="card-header">
                <div>
                  <h4 className="page-title">Movimientos</h4>
                  <p>Estos son los últimos movimientos de tu cuenta.</p>
                </div>
              </div>
              <div style={{ paddingBottom: "30px" }} className="table-show">
                <List>
                  <thead className="background-primary text-white">
                    <tr>
                      <th className="text-white">Fecha</th>
                      <th className="text-white">Importe</th>
                      <th className="text-white">Tipo</th>
                    </tr>
                  </thead>
                  <tbody>
                    {infoTotal &&
                    infoTotal.transactions &&
                    Array.isArray(infoTotal.transactions)
                      ? infoTotal.transactions
                          .slice(
                            0,
                            !showList ? 10 : infoTotal.transactions.length
                          )
                          .map((item, key) => {
                            return (
                              <tr key={key}>
                                <th>
                                  {moment(item.date)
                                    .zone(-120)
                                    .format("DD-MM-YYYY")}
                                </th>
                                <th>{formatCurrency(item.amount)}</th>
                                <th>{getTextTransaction(item)}</th>
                              </tr>
                            );
                          })
                      : null}
                  </tbody>
                </List>
              </div>
            </div>
          ) : null}
        </div>
      </Layout>
    </div>
  );
};

export default Home;
