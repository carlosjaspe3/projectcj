import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Bar } from "react-chartjs-2";
import { formatCurrency } from "./../../utils";

const ChartBar = () => {
  const chartBar = useSelector((state) => state.graphicsStore.chartBar);

  const [data, setData] = useState({
    labels: [],
    datasets: [
      {
        label: "Rendimiento diario",
        data: [],
        backgroundColor: "#1d2b45",
      },
      {
        label: "Rendimiento diario por referidos",
        data: [],
        backgroundColor: "#e5007e",
      },
    ],
  });

  useEffect(() => {
    if (
      chartBar &&
      chartBar.labels &&
      chartBar.performanceUsers &&
      chartBar.performanceUserReferrals
    ) {
      setData({
        labels: chartBar.labels,
        datasets: [
          {
            label: "Rendimiento diario",
            data: chartBar.performanceUsers,
            backgroundColor: "#1d2b45",
          },
          {
            label: "Rendimiento diario por referido",
            data: chartBar.performanceUserReferrals,
            backgroundColor: "#e5007e",
          },
        ],
      });
    }
  }, [setData, chartBar]);

  const options = {
    tooltips: {
      callbacks: {
        label: (items, data) =>
          `${data.datasets[items.datasetIndex].label} ${formatCurrency(
            data.datasets[items.datasetIndex].data[items.index].toString()
          )}`,
      },
    },
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  };

  return (
    <>
      <Bar data={data} options={options} />
    </>
  );
};

export default ChartBar;
