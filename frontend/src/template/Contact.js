import React from "react";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import Layout from "../components/layout/Index";
import { profileSelector } from "../ducks/profile/selectors";
import { postContactInit } from "../ducks/general/actions";
import { isLoggingInSelector } from "../ducks/general/selectors";
import Button from "../components/Button";

const Contact = () => {
  const profile = useSelector(profileSelector);
  const dispatch = useDispatch();
  const isLoading = useSelector(isLoggingInSelector);
  const handleClickSend = (values, { resetForm }) => {
    const body = {
      email: values.email,
      type: values.typeValue,
      subject: values.affair,
      message: values.description,
    };
    dispatch(
      postContactInit(body, (Response) => {
        console.log(Response)
        if (Response.bool) {
          resetForm();
          document.getElementById("title").value = "Soporte enviado!";
          document.getElementById("message").value =
            "Su soporte sera respondido por correo electrónico.";
          document.getElementById("type").value = "success";
          document.getElementById("click").click();
        } else {
          document.getElementById("title").value = Response.message;
          document.getElementById("message").value = "";
          document.getElementById("type").value = "error";
          document.getElementById("click").click();
        }
      })
    );
  };
  return (
    <div className="app sidebar-mini" id="AppSidebar">
      <Layout>
        <div className="page-header">
          <div className="page-leftheader">
            <h4 className="page-title">Soporte</h4>
          </div>
        </div>
        <div className="card">
          <div className="card-header">
            <div className="row" style={{ width: "100%" }}>
              <div className="col-xl-12 col-lg-12">
                <Formik
                  initialValues={{
                    email: profile && profile.email ? profile.email : "",
                    typeValue: "",
                    affair: "",
                    description: "",
                  }}
                  onSubmit={handleClickSend}
                  validationSchema={Yup.object({
                    email: Yup.string()
                      .required("El campo es requerido*")
                      .email("Ingrese un correo electrónico válido*"),
                    affair: Yup.string()
                      .min(4, "Mínimo 4 caracteres*")
                      .required("El campo es requerido*"),
                    typeValue: Yup.string().required("El campo es requerido*"),
                    description: Yup.string()
                      .min(10, "Mínimo 10 caracteres*")
                      .required("El campo es requerido*"),
                  })}
                >
                  {({ values, handleChange, handleBlur }) => (
                    <Form>
                      <div className="row">
                        {[
                          {
                            title: "Correo electrónico",
                            id: "email",
                            type: "text",
                            disabled: true,
                          },
                          {
                            title: "Tipo",
                            id: "typeValue",
                            type: "select",
                          },
                          {
                            title: "Asunto",
                            id: "affair",
                            type: "text",
                          },
                          {
                            title: "Descripción",
                            id: "description",
                            type: "textarea",
                          },
                        ].map((form, key) => {
                          if (
                            form.type !== "select" &&
                            form.type !== "textarea"
                          ) {
                            return (
                              <div
                                className={
                                  form.id === "affair"
                                    ? "col-md-12"
                                    : "col-sm-6 col-md-6"
                                }
                                key={key}
                              >
                                <div className="form-group">
                                  <label className="form-label">
                                    {form.title}
                                  </label>
                                  <div className="width">
                                    <input
                                      className="form-control"
                                      placeholder={form.title}
                                      id={form.id}
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                      value={values[form.id]}
                                      type={form.type}
                                      disabled={form.disabled}
                                    />
                                  </div>
                                  <ErrorMessage
                                    name={form.id}
                                    component="div"
                                    className="textErrorLabel"
                                  />
                                </div>
                              </div>
                            );
                          }
                          if (
                            form.type !== "textarea" &&
                            form.type === "select"
                          ) {
                            return (
                              <div className="col-sm-6" key={key}>
                                <div className="form-group">
                                  <label className="form-label">Tipo</label>
                                  <select
                                    className="form-control custom-select select2"
                                    id={form.id}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values[form.id]}
                                    type="select"
                                  >
                                    <option value="">--Seleccione--</option>
                                    <option value="Depósitos">Depósitos</option>
                                    <option value="Retiros">Retiros</option>
                                    <option value="Dudas">Dudas</option>
                                  </select>
                                  <ErrorMessage
                                    name={form.id}
                                    component="div"
                                    className="textErrorLabel"
                                  />
                                </div>
                              </div>
                            );
                          }
                          return (
                            <div className="col-lg" key={key}>
                              <div className="form-group">
                                <textarea
                                  className="form-control mb-4"
                                  placeholder={form.title}
                                  rows="3"
                                  id={form.id}
                                  style={{ marginBottom: "0px !important" }}
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values[form.id]}
                                  type={form.type}
                                />
                                <ErrorMessage
                                  name={form.id}
                                  component="div"
                                  className="textErrorLabel"
                                />
                              </div>
                            </div>
                          );
                        })}
                      </div>
                      <div className="text-right">
                        <Button
                          type="submit"
                          className={` ${
                            isLoading
                              ? "btn btn-loading"
                              : "text-btn"
                          }`}
                        >
                          {isLoading ? (
                            <div className="btn" />
                          ) : (
                            "Enviar"
                          )}
                        </Button>
                      </div>
                    </Form>
                  )}
                </Formik>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </div>
  );
};

export default Contact;
