import React, { useEffect, useState, useCallback } from "react";
import moment from "moment";
import ReactPaginate from "react-paginate";
import Layout from "../components/layout/Index";
import List from "../components/list";
import Loader from "../components/Loader";
import Botton from "./../components/Button";
import API from "./../api";
import Modal from "./../components/Modal";
import MakePayment from "./../components/MakePayment";
import { useMovementsDepositos } from "./../hooks/useMovementsDepositos";
import { formatCurrency, searchToObject } from "./../utils";
import Swal from "sweetalert2";

const Movements = () => {
  const typeApi = {
    deposit: { title: "Deposito" },
    withdrawal: { title: "Retiro" },
    buy: { title: "Compuesto" },
  };
  const Status = {
    pending: { title: "Pendiente" },
    confirming: { title: "Confirmando" },
    success: { title: "Aprobado" },
    expired: { title: "Expirado" },
    incomplete: { title: "Incompleto" },
    failed: { title: "Fallido" },
  };

  const [page, setPage] = useState(0);
  const [showModalMovement, setShowModalMovement] = React.useState(false);
  const [loadingMovement, setLoadingMovement] = React.useState(false);
  const [movement, setMovement] = React.useState({
    amount: null,
    btc: null,
    wallet: null,
    date: null,
    end: null,
    status: null,
    remaining: null,
    txs: [],
    transaction_id: null,
    isBuy: false,
  });
  const [type, setType] = React.useState("");
  const [status, setStatus] = React.useState("");
  const [dateInit, setDateInit] = React.useState("");
  const [dateEnd, setDateEnd] = React.useState("");
  const [errorDateInit, setErrorDateInit] = React.useState("");
  const [errorDateEnd, setErrorDateEnd] = React.useState("");
  const {
    movements,
    isLoggingInMovements,
    getMovements,
  } = useMovementsDepositos(page, 10, type, status, dateInit, dateEnd);

  const handleFetch = (type, status, dateInit, dateEnd) => {
    console.log({ type, status, dateInit, dateEnd });
  };
  const handleChangeType = (event) => {
    setType(event.target.value);
    if (type !== "deposit") {
      setStatus("");
    } else if (type !== "withdrawal") {
      setStatus("");
    } else if (type !== "buy") {
      setStatus("");
    }
  };
  const handleChangeStatus = (event) => {
    setStatus(event.target.value);
  };
  const handleDateInit = (event) => {
    setErrorDateInit("");
    setDateInit(event.target.value);
    if (moment(dateEnd ? dateEnd : new Date()).isAfter(event.target.value)) {
      handleFetch(type, status, event.target.value, dateEnd);
    } else if (dateInit === "") {
      handleFetch(type, status, event.target.value, dateEnd);
    } else {
      setErrorDateInit(
        "La fecha de inicio no puede ser mayor a la fecha final*"
      );
    }
  };
  const handleDateEnd = (event) => {
    setErrorDateEnd("");
    setDateEnd(event.target.value);
    if (moment(dateInit).isBefore(event.target.value)) {
      handleFetch(type, status, dateInit, event.target.value);
    } else if (dateInit === "") {
      handleFetch(type, status, dateInit, event.target.value);
    } else {
      setErrorDateEnd(
        "La fecha final no puede ser mayor a la fecha de inicio*"
      );
    }
  };
  const handleClickPage = ({ selected }) => {
    setPage(selected);
  };

  const showMovement = async (transactionId) => {
    try {
      setLoadingMovement(true);
      setMovement({
        amount: null,
        btc: null,
        wallet: null,
        date: null,
        end: null,
        status: null,
        remaining: null,
        txs: [],
        transaction_id: null,
        isBuy: false,
      });
      setShowModalMovement(true);
      const { data } = await API.get(`/transaction/${transactionId}`);
      if (data && data.data) {
        setMovement({ ...data.data, isBuy: data.detail === "buy" });
      }
      setLoadingMovement(false);
    } catch (error) {
      console.log(error);
      setLoadingMovement(false);
    }
  };

  const pendingDelete = async (movement) => {
    try {
      const { data } = await API.post(
        `/deposit/pending/delete/${movement.transactionId}`
      );
      if (data && data.OK) {
        getMovements();
      }
    } catch (error) {
      console.log(error);
    }
  };

  const pendingDeleteConfirm = (movement) => {
    Swal.fire({
      title: "¿Estas seguro?",
      text: "¡No podrás revertir esto!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "¡Sí, bórralo!",
      cancelButtonText: "Cancelar",
    }).then((result) => {
      if (result.isConfirmed) {
        pendingDelete(movement);
      }
    });
  };

  const handleRenderMovement = () => {
    return (
      <div
        className={`container-modal make-payment ${
          movement &&
          (movement.status === "confirming" || movement.status === "success")
            ? "confirming-modal"
            : ""
        }`}
      >
        <button
          className="close"
          type="button"
          onClick={() => {
            setShowModalMovement(false);
          }}
        >
          <span aria-hidden="true">&times;</span>
        </button>
        <div
          className="total-center container-logo-modal"
          style={{ flexDirection: "column" }}
        >
          <img
            src="../assets/images/logo-completo.png"
            className="header-brand-img "
            alt="Covido logo"
          />
        </div>
        {loadingMovement ? (
          <div className="total-center">
            <Loader />
          </div>
        ) : (
          <>
            <MakePayment {...movement} />
            <div className="mt-10">
              <button
                onClick={() => {
                  setShowModalMovement(false);
                }}
                className="button-k-primary total-center"
              >
                OK
              </button>
            </div>
          </>
        )}
      </div>
    );
  };

  const showMovementIsDisabled = () => {
    // if (movement.type === "withdrawal") {
    //   return true;
    // }
    return false;
  };

  const showDeleteMovementIsDisabled = (movement) => {
    if (movement.type === "deposit" && movement.status === "pending") {
      return false;
    }
    return true;
  };

  const paramsSearch = useCallback(() => {
    const params = searchToObject();
    if (params && params.modal && params.modal === "show" && params.id) {
      showMovement(params.id);
    }
  }, []);

  useEffect(() => {
    paramsSearch();
  }, [paramsSearch]);

  return (
    <div className="app sidebar-mini" id="AppSidebar">
      <Layout>
        <div className="page-header">
          <div className="page-leftheader">
            <h4 className="page-title">Depósitos y Retiros</h4>
          </div>
        </div>
        <div className="card padding-20">
          <div className="page-leftheader">
            <div className="row ">
              <div className="col-xl-12 col-lg-6">
                <label className="form-label">Tipos</label>
                <select
                  className="form-control custom-select select2"
                  onChange={handleChangeType}
                  value={type}
                >
                  <option value=""></option>
                  <option value="deposit">Depósito</option>
                  <option value="withdrawal">Retiros</option>
                  <option value="buy">Compuesto</option>
                </select>
              </div>
              {(type === "deposit" || type === "withdrawal" || type === "buy") && (
                <div className="col-xl-12 col-lg-6">
                  <label className="form-label">Estatus</label>
                  <select
                    className="form-control custom-select select2"
                    onChange={handleChangeStatus}
                    value={status}
                  >
                    <option value=""></option>
                    <option value="success">Aprobado</option>
                    <option value="failed">Fallido</option>
                    <option value="pending">Pendiente</option>
                    <option value="confirming">Confirmando</option>
                    <option value="incomplete">Incompleto</option>
                  </select>
                </div>
              )}
            </div>
            <div className="row " style={{ marginTop: "25px" }}>
              <div className="col-xl-6 col-lg-12">
                <label className="form-label">Fecha desde</label>
                <input
                  className="form-control"
                  type="date"
                  onChange={handleDateInit}
                  value={dateInit}
                />
                {errorDateInit && (
                  <p className="textErrorLabel">{errorDateInit}</p>
                )}
              </div>
              <div className="col-xl-6 col-lg-12">
                <label className="form-label">Fecha hasta</label>
                <input
                  className="form-control"
                  type="date"
                  onChange={handleDateEnd}
                  value={dateEnd}
                />
                {errorDateEnd && (
                  <p className="textErrorLabel">{errorDateEnd}</p>
                )}
              </div>
            </div>
          </div>
        </div>
        <Modal isOpen={showModalMovement}>{handleRenderMovement()}</Modal>
        <div className="card padding-20">
          <List>
            <thead className="background-primary text-white">
              <tr>
                <th className="text-white center-text-List size-text">fecha</th>
                <th className="text-white center-text-List size-text">
                  importe
                </th>
                {/* <th className="text-white center-text-List size-text">
                  Rendimiento
                </th> */}
                <th className="text-white center-text-List size-text">Tipo</th>
                <th className="text-white center-text-List size-text">
                  Estado
                </th>
                <th className="text-white center-text-List size-text">
                  Detalles
                </th>
              </tr>
            </thead>
            {isLoggingInMovements && isLoggingInMovements ? null : (
              <tbody>
                {movements &&
                movements.movements &&
                Array.isArray(movements.movements) &&
                movements.movements.length
                  ? movements.movements.map((item, key) => {
                      return (
                        <tr key={key}>
                          <th className="center-text-List">
                            {item &&
                              item.date &&
                              moment(item.date).zone(-120).format("DD-MM-YYYY")}
                          </th>
                          <th className="center-text-List">
                            {item && item.amount && formatCurrency(item.amount)}
                          </th>
                          {/* <th className="center-text-List">
                            {item &&
                              item.performance &&
                              `${item.performance.substring(0, 4)}%`}
                          </th> */}
                          <th className="center-text-List">
                            {item &&
                              item.type &&
                              typeApi[item.type] &&
                              typeApi[item.type].title}
                          </th>
                          <th className="center-text-List">
                            {item &&
                              item.status &&
                              Status[item.status] &&
                              Status[item.status].title}
                          </th>
                          <th className="table_btns">
                            <Botton
                              disabled={showMovementIsDisabled(item)}
                              onClick={() => showMovement(item.transactionId)}
                              type="button"
                            >
                              <span
                                className="text-btn"
                                style={{ fontSize: "27px" }}
                              >
                                <i
                                  className="icon-k-kisspng-computer-icons"
                                  data-toggle="tooltip"
                                  style={{
                                    fontSize: "20px",
                                  }}
                                ></i>
                              </span>
                            </Botton>

                            {!showDeleteMovementIsDisabled(item) && (
                              <Botton
                                onClick={() => pendingDeleteConfirm(item)}
                                type="button"
                              >
                                <span
                                  className="text-btn"
                                  style={{ fontSize: "27px" }}
                                >
                                  <i
                                    className="fa fa-trash"
                                    data-toggle="tooltip"
                                    style={{
                                      fontSize: "20px",
                                    }}
                                  ></i>
                                </span>
                              </Botton>
                            )}
                          </th>
                        </tr>
                      );
                    })
                  : null}
              </tbody>
            )}
          </List>
          {movements &&
          movements.movements &&
          Array.isArray(movements.movements) &&
          movements.movements.length ? null : (
            <div className="total-center" style={{ margin: "20px" }}>
              <p className="font-weight-bold">
                No se han encontrado movimientos
              </p>
            </div>
          )}
          {isLoggingInMovements && isLoggingInMovements && (
            <div className="total-center">
              <Loader />
            </div>
          )}
          <div className="table-responsive">
            {isLoggingInMovements && isLoggingInMovements ? null : (
              <div className="total-center" style={{ marginTop: "15px" }}>
                <ReactPaginate
                  previousLabel={"Atras"}
                  nextLabel={"Siguiente"}
                  breakLabel={"..."}
                  pageCount={
                    movements && movements.totalItems
                      ? movements.totalItems / 10
                      : 0
                  }
                  forcePage={page}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={5}
                  onPageChange={handleClickPage}
                  containerClassName={"pagination"}
                  subContainerClassName={"pages pagination"}
                  disableInitialCallback={false}
                  breakClassName={"page-item"}
                  breakLinkClassName={"page-link"}
                  pageClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  previousClassName={"page-item"}
                  previousLinkClassName={"page-link"}
                  nextClassName={"page-item"}
                  nextLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </div>
            )}
          </div>
        </div>
      </Layout>
    </div>
  );
};

export default Movements;
