/* eslint-disable react/jsx-props-no-spreading */
import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import { connect } from "react-redux";
import Home from "./Home/index";
import Login from "./access/Login";
import Register from "./access/Register";
import ForgotPassword from "./access/ForgotPassword";
import ChangePassword from "./access/ChangePassword";
import Profile from "./profile/Profile";
import ActivateAccount from "./access/ActivateAccount";
import Contact from "./Contact";
import Promotions from "./Promotions";
import Document from "./Document";
import LandingPage from "./LandingPage";
import TermsAndConditions from "./TermsAndConditions";
import Cookies from "./Cookies";
import Privacy from "./Privacy";
import Referrals from "./Referrals";
import Movements from "./Movements";
import MovementsDepositos from "./MovementsDepositos";
import Operations from "./Operations";
import Deposit from "./Deposit";
import TestPage from "./TestPage";

const Routes = (props) => {
  const { isAuthenticated } = props;
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={LandingPage} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/register/:id" component={Register} />
        <Route exact path="/forgot-password" component={ForgotPassword} />
        <Route exact path="/activate-account/:id" component={ActivateAccount} />
        <Route exact path="/change-password/:id" component={ChangePassword} />
        <Route
          exact
          path="/terms-and-conditions"
          component={TermsAndConditions}
        />
        <Route exact path="/cookies" component={Cookies} />
        <Route exact path="/privacy" component={Privacy} />
        <PrivateRoute
          exact
          path="/deposit/:id"
          component={Deposit}
          isAuthenticated={isAuthenticated}
        />
        <PrivateRoute
          exact
          path="/home"
          component={Home}
          isAuthenticated={isAuthenticated}
        />
        <PrivateRoute
          exact
          path="/profile"
          isAuthenticated={isAuthenticated}
          component={Profile}
        />
        <PrivateRoute
          exact
          path="/contact"
          isAuthenticated={isAuthenticated}
          component={Contact}
        />
        <PrivateRoute
          exact
          path="/promotions"
          isAuthenticated={isAuthenticated}
          component={Promotions}
        />
        <PrivateRoute
          exact
          path="/document"
          isAuthenticated={isAuthenticated}
          component={Document}
        />
        <PrivateRoute
          exact
          path="/referrals"
          isAuthenticated={isAuthenticated}
          component={Referrals}
        />
        <PrivateRoute
          exact
          path="/movements-rendimientos"
          isAuthenticated={isAuthenticated}
          component={Movements}
        />
        <PrivateRoute
          exact
          path="/movements-depositos"
          isAuthenticated={isAuthenticated}
          component={MovementsDepositos}
        />
        <Route
          exact
          path="/test-page"
          isAuthenticated={isAuthenticated}
          component={TestPage}
        />
        <PrivateRoute
          exact
          path="/operations"
          isAuthenticated={isAuthenticated}
          component={Operations}
        />
      </Switch>
    </Router>
  );
};

const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.accessStore.token,
  };
};

const PrivateRoute = ({ component: Component, isAuthenticated, ...rest }) => {
  const token = !!isAuthenticated;
  return (
    <Route
      {...rest}
      render={(props) => {
        if (token) return <Component {...props} />;
        else return <Redirect exact to="/login" />;
      }}
    />
  );
};

export default connect(mapStateToProps, {})(Routes);
