const formProfile = [
  {
    title: "Nombre",
    id: "name",
    type: "text",
  },
  {
    title: "Apellido",
    id: "surname",
    type: "text",
  },
  {
    title: "Correo electrónico",
    id: "email",
    type: "text",
    disabled: true,
  },
  {
    title: "Teléfono",
    id: "phone",
    type: "text",
  },
  {
    title: "Dirección",
    id: "address",
    type: "text",
  },
];

const formPassword = [
  {
    title: "Contraseña",
    id: "password",
    type: "password",
  },
  {
    title: "Nueva contraseña",
    id: "newPassword",
    type: "password",
  },
  {
    title: "Reptir contraseña",
    id: "repeatPassword",
    type: "password",
  },
];
export { formProfile, formPassword };
