import * as React from "react";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import Layout from "../../components/layout/Index";
import { putProfileInit, putPasswordInit } from "../../ducks/profile/actions";
import {
  profileSelector,
  isPutPasswordSelector,
} from "../../ducks/profile/selectors";
import {
  authenticatorGoogleInit,
  activeAuthenticatorGoogleInit,
} from "../../ducks/access/actions";
import ModalQrVerification from "../../components/ModalQrVerification";
import ModalActive2FA from "./ModalActive2FA";
import { formProfile, formPassword } from "./ArrayForm";
import Button from "../../components/Button";

const Profile = () => {
  const dispatch = useDispatch();
  const profile = useSelector(profileSelector);
  const isPutPassword = useSelector(isPutPasswordSelector);
  const [isLoading, setIsLoading] = React.useState(false);
  const [isLoadingKey, setIsLoadingKey] = React.useState(false);
  const [password, setPassword] = React.useState("");
  const [showQr, setShowQr] = React.useState(false);
  const [code, setCode] = React.useState("");
  const [codeQr, setCodeQr] = React.useState("");
  const [openModal, setOpenModal] = React.useState(false);
  const [checkBox, setCheckBox] = React.useState(
    profile && profile.access2Fa ? true : false
  );
  const [activeCheckBox, setActiveCheckBox] = React.useState(false);
  const [obj, setObj] = React.useState({});
  const [isLoadingModal, setIsLoadingModal] = React.useState(false);
  const [activeModal2FA, setActiveModal2FA] = React.useState(false);
  const [isLoadingActive2FA, setIsLoadingActive2FA] = React.useState(false);
  const [checkBoxInterest, setCheckBoxInterest] = React.useState(
    profile && profile.compoundInterest ? true : false
  );
  const [activeCheckBoxInterest, setActiveCheckBoxInterest] = React.useState(
    false
  );

  const handleClickChangePassword = (values, { resetForm }) => {
    const body = {
      oldPassword: values.password,
      newPassword: values.newPassword,
    };
    if (profile && profile.twoAF !== null) {
      setObj({ body, key: "Password", function: () => resetForm });
      handleClickOpenModal();
    } else {
      handleChangePassword(body, "", resetForm);
    }
  };

  const handlePutProfile = (body, code, message) => {
    dispatch(
      putProfileInit(profile.id, body, code, (Response) => {
        setIsLoadingModal(false);
        setOpenModal(false);
        setCode("");
        setIsLoadingKey(false);
        setIsLoading(false);
        if (!Response.bool) {
          document.getElementById("title").value = Response.message;
          document.getElementById("message").value = "";
          document.getElementById("type").value = "error";
          document.getElementById("click").click();
        } else {
          document.getElementById("title").value = message;
          document.getElementById("message").value = "Edición exitosa.";
          document.getElementById("type").value = "success";
          document.getElementById("click").click();
        }
      })
    );
  };

  const handleClickPutProfile = (values) => {
    const body = {
      email: values.email,
      name: values.name,
      surname: values.surname,
      phone: values.phone,
      address: values.address,
    };
    if (profile && profile.twoAF !== null) {
      setObj({ body, key: "Profile", message: "Perfil editado!" });
      handleClickOpenModal();
    } else {
      setIsLoading(true);
      handlePutProfile(body, "", "Perfil editado!");
    }
  };

  const handleClickSetAntiPhishing = (values) => {
    const body = {
      antiPhishingPassword: values.antiPhishingPassword,
    };
    if (profile && profile.twoAF !== null) {
      setObj({ body, key: "Profile", message: "Clave Anti-Phishing!" });
      handleClickOpenModal();
    } else {
      setIsLoadingKey(true);
      handlePutProfile(body, "", "Clave Anti-Phishing!");
    }
  };

  const handleActiveAuthenticator = (formData) => {
    dispatch(
      activeAuthenticatorGoogleInit(formData, (Response) => {
        setActiveModal2FA(false);
        setIsLoadingActive2FA(false);
        setCode("");
        if (Response.bool) {
          document.getElementById("title").value =
            "Activacion de Google Authenticator!";
          document.getElementById("message").value = "Activacion exitosa.";
          document.getElementById("type").value = "success";
          document.getElementById("click").click();
        } else {
          document.getElementById("title").value = Response.message;
          document.getElementById("message").value = "";
          document.getElementById("type").value = "error";
          document.getElementById("click").click();
        }
      })
    );
  };

  const handleClickActive2FA = () => {
    setIsLoadingActive2FA(true);
    if (password !== "" && !showQr) {
      const formData = new FormData();
      formData.append("password", password);
      dispatch(
        authenticatorGoogleInit(formData, (Response) => {
          setIsLoadingActive2FA(false);
          if (Response.bool) {
            setShowQr(true);
            setCodeQr(Response.qr.url);
          } else {
            document.getElementById("title").value = Response.message;
            document.getElementById("message").value = "";
            document.getElementById("type").value = "error";
            document.getElementById("click").click();
            document.querySelector(".close").click();
          }
        })
      );
    } else {
      if (code !== "") {
        const formData = new FormData();
        formData.append("code", code);
        handleActiveAuthenticator(formData);
      }
    }
  };

  const handleClickCheckBoxInterest = (event) => {
    setActiveCheckBoxInterest(true);
    if (profile && profile.twoAF) {
      if (event.target.value === "false") {
        const body = {
          compoundInterest: true,
        };
        setObj({ body, key: "Profile", message: "Interés compuesto!" });
        setCheckBoxInterest(true);
        handleClickOpenModal();
      } else {
        const body = {
          compoundInterest: false,
        };
        setObj({ body, key: "Profile", message: "Interés compuesto!" });
        setCheckBoxInterest(false);
        handleClickOpenModal();
      }
    } else {
      if (event.target.value === "false") {
        const body = {
          compoundInterest: true,
        };
        setCheckBoxInterest(true);
        handlePutProfile(body, "", "Interés compuesto!");
      } else {
        const body = {
          compoundInterest: false,
        };
        setCheckBoxInterest(false);
        handlePutProfile(body, "", "Interés compuesto!");
      }
    }
  };

  const handleClickCheckBox = (event) => {
    setActiveCheckBox(true);
    if (event.target.value === "false") {
      const body = {
        access2Fa: true,
      };
      setCheckBox(true);
      setObj({ body, key: "Profile", message: "Activación login 2FA!" });
      handleClickOpenModal();
    } else {
      const body = {
        access2Fa: false,
      };
      setCheckBox(false);
      setObj({ body, key: "Profile", message: "Desactivación login 2FA!" });
      handleClickOpenModal();
    }
  };

  const handleChangePassword = (body, code, resetForm) => {
    dispatch(
      putPasswordInit(body, code, (Response) => {
        setIsLoadingModal(false);
        setOpenModal(false);
        setCode("");
        if (!Response.bool) {
          document.getElementById("title").value = Response.message;
          document.getElementById("message").value = "";
          document.getElementById("type").value = "error";
          document.getElementById("click").click();
        } else {
          document.getElementById("title").value = "Contraseña editada!";
          document.getElementById("message").value = "Edición exitosa.";
          document.getElementById("type").value = "success";
          document.getElementById("click").click();
          resetForm();
        }
      })
    );
  };

  const handleLoadingClass = (loading, twoAF) => {
    let classValue;
    if (!twoAF && loading) {
      classValue = "btn btn-loading";
    } else {
      classValue = "btn text-btn";
    }
    return classValue;
  };

  const handleClickOpenModal = () => {
    setOpenModal(true);
  };

  const handleCloseModal = () => {
    setOpenModal(false);
    setCode("");
    if (activeCheckBox) {
      setCheckBox(!checkBox);
      setActiveCheckBox(false);
    }
    if (activeCheckBoxInterest) {
      setCheckBoxInterest(!checkBoxInterest);
      setActiveCheckBoxInterest(false);
    }
  };

  const handleChangeCode = (event) => {
    setCode(event.target.value);
  };

  const handleClickSendCode = () => {
    const { key, body, message } = obj;
    if (code !== "") {
      if (key === "Profile") {
        setIsLoadingModal(true);
        handlePutProfile(body, `?google-code=${code}`, message);
      } else {
        setIsLoadingModal(true);
        handleChangePassword(body, `?google-code=${code}`, obj.function());
      }
    }
  };

  const handleClickOpenModal2FA = () => {
    setShowQr(false);
    setActiveModal2FA(true);
  };

  const handleCloseModal2FA = () => {
    setActiveModal2FA(false);
    setShowQr(false);
    setPassword("");
    setCode("");
  };

  const onChangePassword = (event) => {
    setPassword(event.target.value);
  };

  return (
    <div className="app sidebar-mini" id="AppSidebar">
      <ModalActive2FA
        isOpen={activeModal2FA}
        close={handleCloseModal2FA}
        onChangePassword={onChangePassword}
        password={password}
        handleClickActive2FA={handleClickActive2FA}
        showQr={showQr}
        codeQr={codeQr}
        isLoading={isLoadingActive2FA}
        code={code}
        handleChangeCode={handleChangeCode}
      />
      <ModalQrVerification
        isOpen={openModal}
        close={handleCloseModal}
        isLoading={isLoadingModal}
        code={code}
        handleChangeCode={handleChangeCode}
        send={handleClickSendCode}
      />
      <Layout>
        <div className="page-header">
          <div className="page-leftheader">
            <h4 className="page-title">Editar perfil</h4>
          </div>
        </div>
        <div className="row">
          <div className="col-xl-4 col-lg-5">
            <div className="card box-widget widget-user">
              <div className="widget-user-image mx-auto mt-5 text-center">
                <div className="logo-profile line-gradient">{`${profile.name.charAt(
                  0
                )}${profile.surname.charAt(0)}`}</div>
              </div>
              <div className="card-body text-center">
                <div className="pro-user ">
                  <h3 className="pro-user-username text-dark mb-1">
                    {`${profile && profile.name} ${profile && profile.surname}`}
                  </h3>
                </div>
              </div>
            </div>
            <div className="card">
              <div className="card-header">
                <div className="card-title">Editar contraseña</div>
              </div>
              <Formik
                initialValues={{
                  password: "",
                  newPassword: "",
                  repeatPassword: "",
                }}
                validationSchema={Yup.object({
                  password: Yup.string()
                    .min(6, "Debe tener mínimo 6 caracteres")
                    .required("El campo es requerido*"),
                  newPassword: Yup.string()
                    .min(6, "Debe tener mínimo 6 caracteres")
                    .required("El campo es requerido*"),
                  repeatPassword: Yup.string()
                    .oneOf(
                      [Yup.ref("newPassword"), null],
                      "Las contraseñas no coinciden*"
                    )
                    .required("El campo es requerido*"),
                })}
                onSubmit={handleClickChangePassword}
              >
                {({ values, handleChange, handleBlur, errors }) => (
                  <Form>
                    <div className="card-body">
                      {formPassword.map((form, key) => {
                        return (
                          <div className="form-group" key={key}>
                            <label className="form-label">{form.title}</label>
                            <div className="width">
                              <input
                                className="form-control"
                                placeholder={form.title}
                                id={form.id}
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values[form.id]}
                                type={form.type}
                              />
                            </div>
                            <ErrorMessage
                              name={form.id}
                              component="div"
                              className="textErrorLabel"
                            />
                          </div>
                        );
                      })}
                    </div>
                    <div className="card-footer text-right">
                      <Button
                        className={
                          isPutPassword
                            ? handleLoadingClass(
                                isPutPassword,
                                profile && profile.twoAF !== null
                              )
                            : handleLoadingClass(
                                isPutPassword,
                                profile && profile.twoAF !== null
                              )
                        }
                        type="submit"
                      >
                        {isPutPassword &&
                        handleLoadingClass(
                          isPutPassword,
                          profile && profile.twoAF !== null
                        ) ? (
                          <div className="btn" />
                        ) : (
                          "Guardar"
                        )}
                      </Button>
                    </div>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
          <div className="col-xl-8 col-lg-7">
            <div className="card">
              <div className="card-header">
                <div className="card-title">Editar perfil</div>
              </div>
              <Formik
                initialValues={{
                  name: profile && profile.name ? profile.name : "",
                  surname: profile && profile.surname ? profile.surname : "",
                  email: profile && profile.email ? profile.email : "",
                  phone: profile && profile.phone ? profile.phone : "",
                  address: profile && profile.address ? profile.address : "",
                }}
                validationSchema={Yup.object({
                  name: Yup.string()
                    .min(3, "Debe tener mínimo 3 caracteres")
                    .required("El campo es requerido*"),
                  surname: Yup.string()
                    .min(3, "Debe tener mínimo 3 caracteres")
                    .required("El campo es requerido*"),
                  phone: Yup.string()
                    .matches(/^\+?\d*$/, "Número de teléfono invalido*")
                    .max(15, "Maximo 15 caracteres")
                    .required("El campo es requerido*"),
                  address: Yup.string()
                    .min(3, "Debe tener mínimo 3 caracteres")
                    .required("El campo es requerido*"),
                })}
                onSubmit={handleClickPutProfile}
              >
                {({ values, handleChange, handleBlur }) => {
                  return (
                    <Form>
                      <div className="card-body">
                        <div className="card-title font-weight-bold">
                          Información básica:
                        </div>
                        <div className="row">
                          {formProfile.map((form, key) => {
                            return (
                              <div
                                className={
                                  form.id === "address"
                                    ? "col-md-12"
                                    : "col-sm-6 col-md-6"
                                }
                                key={key}
                              >
                                <div className="form-group">
                                  <label className="form-label">
                                    {form.title}
                                  </label>
                                  <div className="width">
                                    <input
                                      className="form-control"
                                      placeholder={form.title}
                                      id={form.id}
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                      value={values[form.id]}
                                      type={form.type}
                                      disabled={form.disabled}
                                    />
                                  </div>
                                  <ErrorMessage
                                    name={form.id}
                                    component="div"
                                    className="textErrorLabel"
                                  />
                                </div>
                              </div>
                            );
                          })}
                        </div>
                      </div>
                      <div className="card-footer text-right">
                        <Button
                          type="submit"
                          className={
                            isLoading
                              ? handleLoadingClass(
                                  isLoading,
                                  profile && profile.twoAF !== null
                                )
                              : handleLoadingClass(
                                  isLoading,
                                  profile && profile.twoAF !== null
                                )
                          }
                        >
                          {isLoading &&
                          handleLoadingClass(
                            isLoading,
                            profile && profile.twoAF !== null
                          ) ? (
                            <div className="btn" />
                          ) : (
                            "Guardar"
                          )}
                        </Button>
                      </div>
                    </Form>
                  );
                }}
              </Formik>
            </div>
          </div>
          <div className="col-xl-12 col-lg-12">
            <div className="card">
              <div className="card-header">
                <div className="card-title">Ajustes</div>
              </div>
              <div className="card-body">
                <div className="card-title font-weight-bold">
                  Interés compuesto
                </div>
                <div
                  className="col-xl-12 col-lg-12"
                  style={{ paddingLeft: "0px" }}
                >
                  <div>
                    <div className="custom-controls-stacked">
                      <label className="custom-control custom-checkbox">
                        <input
                          type="checkbox"
                          className="custom-control-input"
                          name="example-checkbox1"
                          value={checkBoxInterest}
                          onChange={handleClickCheckBoxInterest}
                          data-toggle="modal"
                          href="#modaldemo9"
                          checked={checkBoxInterest}
                        />
                        <span className="custom-control-label">
                          Para optimizar tus rendimientos y aprovechar el enorme
                          potencial del interés compuesto, puedes activar la
                          opción de comprar USD automáticamente.
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="card-title font-weight-bold">
                  Establecer una clave 2FA
                </div>
                <div
                  className="col-xl-4 col-lg-4"
                  style={{ paddingLeft: "0px" }}
                >
                  <div style={{ width: "100%" }}>
                    <Button
                      className={"btn btn-lg btn-block text-btn"}
                      type="button"
                      small="100%"
                      height="50px"
                      data-effect="effect-scale"
                      onClick={
                        profile && profile.twoAF
                          ? null
                          : handleClickOpenModal2FA
                      }
                      outline={profile && profile.twoAF && "#8080808a"}
                    >
                      Google Authenticator
                    </Button>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="card-title font-weight-bold">Login 2FA</div>
                <p>
                  * Puedes desactivar esta caracteristica para acceder al panel,
                  pero el 2FA seguirá siendo obligatorio para hacer retiradas de
                  saldo
                </p>
                <div
                  className="col-xl-12 col-lg-12"
                  style={{ paddingLeft: "0px" }}
                >
                  <div>
                    <div className="custom-controls-stacked">
                      <label className="custom-control custom-checkbox">
                        <input
                          type="checkbox"
                          className="custom-control-input"
                          name="example-checkbox1"
                          value={checkBox}
                          onChange={handleClickCheckBox}
                          data-toggle="modal"
                          href="#modaldemo9"
                          checked={checkBox}
                          disabled={profile && profile.twoAF === null}
                        />
                        <span className="custom-control-label">
                          Utilizar 2FA cada vez que accedo a mi panel de control
                        </span>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="card-title font-weight-bold">
                  Clave anti-phishing:
                </div>
                <p>
                  Esta clave sirve para protegerte ante el phishing. Cualquier
                  correo que te enviemos llevará esta palabra en la cabecera.
                  Ignora cualquier email en el cual no aparezca.
                </p>
                <div
                  className="col-xl-6 col-lg-12"
                  style={{ paddingLeft: "0px" }}
                >
                  <Formik
                    initialValues={{
                      antiPhishingPassword:
                        profile && profile.antiPhishingPassword
                          ? profile.antiPhishingPassword
                          : "",
                    }}
                    validationSchema={Yup.object({
                      antiPhishingPassword: Yup.string()
                        .min(3, "Debe tener mínimo 3 caracteres")
                        .required("El campo es requerido*"),
                    })}
                    onSubmit={handleClickSetAntiPhishing}
                  >
                    {({ values, handleChange, handleBlur }) => {
                      return (
                        <Form>
                          <div className="input-group">
                            <input
                              type="text"
                              className="form-control"
                              placeholder="Clave anti-phishing"
                              id="antiPhishingPassword"
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values.antiPhishingPassword}
                            />
                            <span className="input-group-append">
                              <Button
                                className={
                                  isLoadingKey
                                    ? handleLoadingClass(
                                        isLoadingKey,
                                        profile && profile.twoAF !== null
                                      )
                                    : handleLoadingClass(
                                        isLoadingKey,
                                        profile && profile.twoAF !== null
                                      )
                                }
                                type={isLoadingKey ? "" : "submit"}
                              >
                                {isLoadingKey &&
                                handleLoadingClass(
                                  isLoadingKey,
                                  profile && profile.twoAF !== null
                                ) ? (
                                  <div className="btn" />
                                ) : (
                                  "Guardar"
                                )}
                              </Button>
                            </span>
                          </div>
                          <ErrorMessage
                            name="antiPhishingPassword"
                            component="div"
                            className="textErrorLabel"
                          />
                        </Form>
                      );
                    }}
                  </Formik>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </div>
  );
};

export default Profile;
