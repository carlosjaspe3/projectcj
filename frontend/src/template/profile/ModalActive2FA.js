import React from "react";
import Modal from "react-modal";
import Button from "../../components/Button";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    padding: 0,
    marginRight: "-50%",
    borderRadius: 5,
    boxShadow: "0 20px 20px 0 rgba(0, 0, 0, 0.1)",
    border: "none",
    transform: "translate(-50%, -50%)",
    overflow: "visible",
    zIndex: "9999999999999999999999 !important",
  },
};

Modal.setAppElement("#root");

const ModalActive2FA = ({
  isOpen,
  close,
  isLoading,
  code,
  handleChangeCode,
  onChangePassword,
  password,
  handleClickActive2FA,
  showQr,
  codeQr,
}) => {
  return (
    <Modal
      isOpen={isOpen}
      style={customStyles}
      closeTimeoutMS={300}
      contentLabel="Detalle del producto"
    >
      <div className="modal-query">
        <div className="modal-header">
          <h6 className="modal-title">Google Authenticator</h6>
          <button className="close" type="button" onClick={close}>
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div className="modal-body">
          Para poder utilizar tu clave 2FA deberás instalarte la aplicaciones
          Google Authenticator existentes en el mercado.
        </div>
        {!showQr && (
          <div style={{ padding: "20px" }}>
            <input
              className="form-control"
              placeholder="Ingrese su contraseña"
              type="password"
              onChange={onChangePassword}
              value={password}
            />
          </div>
        )}
        {showQr && (
          <div className="total-center">
            <img src={codeQr} alt="alternatetext" style={{ width: "200px" }} />
          </div>
        )}
        {showQr && (
          <div>
            <input
              className="code_2FA"
              maxLength="6"
              onChange={handleChangeCode}
              value={code}
            />
          </div>
        )}
        <div className="modal-body">
          <p>
            Escanea este código con tu aplicación de 2FA favorita para
            establecer tu generador de tokens.
          </p>
        </div>
        <div className="modal-footer">
          <Button
            className={isLoading ? "btn btn-loading text-btn" : "btn text-btn"}
            type="button"
            onClick={handleClickActive2FA}
            small="100%"
            height="50px"
          >
            {isLoading ? <div style={{ height: "35px" }} /> : "Enviar"}
          </Button>
        </div>
      </div>
    </Modal>
  );
};

export default ModalActive2FA;
