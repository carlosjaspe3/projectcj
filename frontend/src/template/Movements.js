import React, { useState, useCallback, useEffect } from "react";
import moment from "moment";
import ReactPaginate from "react-paginate";
import Layout from "../components/layout/Index";
import List from "../components/list";
import Loader from "../components/Loader";
import Botton from "./../components/Button";
import API from "./../api";
import { ModalsMovements } from "./../components/Modals/ModalsMovements";
import { useMovements } from "./../hooks/useMovements";
import { formatCurrency, searchToObject } from "./../utils";

const Movements = () => {
  const typeApi = {
    referral: { title: "Referido" },
    performance: { title: "Rendimiento" },
  };
  const Status = {
    pending: { title: "Pendiente" },
    confirming: { title: "Confirmando" },
    success: { title: "Aprobado" },
    expired: { title: "Expirado" },
    incomplete: { title: "Incompleto" },
  };

  const [movementCuston, setMovementCuston] = React.useState(null);
  const [isOpenModalsMovement, setOpenModalsMovement] = useState(false);
  const [loadingMovementCuston, setLoadingMovementCuston] = useState(false);
  const toggleOpenModalsMovement = () => {
    setOpenModalsMovement(false);
    setMovementCuston(null);
  };

  const [page, setPage] = useState(0);
  const [type, setType] = React.useState("");
  const [status, setStatus] = React.useState("");
  const [dateInit, setDateInit] = React.useState("");
  const [dateEnd, setDateEnd] = React.useState("");
  const [errorDateInit, setErrorDateInit] = React.useState("");
  const [errorDateEnd, setErrorDateEnd] = React.useState("");
  const { movements, isLoggingInMovements } = useMovements(
    page,
    10,
    type,
    status,
    dateInit,
    dateEnd
  );

  const handleFetch = (type, status, dateInit, dateEnd) => {
    console.log({ type, status, dateInit, dateEnd });
  };
  const handleChangeType = (event) => {
    setType(event.target.value);
    if (type !== "deposit") {
      setStatus("");
    } else if (type !== "withdrawal") {
      setStatus("");
    }
  };
  const handleChangeStatus = (event) => {
    setStatus(event.target.value);
  };
  const handleDateInit = (event) => {
    setErrorDateInit("");
    setDateInit(event.target.value);
    if (moment(dateEnd ? dateEnd : new Date()).isAfter(event.target.value)) {
      handleFetch(type, status, event.target.value, dateEnd);
    } else if (dateInit === "") {
      handleFetch(type, status, event.target.value, dateEnd);
    } else {
      setErrorDateInit(
        "La fecha de inicio no puede ser mayor a la fecha final*"
      );
    }
  };
  const handleDateEnd = (event) => {
    setErrorDateEnd("");
    setDateEnd(event.target.value);
    if (moment(dateInit).isBefore(event.target.value)) {
      handleFetch(type, status, dateInit, event.target.value);
    } else if (dateInit === "") {
      handleFetch(type, status, dateInit, event.target.value);
    } else {
      setErrorDateEnd(
        "La fecha final no puede ser mayor a la fecha de inicio*"
      );
    }
  };
  const handleClickPage = ({ selected }) => {
    setPage(selected);
  };

  const showMovement = async (movement) => {
    try {
      setLoadingMovementCuston(true);
      setMovementCuston(null);
      setOpenModalsMovement(true);
      const { data } = await API.get(
        `/details-movimientos?date=${movement.date}&type=${movement.type}`
      );
      if (data && data.data) {
        let info = {
          ...movement,
          data: data.data,
          aux: data.aux ? data.aux : {},
        };
        setMovementCuston({...info});
      }
      setLoadingMovementCuston(false);
    } catch (error) {
      console.log(error);
      setLoadingMovementCuston(false);
      setMovementCuston(null);
    }
  };

  const showMovementIsDisabled = () => {
    return false;
  };

  const paramsSearch = useCallback(() => {
    const params = searchToObject();
    console.log(params);
    if (
      params &&
      params.modal &&
      params.modal === "show" &&
      params.date &&
      params.type
    ) {
      showMovement({ date: params.date, type: params.type });
    }
  }, []);

  useEffect(() => {
    paramsSearch();
  }, [paramsSearch]);

  return (
    <div className="app sidebar-mini" id="AppSidebar">
      <Layout>
        <div className="page-header">
          <div className="page-leftheader">
            <h4 className="page-title">Rendimientos</h4>
          </div>
        </div>
        <div className="card padding-20">
          <div className="page-leftheader">
            <div className="row ">
              <div className="col-xl-12 col-lg-6">
                <label className="form-label">Tipos</label>
                <select
                  className="form-control custom-select select2"
                  onChange={handleChangeType}
                  value={type}
                >
                  <option value=""></option>
                  <option value="referral">Referidos</option>
                  <option value="performance">Rendimientos</option>
                </select>
              </div>
              {(type === "deposit" || type === "withdrawal") && (
                <div className="col-xl-12 col-lg-6">
                  <label className="form-label">Estatus</label>
                  <select
                    className="form-control custom-select select2"
                    onChange={handleChangeStatus}
                    value={status}
                  >
                    <option value=""></option>
                    <option value="success">Aprobado</option>
                    <option value="pending">Pendiente</option>
                  </select>
                </div>
              )}
            </div>
            <div className="row " style={{ marginTop: "25px" }}>
              <div className="col-xl-6 col-lg-12">
                <label className="form-label">Fecha desde</label>
                <input
                  className="form-control"
                  type="date"
                  onChange={handleDateInit}
                  value={dateInit}
                />
                {errorDateInit && (
                  <p className="textErrorLabel">{errorDateInit}</p>
                )}
              </div>
              <div className="col-xl-6 col-lg-12">
                <label className="form-label">Fecha hasta</label>
                <input
                  className="form-control"
                  type="date"
                  onChange={handleDateEnd}
                  value={dateEnd}
                />
                {errorDateEnd && (
                  <p className="textErrorLabel">{errorDateEnd}</p>
                )}
              </div>
            </div>
          </div>
        </div>
        <ModalsMovements
          isOpen={isOpenModalsMovement}
          handleClose={toggleOpenModalsMovement}
          movement={movementCuston}
          loading={loadingMovementCuston}
        />
        <div className="card padding-20">
          <List>
            <thead className="background-primary text-white">
              <tr>
                <th className="text-white center-text-List size-text">fecha</th>
                <th className="text-white center-text-List size-text">
                  importe
                </th>
                <th className="text-white center-text-List size-text">
                  Rendimiento
                </th>
                <th className="text-white center-text-List size-text">Tipo</th>
                <th className="text-white center-text-List size-text">
                  Estado
                </th>
                <th className="text-white center-text-List size-text">
                  Detalles
                </th>
              </tr>
            </thead>
            {isLoggingInMovements && isLoggingInMovements ? null : (
              <tbody>
                {movements &&
                movements.movements &&
                Array.isArray(movements.movements) &&
                movements.movements.length
                  ? movements.movements.map((item, key) => {
                      return (
                        <tr key={key}>
                          <th className="center-text-List">
                            {item &&
                              item.date &&
                              moment(item.date).zone(-120).format("DD-MM-YYYY")}
                          </th>
                          <th className="center-text-List">
                            {item && item.amount && formatCurrency(item.amount)}
                          </th>
                          <th className="center-text-List">
                            {item &&
                              item.performance &&
                              `${item.performance.substring(0, 4)}%`}
                          </th>
                          <th className="center-text-List">
                            {item &&
                              item.type &&
                              typeApi[item.type] &&
                              typeApi[item.type].title}
                          </th>
                          <th className="center-text-List">
                            {item &&
                              item.status &&
                              Status[item.status] &&
                              Status[item.status].title}
                          </th>
                          <th className="table_btns">
                            <Botton
                              disabled={showMovementIsDisabled(item)}
                              onClick={() => showMovement(item)}
                              type="button"
                            >
                              <span
                                className="text-btn"
                                style={{ fontSize: "27px" }}
                              >
                                <i
                                  className="icon-k-kisspng-computer-icons"
                                  data-toggle="tooltip"
                                  style={{
                                    fontSize: "20px",
                                  }}
                                ></i>
                              </span>
                            </Botton>
                          </th>
                        </tr>
                      );
                    })
                  : null}
              </tbody>
            )}
          </List>
          {movements &&
          movements.movements &&
          Array.isArray(movements.movements) &&
          movements.movements.length ? null : (
            <div className="total-center" style={{ margin: "20px" }}>
              <p className="font-weight-bold">
                No se han encontrado movimientos
              </p>
            </div>
          )}
          {isLoggingInMovements && isLoggingInMovements && (
            <div className="total-center">
              <Loader />
            </div>
          )}
          <div className="table-responsive">
            {isLoggingInMovements && isLoggingInMovements ? null : (
              <div className="total-center" style={{ marginTop: "15px" }}>
                <ReactPaginate
                  previousLabel={"Atras"}
                  nextLabel={"Siguiente"}
                  breakLabel={"..."}
                  pageCount={
                    movements && movements.totalItems
                      ? movements.totalItems / 10
                      : 0
                  }
                  forcePage={page}
                  marginPagesDisplayed={2}
                  pageRangeDisplayed={5}
                  onPageChange={handleClickPage}
                  containerClassName={"pagination"}
                  subContainerClassName={"pages pagination"}
                  disableInitialCallback={false}
                  breakClassName={"page-item"}
                  breakLinkClassName={"page-link"}
                  pageClassName={"page-item"}
                  pageLinkClassName={"page-link"}
                  previousClassName={"page-item"}
                  previousLinkClassName={"page-link"}
                  nextClassName={"page-item"}
                  nextLinkClassName={"page-link"}
                  activeClassName={"active"}
                />
              </div>
            )}
          </div>
        </div>
      </Layout>
    </div>
  );
};

export default Movements;
