import React from "react";
import styled from "styled-components";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { useSelector, useDispatch } from "react-redux";
import HeaderPublic from "../components/layout/HeaderPublic";
import Button from "../components/Button";
import CardRoadMap from "../components/CardRoadMap";
import Collapse from "../components/Collapse";
import Input from "../components/Input";
import FooterPublic from "../components/layout/FooterPublic";
import ModalCookies from "../components/ModalCookies";
import { postContact1Init } from "../ducks/general/actions";

const LandingPage = (props) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(
    (state) => state.generalStore.isLoggingInContact
  );

  const handleClickSend = (values, { resetForm }) => {
    const body = {
      name: values.name,
      email: values.email,
      description: values.description,
    };
    dispatch(
      postContact1Init(body, (Response) => {
        if (Response.bool) {
          resetForm();
          document.getElementById("title").value =
            "Gracias por comunicarte con nosotros";
          document.getElementById("message").value =
            "Su mensaje fue enviado exitosamente.";
          document.getElementById("type").value = "success";
          document.getElementById("click").click();
        } else {
          document.getElementById("title").value = Response.message;
          document.getElementById("message").value = "";
          document.getElementById("type").value = "error";
          document.getElementById("click").click();
        }
      })
    );
  };
  return (
    <div className="bg-white" id="home">
      <ModalCookies />
      <HeaderPublic history={props.history} />
      <MainBackground>
        <ContainerTextMain>
          <MainTitle>
            La Plataforma “EcoTech” que da Sentido Ecológico a la Tecnología
            Blockchain
          </MainTitle>
          <TextSecondary>
            Eleva la rentabilidad de tus Criptoactivos y obtén rendimientos
            mientras cuidas el planeta.
          </TextSecondary>
          <WrapButton maxWidth="420px">
            <Button
              outline
              className="btn-login"
              link="/login"
              onClick={() => props.history.push("/login")}
            >
              Iniciar Sesión
            </Button>
            <Button
              link="/register"
              onClick={() => props.history.push("/register")}
            >
              Registrate
            </Button>
          </WrapButton>
        </ContainerTextMain>
      </MainBackground>
      <BackgroundDeco className="total-center col-cont">
        <img
          src="/assets/images/logo-k.svg"
          alt="karakorum"
          height="80px"
          style={{ marginBottom: "50px" }}
        />
        <Text maxWidth="90vw" fontSizeMd="18px">
          ¡Un viaje de mil millas comienza con el primer paso!
        </Text>
        <Text maxWidth="90vw" marginTop="20px" fontSizeMd="18px">
          Estas un paso más cerca, de obtener rendimientos por tus Criptoactivos
        </Text>
      </BackgroundDeco>
      <ContainerAboutUs id="aboutus">
        <div>
          <div>
            <Text
              textAlign="left"
              fontSize="22px"
              color="#808080"
              textAlignLg="center"
            >
              Sobre nosotros
            </Text>
            <TitleAboutUs>
              <span>KARAKORUM</span> es una plataforma <span>“EcoTech”</span>
            </TitleAboutUs>
          </div>
          <Text
            textAlign="left"
            fontSize="14px"
            color="#1d2b45"
            textAlignLg="center"
            marginTop="30px"
          >
            Gestionada por un equipo internacional de expertos en programación,
            matemáticas, economía, trading e ingeniería industrial.
            Desarrollamos sistemas tecnológicos para la gestión de Criptoactivos
            , aprovechando las inmensas oportunidades que este nuevo mercado
            ofrece.
          </Text>
          <GridTool colMd="1fr" marginTop="30px" gridGap="50px">
            <Text
              textAlign="left"
              fontSize="14px"
              color="#1d2b45"
              textAlignLg="center"
            >
              En KARAKORUM queremos escalar aún más alto y sabemos que la
              tecnología Blockchain, con sentido ecológico, es una de las más
              novedosas implementaciones que se están realizando, para impactar
              de forma positiva en la sociedad y el medioambiente a nivel
              global.
            </Text>
            <Text
              textAlign="left"
              fontSize="14px"
              color="#1d2b45"
              textAlignLg="center"
            >
              Creemos en el dicho que dice: “Llegar a la cima, es solo la mitad
              del camino” y por ello tenemos el compromiso de aprovechar las
              tecnologías, para generar oportunidades y devolverlas a su vez a
              la naturaleza, acelerando la transición energética limpia en el
              mundo.
            </Text>
          </GridTool>
        </div>
      </ContainerAboutUs>
      <div id="sherpa" />
      <ContainerSherpa>
        <BoxLogo>
          <LogoSherpa src="assets/images/logo-sherpa.svg" alt="Sherpa" />
        </BoxLogo>

        <div className="total-center">
          <GridTool
            col="500px 1fr"
            gridGap="50px"
            maxWidth="1200px"
            width="95vw"
            justifyItems="center"
            alignItems="end"
            colLg="1fr"
            marginBottom="20px"
            marginTop="15vh"
          >
            <div>
              <TitleSystem>Nuestro sistema</TitleSystem>
              <ParagraphsText>
                <p>
                  Al igual que los grandes escaladores buscan el apoyo de
                  personas expertas, que los ayuden a conquistar las cumbres más
                  altas del mundo. Nuestro sistema de IA nos ayuda a obtener los
                  máximos rendimientos sobre nuestros Criptoactivos.
                </p>
                <p>
                  Después de meses de investigación y desarrollo, somos capaces
                  de analizar la evolución de los principales Criptoactivos y
                  analizar las oportunidades de compra y venta en las diferentes
                  divisas. Operamos en los diferentes mercados (Cripto-Fiat) a
                  través de potentes algoritmos matemáticos y computación
                  avanzada, que nos permiten obtener los máximos rendimientos.
                </p>
                <p>
                  Nuestro sistema: “SHERPA”, permite a cualquier persona
                  beneficiarse desde 50$, de los rendimientos obtenidos
                  diariamente y acompañarlos así, hasta la cima de su libertad
                  financiera.
                </p>
              </ParagraphsText>
              <WrapButton maxWidth="450px" marginTop="30px">
                <Button
                  outline
                  className="btn-login"
                  link="/login"
                  onClick={() => props.history.push("/login")}
                >
                  Iniciar Sesión
                </Button>
                <Button
                  link="/register"
                  onClick={() => props.history.push("/register")}
                >
                  Registrate
                </Button>
              </WrapButton>
            </div>
            <LaptopSherpa
              src="/assets/images/img-sherpa.svg"
              alt="Laptop sherpa"
            />
          </GridTool>
        </div>
      </ContainerSherpa>
      <div className="total-center">
        <ContainerPerformance>
          <Text fontSize="42px" fontWeight="bold" color="#1d2b45">
            <span>
              <img
                src="assets/images/logo-a.svg"
                alt="logo sherpa"
                height="50px"
              />
            </span>
            Los rendimientos
          </Text>
          <div className="total-center">
            <GridTool
              col="1fr 1fr 1fr"
              justifyItems="center"
              alignItems="start"
              colMd="1fr"
            >
              <CardIcons>
                <WrapIcon>
                  <img src="assets/images/icon-1.svg" alt="icon performance" />
                </WrapIcon>
                <Text
                  color="#1d2b45"
                  fontSize="16px"
                  textAlign="center"
                  maxWidth="300px"
                  marginBottom="20px"
                >
                  Nuestro sistema reparte entre los socios los rendimientos
                  obtenidos en nuestras operaciones de forma automatizada.
                </Text>
              </CardIcons>
              <CardIcons>
                <WrapIcon>
                  <img src="assets/images/icon-2.svg" alt="icon performance" />
                </WrapIcon>
                <Text
                  color="#1d2b45"
                  fontSize="16px"
                  textAlign="center"
                  maxWidth="300px"
                >
                  La plataforma reparte diariamente los beneficios obtenidos el
                  día anterior.
                </Text>
              </CardIcons>
              <CardIcons>
                <WrapIcon>
                  <img src="assets/images/icon-3.svg" alt="icon performance" />
                </WrapIcon>
                <Text
                  color="#1d2b45"
                  fontSize="16px"
                  textAlign="center"
                  maxWidth="300px"
                >
                  Reinvertimos el 10% de todos los beneficios en el desarrollo
                  de proyectos ecológicos y contribuimos en la aceleración de
                  las energías renovables.
                </Text>
              </CardIcons>
            </GridTool>
          </div>
        </ContainerPerformance>
      </div>
      <BgMontana>
        <Text>Juntos podemos ser protagonistas del cambio.</Text>
        <Button
          link="/register"
          marginTop="5vh"
          onClick={() => props.history.push("/register")}
        >
          Registrate
        </Button>
      </BgMontana>
      <ContainerPlatform>
        <div>
          <TitlePlatform>
            Hemos desarrollado una <span>plataforma</span> fácil, segura y
            transparente
          </TitlePlatform>
          <Text fontSize="16px" textAlign="left" marginTop="45px">
            Completamente integrada con la tecnología Blockchain de Bitcoin, y
            desde la que a través de cualquier dispositivo, nuestros socios
            pueden ver en directo la evolución de nuestro fondo, realizar nuevas
            aportaciones, controlar el historial de movimientos, ¡y lo más
            importante!, realizar el retiro automatizado de sus beneficios, de
            forma instantánea a sus billeteras de Bitcoin.
          </Text>
        </div>
        <PcPlatform src="/assets/images/img-platform.svg" alt="platform" />
      </ContainerPlatform>
      <ContainerAdvantage id="Advantage">
        <Compass src="/assets/images/compass.svg" alt="compass" />
        <TextsBoxAdvantage>
          <Text
            color="#1d2b45"
            fontSize="64px"
            textAlign="left"
            fontWeight="bold"
            textAlignSm="center"
          >
            Ventajas
          </Text>
          <GridTool col="80px 1fr" marginTop="5vh" justifyItems="start">
            <IconAdvantage
              src="/assets/images/icon-dvantage-1.svg"
              alt="icon-dvantage-1.svg"
            />
            <Text color="#1d2b45" fontSize="16px" textAlign="left">
              Monitoreo en directo de nuestras operaciones a través del panel de
              control.
            </Text>
          </GridTool>
          <GridTool col="80px 1fr" marginTop="5vh" justifyItems="start">
            <IconAdvantage
              src="/assets/images/icon-dvantage-2.svg"
              alt="icon-dvantage-1.svg"
            />
            <Text color="#1d2b45" fontSize="16px" textAlign="left">
              Integración de nuestro sistema con la tecnología Blokchain de
              Bitcoin.
            </Text>
          </GridTool>
          <GridTool col="80px 1fr" marginTop="5vh" justifyItems="start">
            <IconAdvantage
              src="/assets/images/icon-dvantage-3.svg"
              alt="icon-dvantage-1.svg"
            />
            <Text color="#1d2b45" fontSize="16px" textAlign="left">
              Automatización para las compras, y retiros instantáneos a las
              billeteras de Bitcoin.
            </Text>
          </GridTool>
        </TextsBoxAdvantage>
      </ContainerAdvantage>
      <Text color="#1d2b45" fontSize="42px" fontWeight="bold">
        ¿Cómo participar?
      </Text>
      <div className="total-center">
        <GridTool
          col="1fr 1fr 1fr"
          colMd="1fr"
          maxWidth="1200px"
          justifyItems="center"
          marginTop="50px"
          gridGap="20px"
        >
          <StepCard>
            <img src="/assets/images/step-1.svg" alt="img-step" />
            <Text
              color="#1d2b45"
              fontSize="16px"
              fontWeight="bold"
              textAlign="center"
              marginBottom="20px"
              marginTop="-20px"
              maxWidth="250px"
            >
              Crea una cuenta:
            </Text>

            <Text
              color="#1d2b45"
              fontSize="14px"
              textAlign="center"
              fontWeight="400"
              maxWidth="280px"
              marginTop="20px"
            >
              Es totalmente gratuita, solo tienes que rellenar el formulario de
              registro y validar el email de confirmación.
            </Text>
          </StepCard>
          <StepCard>
            <img src="/assets/images/step-2.svg" alt="img-step" />
            <Text
              color="#1d2b45"
              fontSize="16px"
              fontWeight="bold"
              textAlign="center"
              marginBottom="20px"
              marginTop="-20px"
              maxWidth="250px"
            >
              Selecciona un importe:
            </Text>

            <Text
              color="#1d2b45"
              fontSize="14px"
              textAlign="center"
              fontWeight="400"
              maxWidth="280px"
              marginTop="20px"
            >
              Selecciona el importe que deseas, desde tan solo 50$ puedes
              comenzar a ser parte del cambio.
            </Text>
          </StepCard>
          <StepCard>
            <img src="/assets/images/step-3.svg" alt="img-step" />
            <Text
              color="#1d2b45"
              fontSize="16px"
              fontWeight="bold"
              textAlign="center"
              marginBottom="20px"
              marginTop="-20px"
              maxWidth="250px"
            >
              Comienza a obtener rentabilidad:
            </Text>

            <Text
              color="#1d2b45"
              fontSize="14px"
              textAlign="center"
              fontWeight="400"
              maxWidth="280px"
              marginTop="20px"
            >
              Una vez realizada tu aportación, comenzarás a obtener los
              beneficios y podrás retirarlos cuando tu prefieras.
            </Text>
          </StepCard>
        </GridTool>
      </div>
      <ContainerRoadMap>
        <div className="total-center">
          <GridTool
            col="320px 1fr"
            colLg="200px 1fr"
            colMd="1fr"
            maxWidth="1200px"
            width="95vw"
            gridGap="20px"
          >
            <div>
              <Text color="#808080" fontSize="48px" fontSizeLg="32px">
                Road map
              </Text>
              <Text
                color="#1d2b45"
                fontSize="48px"
                fontSizeLg="32px"
                fontWeight="bold"
              >
                Karakorum
              </Text>
            </div>
            <div>
              <CardRoadMap
                month="Octubre"
                year="2018"
                description="En este momento, los miembros del corporativo Karakorum, experimentan y desarrollan plataformas tecnológicas para terceros, ampliando su know how que será clave para el futuro."
              />
              <CardRoadMap
                month="Marzo"
                year="2019"
                description="Se comienza a investigar otros sistemas tecnológicos y se presta mayor atención a los negocios que incorporan el Bitcoin y la tecnología Blockchain en sus plataformas."
              />
              <CardRoadMap
                month="Junio"
                year="2019"
                description="Siguiendo los patrones que conducen a la obtención de altos rendimientos en los diferentes proyectos en estudio, se descubre la operativa para aprovechar la descentralización y obtener rentabilidad en los mercados Cripto-Fiat."
              />
              <CardRoadMap
                month="Octubre"
                year="2019"
                description="Se inicia el desarrollo del sistema y se comienza a trabajar en los algoritmos que nos permitan sacar el máximo beneficio de la discordancia de los precios, en las plataformas de intercambio que operan a través de blockchain"
              />
              <CardRoadMap
                month="Marzo"
                year="2020"
                description="Incorporación de Mehdi Nadir, “Nuestro Presidente”. Quien después de revisar y validar la idea de negocio, aporta los recursos necesarios para el desarrollo del proyecto. Además traza una nueva ruta, acercándonos aún más, a las energías renovables. Complementando esta ecuación, que pretende: “obtener la máxima rentabilidad mientras cuidamos el planeta”."
              />
              <CardRoadMap
                month="Mayo"
                year="2020"
                description="Después de meses de investigación y desarrollo nuestro sistema está probando una altísima eficiencia y obteniendo resultados que superan todas las expectativas."
              />
              <CardRoadMap
                month="Junio"
                year="2020"
                description="Empezamos el desarrollo de la plataforma de administración, la cual permite a todas las personas aprovecharse y sacar beneficio de nuestro sistema."
              />
              <CardRoadMap
                month="Octubre"
                year="2020"
                description="Se logra integrar nuestra plataforma con la tecnología Blockchain. Realizamos las primeras pruebas de depósitos y retiros automáticos y se sincroniza el sistema para mostrar algunas de las operaciones que realizamos en tiempo real."
              />
              <CardRoadMap
                month="Diciembre"
                year="2020"
                description="Pre-lanzamiento del sistema para socios y líderes."
              />
              <CardRoadMap
                month="Febrero"
                year="2021"
                description="Lanzamiento del sistema Karakorum para todo el mundo"
              />
            </div>
          </GridTool>
        </div>
      </ContainerRoadMap>
      <div style={{ backgroundColor: "#f6f7f9" }}>
        <ContainerMap>
          <GridTool
            sizeLg="1366px"
            alignItemsLg="end"
            colMd="1fr"
            col="500px 1fr"
            justifyItemsMd="center"
            maxWidth="90vw"
          >
            <div className="total-center col-cont align-items-end">
              <TextMap>
                <p>
                  ¡Nuestra comunidad global <br /> no para de crecer!
                </p>
                <br />
                <Text color="#FFF" fontSize="14px">
                  Administra nuestro sistema desde cualquier parte del mundo.
                </Text>
              </TextMap>
              <Button
                link="/register"
                marginTop="20px"
                onClick={() => props.history.push("/register")}
              >
                Registrate
              </Button>
            </div>
            <ImgMap src="/assets/images/map.svg" alt="map" />
          </GridTool>
        </ContainerMap>
      </div>
      <BgFaq>
        <div className="total-center">
          <ContainerFaq
            col=" 1fr 500px "
            colLg="1fr 1fr"
            colMd="1fr"
            maxWidth="1200px"
            width="95vw"
            gridGap="20px"
            justifyItems="center"
          >
            <div>
              <Collapse
                title="¿Cómo puedo participar?"
                description="Para participar solo tienes que abrir una cuenta y realizar una compra
          mínima de 50$?"
              />
              <Collapse
                title="¿Cuáles son las formas de pago en la plataforma?"
                description="Puedes realizar tus compras pagando directamente con Bitcoin a través del proceso de compra o realizar directamente nuevas compras con el saldo de tu plataforma."
              />
              <Collapse
                title="¿Cómo puedo comprar Bitcoin?"
                description={
                  <span>
                    "Para comprar Bitcoin tienes que tener un wallet en alguna
                    de las diferentes plataformas de compra venta de Bitcoin. Si
                    no sabes como funciona puedes recibir más información: Aquí
                    :{" "}
                    <a
                      href="https://bitcoin.org/es/como-empezar"
                      target="_blank"
                      rel="noopener noreferrer"
                      className="opacity link-decoration"
                    >
                      https://bitcoin.org/es/como-empezar
                    </a>
                    "
                  </span>
                }
              />

              <Collapse
                title="¿Puedo retirar mis aportaciones?"
                description="Si, una vez vencidos los 180 días de depósito, podrás realizar el retiro de la aportación."
              />
              <Collapse
                title=" ¿Cuándo se comienza a recibir rendimientos de mi aportación?"
                description="Pasados 7 días de tu aportación, el sistema comienza a repartir los  rendimientos en tu cuenta. "
              />
              <Collapse
                title=" ¿Por qué pasan 7 días para comenzar a recibir rendimientos?"
                description="Nuestro sistema se diversifica en diferentes fondos, que operan de forma independiente y que a través de potentes algoritmos se computan unificando el balance de los rendimientos. Esta fórmula nos permite reducir el riesgo en nuestras operaciones y para ello, nuestro equipo técnico calcula los balances de depósitos semanalmente, integrándolos en los diferentes fondos."
              />
              <Collapse
                title=" ¿Cómo se reciben los rendimientos?"
                description="Se reciben diariamente en tu cuenta de usuario, excepto los fines de semana ya que el sistema no opera esos días."
              />
              <Collapse
                title="¿Puedo incrementar más los beneficios?"
                description="Si. Nuestro sistema permite a los usuarios, efectuar nuevos depósitos o realizar recompras automáticas que aumentan exponencialmente los rendimientos generados a través del interés compuesto."
              />
              <Collapse
                title=" ¿Qué es el interés compuesto?"
                description="El interés compuesto, es aquel que se va sumando al capital inicial y sobre el que se van generando nuevos intereses. En este caso, tiene un efecto multiplicador porque los intereses producen nuevos intereses."
              />
              <Collapse
                title=" ¿Cómo recibo mis retiros?"
                description="Los retiros que solicites, te llegarán de forma automática  en Bitcoin a la Wallet que nos hayas indicado."
              />
              <Collapse
                title=" ¿Qué seguridad me proporciona abrir una cuenta en Karakorum?"
                description="La cuenta que abres en Karakorum tiene la opción de configurar el doble factor de autenticación (2FA) para el acceso a la plataforma y para los retiros. Además para evitar el riesgo de correos suplantando nuestra identidad (phishing), nuestros correos se enviarán con una frase de seguridad que decide el usuario."
              />
              <Collapse
                title="¿Cómo opera el sistema?"
                description="Nuestro sistema es capaz de analizar la evolución de los principales Criptoactivos y analizar las oportunidades de compra y venta en las diferentes divisas. Operamos en los diferentes mercados (Cripto-Fiat) a través de potentes algoritmos matemáticos y computación avanzada, que nos permiten obtener los máximos rendimientos."
              />
              <Collapse
                title="¿Usáis tecnología Blockchain?"
                description="Si, nuestra plataforma está integrada con la tecnología Blockchain de Bitcoin, lo que nos permite realizar los retiros de forma instantánea y totalmente automática."
              />
              <Collapse
                title="¿Puedo recomendarlo a mis amigos?"
                description="Si, y además por ello queremos premiarte. Hemos desarrollado un programa de referidos con el que podrás beneficiarte también de la rentabilidad que obtienen tus invitados."
              />
            </div>
            <div>
              <Text
                color="#808080"
                fontSize="42px"
                fontSizeLg="28px"
                textAlign="right"
                textAlignMd="center"
              >
                FAQ
              </Text>
              <Text
                color="#1d2b45"
                fontSize="42px"
                fontSizeLg="28px"
                fontWeight="bold"
                textAlign="right"
                textAlignMd="center"
              >
                Preguntas Frecuentes
              </Text>
            </div>
          </ContainerFaq>
        </div>
        <div id="contact" />
      </BgFaq>

      <Contact>
        <Text fontSize="22px" color="#1d2b45" fontWeight="bold">
          Comunicate con nosotros
        </Text>
        <Text
          fontSize="16px"
          color="#1d2b45"
          maxWidth="500px"
          fontWeight="400"
          marginTop="20px"
        >
          Si tienes cualquier duda o quieres ampliar información ponte en
          contacto con nosotros y nuestros expertos te asesorarán.
        </Text>
        <ContainerForm>
          <Formik
            initialValues={{
              name: "",
              email: "",
              description: "",
            }}
            validationSchema={Yup.object({
              name: Yup.string().required("El campo es requerido*"),
              email: Yup.string()
                .required("El campo es requerido*")
                .email("Ingrese un correo electrónico válido*"),
              description: Yup.string()
                .min(10, "Mínimo 10 caracteres*")
                .required("El campo es requerido*"),
            })}
            onSubmit={handleClickSend}
          >
            {({ values, handleChange, errors }) => {
              return (
                <Form>
                  <Input
                    id="name"
                    name="name"
                    fullWidth
                    placeholder="Nombre"
                    marginTop="10px"
                    onChange={handleChange}
                    value={values.name}
                  />
                  {errors && errors.name ? (
                    <ErrorMessage
                      name="name"
                      component="div"
                      className="textErrorLabel"
                    />
                  ) : null}
                  <Input
                    id="email"
                    name="email"
                    fullWidth
                    placeholder="Correo electrónico"
                    marginTop="10px"
                    onChange={handleChange}
                    value={values.email}
                  />
                  <ErrorMessage
                    name="email"
                    component="div"
                    className="textErrorLabel"
                  />
                  <TextareaStyle
                    id="description"
                    name="description"
                    placeholder="Escriba su mensaje aquí!"
                    cols="30"
                    rows="10"
                    onChange={handleChange}
                    value={values.description}
                  />
                  <ErrorMessage
                    name="description"
                    component="div"
                    className="textErrorLabel"
                  />
                  <div className="total-center" style={{ marginTop: "10px" }}>
                    {isLoading ? (
                      <Button
                        className="btn btn-loading text-btn"
                        type="submit"
                      >
                        Enviar
                      </Button>
                    ) : (
                      <Button className="text-btn" type="submit">
                        Enviar
                      </Button>
                    )}
                  </div>
                </Form>
              );
            }}
          </Formik>
        </ContainerForm>
      </Contact>
      <FooterPublic />
    </div>
  );
};

const MainBackground = styled.div`
  background-image: url("assets/images/bg-main-2.png");
  width: 100%;
  height: 130vh;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: bottom;
  background-position-x: center;
  background-position-y: bottom;
  margin-bottom: 20vh;
  /* background-attachment: fixed; */
  /* padding-top: 265px; */
  /* padding-left: 100px; */
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  /* @media (max-height: 768px) and (orientation: landscape) {
    padding-top: 165px;
    height: 120vh;
  }
  @media (max-width: 620px) {
    padding-top: 165px;
    height: 120vh;
  } */
  @media (max-width: 768px) {
    padding-top: 0vh;
    padding-left: 0px;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }
`;
const ContainerTextMain = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin-top: -30vh;
`;
const BackgroundDeco = styled.div`
  background-image: url("assets/images/bg-parallax.jpg");
  width: 100%;
  height: 500px;
  background-repeat: no-repeat;
  background-size: cover;
  background-position: bottom;
  background-position-x: center;
  background-position-y: bottom;
  background-attachment: fixed;
  /* margin-top: 10vh; */
  margin-bottom: 10vh;
`;
const MainTitle = styled.p`
  width: 100%;
  max-width: 800px;
  font-family: Poppins;
  font-size: 42px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
  margin: 0px;
  /* position: relative; */
  text-align: center;
  @media (max-height: 768px) {
    font-size: 32px;
  }
  @media (max-width: 768px) {
    max-width: 90vw;
    text-align: center;
  }
  @media (max-width: 620px) {
    font-size: 26px;
  }
`;
const TextSecondary = styled.p`
  width: 100%;
  max-width: 570px;
  margin-top: 20px;
  font-family: Poppins;
  font-size: 18px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: left;
  color: #d6d6d6;
  text-align: center;

  @media (max-width: 768px) {
    max-width: 80vw;
    text-align: center;
  }
  @media (max-width: 620px) {
    font-size: 16px;
  }
`;
const WrapButton = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  max-width: ${(props) => props.maxWidth || "540px"};
  margin-top: ${(props) => props.marginTop || "50px"};
  @media (max-width: 620px) {
    flex-direction: column;
    height: 90px;
  }
`;
const Text = styled.p`
  width: 100%;
  max-width: ${(props) => props.maxWidth};
  font-family: "Poppins";
  font-size: ${(props) => props.fontSize || "26px"};
  font-weight: ${(props) => props.fontWeight || 400};
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: ${(props) => props.textAlign || "center"};
  color: ${(props) => props.color || "#1d2b45"};
  margin-top: ${(props) => props.marginTop};
  margin: ${(props) => props.margin};
  padding: ${(props) => props.padding};
  margin-left: ${(props) => props.marginLeft};
  margin-bottom: ${(props) => props.marginBottom || "0px"};
  @media (max-width: ${(props) => props.sizeLg || "992px"}) {
    font-size: ${(props) => props.fontSizeLg};
    text-align: ${(props) => props.textAlignLg};
  }
  @media (max-width: ${(props) => props.sizeMd || "768px"}) {
    font-size: ${(props) => props.fontSizeMd};
    text-align: ${(props) => props.textAlignMd};
  }
  @media (max-width: ${(props) => props.sizeSm || "620px"}) {
    font-size: ${(props) => props.fontSizeSm};
    text-align: ${(props) => props.textAlignSm};
  }
`;
const ContainerAboutUs = styled.div`
  background-image: url("assets/images/img-about-us.svg");
  width: 100%;
  min-height: 100vh;
  background-size: contain;
  background-repeat: no-repeat;
  background-position: right;
  padding-left: 80px;
  margin-top: 20vh;
  display: flex;
  align-items: center;
  div {
    width: 100%;
    max-width: 600px;
  }
  @media (min-width: 1590px) {
    div {
      max-width: 750px;
    }
  }
  @media (max-width: 1300px) {
    padding-left: 20px;
    min-height: 75vh;
  }
  @media (max-width: 1080px) {
    padding-left: 20px;
    min-height: 60vh;
  }
  @media (max-width: 992px) {
    background-image: none;
    justify-content: center;
    padding-left: 0px;
    div {
      max-width: 90vw;
    }
  }
`;
const TitleAboutUs = styled.div`
  width: 100%;
  max-width: 574px;

  font-family: "Poppins";
  font-size: 36px;
  font-weight: 300;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.53;
  letter-spacing: normal;
  text-align: left;
  color: #1d2b45;
  @media (max-width: 992px) {
    text-align: center;
  }
  span {
    font-weight: bold;
  }
`;
const GridTool = styled.div`
  display: grid;
  grid-template-columns: ${(props) => props.col || "1fr 1fr"};
  grid-template-rows: ${(props) => props.rows};
  justify-items: ${(props) => props.justifyItems};
  align-items: ${(props) => props.alignItems};
  grid-gap: ${(props) => props.gridGap || "10px"};
  grid-row-gap: ${(props) => props.gridRow};
  width: ${(props) => props.width || "100%"};
  max-width: ${(props) => props.maxWidth};
  height: ${(props) => props.height};
  min-height: ${(props) => props.minHeight};
  margin-top: ${(props) => props.marginTop};
  margin: ${(props) => props.margin};
  padding: ${(props) => props.padding};
  margin-bottom: ${(props) => props.marginBottom};
  justify-items: ${(props) => props.justifyItems};
  background: ${(props) => props.backgroundColor};
  border-bottom: ${(props) => props.borderBottom};
  border-right: ${(props) => props.borderRight};
  border-top: ${(props) => props.borderTop};
  border-left: ${(props) => props.borderLeft};
  /* ------------Responsive------------ */
  @media (max-width: ${(props) => props.sizeLg || "992px"}) {
    grid-template-columns: ${(props) => props.colLg};
    max-width: ${(props) => props.maxWidthLg};
    margin-top: ${(props) => props.marginTopLg};
    justify-items: ${(props) => props.justifyItemsLg};
  }
  @media (max-width: ${(props) => props.sizeMd || "768px"}) {
    grid-template-columns: ${(props) => props.colMd};
    justify-items: ${(props) => props.justifyItemsMd};
  }
  @media (max-width: ${(props) => props.sizeSm || "620px"}) {
    grid-template-columns: ${(props) => props.colSm};
    width: ${(props) => props.widthSm};
    justify-items: ${(props) => props.justifyItemsSm};
  }
  @media (max-width: ${(props) => props.sizeXs || "420px"}) {
    grid-template-columns: ${(props) => props.colXs};
    width: ${(props) => props.widthXs};
    justify-items: ${(props) => props.justifyItemsSm};
  }
`;
const ContainerSherpa = styled.div`
  background-image: url("assets/images/bg-sherpa.jpg");
  width: 100%;
  min-height: 120vh;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: top;
  margin-top: 20vh;
  /* padding-left: 120px; */
  position: relative;
  display: flex;
  align-items: center;
  /* justify-content: center; */
  flex-direction: column;
  @media (min-width: 1600px) {
    min-height: 85vh;
  }
  @media (max-width: 992px) {
    padding: 30px 0px;
  }
`;
const BoxLogo = styled.div`
  width: 95vw;
  max-width: 1200px;
  @media (max-width: 620px) {
    margin-top: -30px;
    display: flex;
    justify-content: center;
  }
`;
const LogoSherpa = styled.img`
  width: auto;
  height: 60px;
  margin-top: 20px;
  @media (max-width: 620px) {
    margin-top: 0px;
    height: 40px;
  }
`;
const TitleSystem = styled.p`
  width: 100%;
  font-family: "Poppins";
  font-size: 42px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  -webkit-letter-spacing: normal;
  -moz-letter-spacing: normal;
  -ms-letter-spacing: normal;
  letter-spacing: normal;
  text-align: left;
  color: #fff;
  margin-top: 10px;
  margin-bottom: 0px;
  @media (max-width: 620px) {
    font-size: 28px;
    text-align: center;
  }
`;

const ParagraphsText = styled.div`
  width: 100%;
  max-width: 500px;
  margin-top: 30px;
  p {
    font-family: "Poppins";
    font-size: 14px;
    font-weight: 300;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.5;
    text-align: left;
    color: #fff;
    margin-top: 30px;
  }
`;
const LaptopSherpa = styled.img`
  width: 100%;
  height: auto;
  object-fit: contain;
  @media (max-width: 992px) {
    display: none;
  }
`;
const ContainerPerformance = styled.div`
  width: 95vw;
  max-width: 1200px;
  min-height: 550px;
  padding: 20px 0px;
  border-radius: 50px;
  box-shadow: 0 10px 22px 0 rgba(0, 0, 0, 0.16);
  background-color: #ffffff;
  margin-top: -40px;
  position: relative;
  z-index: 99;
  justify-content: space-around;
  display: flex;
  flex-direction: column;

  p > span {
    margin-right: 20px;
  }
  @media (max-width: 620px) {
    padding-top: 50px;
    p:nth-child(1) {
      font-size: 32px;
      margin-top: 20px !important;
    }
  }
`;
const WrapIcon = styled.div`
  width: 180px;
  height: 180px;
  background-color: #f3f3f3;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 100px;
  margin-bottom: 50px;
  img {
    width: auto;
    height: 100px;
  }
`;
const BgMontana = styled.div`
  background-image: url("assets/images/bg-montana.png");
  width: 100%;
  min-height: 100vh;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: bottom;
  margin-top: -35vh;
  margin-bottom: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  p {
    max-width: 670px;
    margin: 30vh 0 86.8px;
    font-family: Poppins;
    font-size: 48px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.5;
    letter-spacing: normal;
    text-align: center;
    color: #1d2b45;
  }
  @media (max-width: 992px) {
    min-height: 70vh;
    margin-top: -10vh;
    p {
      width: 94vw;
      margin-top: 10vh;
      font-size: 28px;
    }
  }
`;
const CardIcons = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  @media (max-width: 768px) {
    border-bottom: solid 0.5px #1d2b45;
    margin-bottom: 50px;

    p {
      margin-bottom: 50px;
    }
  }
`;
const ContainerPlatform = styled.div`
  background-image: url("assets/images/bg-platform.svg");
  width: 100%;
  min-height: 80vh;
  background-size: contain;
  background-repeat: no-repeat;
  background-position: left;
  display: flex;
  justify-content: space-between;
  align-items: center;
  div {
    margin-left: 100px;
    @media (min-width: 1600px) {
      margin-left: 200px;
    }
  }
  div > p {
    width: 90%;
    max-width: 700px;
  }
  @media (max-width: 992px) {
    grid-template-columns: 1fr;
    background-image: none;
    div {
      margin-left: 0px;
    }
    display: flex;
    justify-content: center;
    align-items: center;
    p {
      text-align: center;
    }

    min-height: 70vh;
  }
`;
const TitlePlatform = styled.p`
  max-width: 400px;
  font-family: Poppins;
  font-size: 32px;
  font-weight: 300;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: left;
  color: #808080;
  span {
    font-weight: bold;
    color: #1d2b45;
  }
  @media (min-width: 1600px) {
    font-size: 52px;
  }
`;
const PcPlatform = styled.img`
  max-width: 1000px;
  width: 60vw;
  height: auto;
  object-fit: contain;
  @media (max-width: 1200px) {
    height: 80vh;
  }
  @media (min-width: 1920px) {
    max-width: 80%;
  }
  @media (max-width: 992px) {
    display: none;
  }
`;
const ContainerAdvantage = styled.div`
  background-image: url("assets/images/bg-advantage.jpg");
  width: 100%;
  min-height: 120vh;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: left;
  display: grid;
  grid-template-columns: 0.8fr 1fr;
  align-items: center;
  grid-gap: 20px;
  margin: 50px 0px;

  @media (max-width: 768px) {
    grid-template-columns: 1fr;
    justify-items: center;
  }
`;

const Compass = styled.img`
  width: 90%;
  height: auto;
  object-fit: contain;
  @media (max-width: 768px) {
    display: none;
  }
`;
const IconAdvantage = styled.img`
  width: 50px;
  height: auto;
  object-fit: contain;
`;
const TextsBoxAdvantage = styled.div`
  margin-right: 100px;
  max-width: 500px;
  @media (min-width: 768px) {
    max-width: 700px;
    p:nth-child(1) {
      font-size: 80px;
    }
    p {
      font-size: 18px;
    }
  }
  @media (max-width: 768px) {
    margin-right: 0px;
    width: 95vw;
  }
`;
const StepCard = styled.div`
  max-width: 320px;
  padding: 0px 10px;
  min-height: 350px;
  border-radius: 10px;
  box-shadow: 0 20px 15px 0 rgba(0, 0, 0, 0.1);
  background-color: #ffffff;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;
const ContainerRoadMap = styled.div`
  background-image: url("assets/images/bg-road-map.svg");
  width: 100%;
  min-height: 100vh;
  background-size: cover;
  margin-top: 30px;
  padding-top: 100px;
  padding-bottom: 100px;
`;

const ContainerMap = styled.div`
  background-image: url("assets/images/bg-map.svg");
  width: auto;
  height: 32vw;
  background-size: cover;
  padding-bottom: 30px;
  background-repeat: no-repeat;
  display: flex;
  justify-content: center;
  align-items: center;
  padding-left: 5vw;
  padding-right: 5vw;
  @media (max-width: 768px) {
    background-image: none;
    background-color: #1d2b45;
    height: auto;
  }
`;
const ImgMap = styled.img`
  width: auto;
  height: 65vh;
  object-fit: contain;
  @media (max-width: 768px) {
    display: none;
  }
`;
const TextMap = styled.div`
  margin-top: 150px;
  p {
    text-align: right;
  }
  p:first-child {
    font-family: Poppins;
    font-size: 35px;
    font-weight: bold;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.33;
    letter-spacing: normal;
    color: #ffffff;
  }
  @media (max-width: 768px) {
    text-align: center;
  }
`;
const BgFaq = styled.div`
  background-image: url("assets/images/bg-faq.svg");
  width: 100%;
  min-height: 100vh;
  background-position: bottom;
  background-size: cover;
  padding-top: 100px;
  margin-bottom: 100px;
`;
const ContainerFaq = styled.div`
  display: grid;
  grid-template-columns: 1fr 500px;
  justify-items: center;
  grid-gap: 20px;
  width: 95vw;
  max-width: 1200px;
  justify-items: center;
  @media (max-width: 992px) {
    grid-template-columns: 1fr 1fr;
  }
  @media (max-width: 768px) {
    display: flex;
    flex-direction: column-reverse;
    align-items: center;
  }
`;
const Contact = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
`;
const ContainerForm = styled.div`
  width: 95vw;
  max-width: 400px;
  margin-top: 20px;
  padding-bottom: 15vh;
`;
const TextareaStyle = styled.textarea`
  width: 100%;
  height: 150px;
  border: 1px solid #c8cfd8;
  border-radius: 5px;
  padding: 5px;
  outline: none;
  color: #686461;
  margin-top: 10px;
  :focus {
    outline: none !important;
    border-color: #3f768a;
  }
  ::placeholder {
    padding-left: 5px;
    color: #728096 !important;
    font-weight: 400 !important;
  }
`;
export default LandingPage;
