import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Layout from "../components/layout/Index";
import {
  referralsInit,
  referralMyLevelInit,
  requirementsLevelInit,
  referralLevel1Init,
  referralLevel2Init,
  referralLevel3Init,
  referralLevel4Init,
  referralLevel5Init,
  referralLevel6Init,
  referralLevel7Init,
  referralLevel8Init,
  referralLevel9Init,
} from "../ducks/general/actions";
import {
  referralsInSelector,
  referralLevel1Selector,
  referralLevel2Selector,
  referralLevel3Selector,
  referralLevel4Selector,
  referralLevel5Selector,
  referralLevel6Selector,
  referralLevel7Selector,
  referralLevel8Selector,
  referralLevel9Selector,
  referralMyLevelSelector,
  requirementsLevelInSelector,
  referralIsLoggingInSelector,
  isLoggingInRequirementsLevelSelector,
  isLoggingInReferralMyLevelSelector,
} from "../ducks/general/selectors";
import { profileSelector } from "../ducks/profile/selectors";
import List from "../components/list";
import Loader from "../components/Loader";
import { formatCurrency } from "./../utils";

const Referrals = () => {
  const dispatch = useDispatch();
  const profile = useSelector(profileSelector);
  const referralIsLoading = useSelector(referralIsLoggingInSelector);
  const referralLevel1 = useSelector(referralLevel1Selector);
  const referralLevel2 = useSelector(referralLevel2Selector);
  const referralLevel3 = useSelector(referralLevel3Selector);
  const referralLevel4 = useSelector(referralLevel4Selector);
  const referralLevel5 = useSelector(referralLevel5Selector);
  const referralLevel6 = useSelector(referralLevel6Selector);
  const referralLevel7 = useSelector(referralLevel7Selector);
  const referralLevel8 = useSelector(referralLevel8Selector);
  const referralLevel9 = useSelector(referralLevel9Selector);
  const referrals = useSelector(referralsInSelector);
  const referralMyLevel = useSelector(referralMyLevelSelector);
  const requirementsLevel = useSelector(requirementsLevelInSelector);
  const isLoggingInRequirementsLevel = useSelector(
    isLoggingInRequirementsLevelSelector
  );
  const isLoggingInReferralMyLevel = useSelector(
    isLoggingInReferralMyLevelSelector
  );
  const [activeCopy, setActiveCopy] = React.useState(false);
  React.useEffect(() => {
    dispatch(referralLevel1Init());
    dispatch(referralLevel2Init());
    dispatch(referralLevel3Init());
    dispatch(referralLevel4Init());
    dispatch(referralLevel5Init());
    dispatch(referralLevel6Init());
    dispatch(referralsInit());
    dispatch(referralMyLevelInit());
    dispatch(requirementsLevelInit());
  }, [dispatch]);

  const handleValidate = (validate) => {
    return validate;
  };

  const handleIconList = (item) => {
    return (
      <i
        className={
          referralLevel1 &&
          referralLevel1.id &&
          referralLevel1.id === item.childrenFirstLevel.id
            ? "fa fa-minus icon-list-select"
            : "fa fa-plus icon-list-select"
        }
      />
    );
  };

  const handleIcon2List = (item) => {
    return (
      <i
        className={
          referralLevel2 && referralLevel2.id && referralLevel2.id === item.id
            ? "fa fa-minus icon-list-select"
            : "fa fa-plus icon-list-select"
        }
      />
    );
  };

  const handleIcon3List = (item) => {
    return (
      <i
        className={
          referralLevel3 && referralLevel3.id && referralLevel3.id === item.id
            ? "fa fa-minus icon-list-select"
            : "fa fa-plus icon-list-select"
        }
      />
    );
  };

  const handleIcon4List = (item) => {
    return (
      <i
        className={
          referralLevel4 && referralLevel4.id && referralLevel4.id === item.id
            ? "fa fa-minus icon-list-select"
            : "fa fa-plus icon-list-select"
        }
      />
    );
  };

  const handleIcon5List = (item) => {
    return (
      <i
        className={
          referralLevel5 && referralLevel5.id && referralLevel5.id === item.id
            ? "fa fa-minus icon-list-select"
            : "fa fa-plus icon-list-select"
        }
      />
    );
  };

  const handleIcon6List = (item) => {
    return (
      <i
        className={
          referralLevel6 && referralLevel6.id && referralLevel6.id === item.id
            ? "fa fa-minus icon-list-select"
            : "fa fa-plus icon-list-select"
        }
      />
    );
  };

  const handleIcon7List = (item) => {
    return (
      <i
        className={
          referralLevel7 && referralLevel7.id && referralLevel7.id === item.id
            ? "fa fa-minus icon-list-select"
            : "fa fa-plus icon-list-select"
        }
      />
    );
  };

  const handleIcon8List = (item) => {
    return (
      <i
        className={
          referralLevel8 && referralLevel8.id && referralLevel8.id === item.id
            ? "fa fa-minus icon-list-select"
            : "fa fa-plus icon-list-select"
        }
      />
    );
  };

  const handleIcon9List = (item) => {
    return (
      <i
        className={
          referralLevel9 && referralLevel9.id && referralLevel9.id === item.id
            ? "fa fa-minus icon-list-select"
            : "fa fa-plus icon-list-select"
        }
      />
    );
  };

  const getRendimiento = (item) => {
    const total = formatCurrency(
      parseFloat(item.referred) + parseFloat(item.performance)
    );
    return total;
  };

  const handleOnchangeRenderList = (
    level,
    item,
    onClick,
    hidden,
    styles,
    handleIcon
  ) => {
    return (
      <div className="grind-list">
        <div className="total-center text-list">{level}</div>
        <div className="text-list text-list">
          {level === 1 && item ? item.email : "-"}
        </div>
        <div className="total-center text-list">{item && item.name}</div>
        <div className="total-center text-list">{item && item.surname}</div>
        <div className="total-center text-list">
          {item && item.referralCode}
        </div>
        <div className="total-center text-list">
          {item && item.deposited ? formatCurrency(item.deposited) : "0$"}
        </div>
        <div className="total-center text-list">{getRendimiento(item)}</div>
        <div className="total-center">
          {item.childrenFirstLevel.length && hidden ? (
            <div
              onClick={onClick}
              className="container-show-list"
              style={styles}
            >
              {handleIcon}
            </div>
          ) : null}
        </div>
      </div>
    );
  };

  const handleRenderList = (item, onClick) => {
    return (
      <div>
        {handleOnchangeRenderList(
          1,
          item.childrenFirstLevel,
          onClick,
          true,
          null,
          handleIconList(item)
        )}
        <div className="divider" />
      </div>
    );
  };

  const handleRenderList1 = (item, listNivel0) => {
    return (
      <div>
        {handleValidate(item.id === listNivel0.id && item.level === 2)
          ? item.nivel.map((item, key) => {
              return (
                <div
                  key={key}
                  style={{
                    backgroundColor: "#eaeaeaba",
                    paddingTop: "10px",
                    paddingLeft: "3px",
                  }}
                >
                  {handleOnchangeRenderList(
                    2,
                    item,
                    handleClickLevel2Referral(item),
                    true,
                    { marginRight: "4px" },
                    handleIcon2List(item)
                  )}
                  <div className="divider" />
                  {referralLevel2 &&
                  referralLevel2 &&
                  referralLevel2.level === 3
                    ? handleRenderList2(referralLevel2, listNivel0, item, null)
                    : null}
                </div>
              );
            })
          : null}
      </div>
    );
  };

  const handleClickLevel1Referral = (itemLevel) => () => {
    if (
      !(
        referralLevel1 &&
        referralLevel1.id &&
        referralLevel1.id === itemLevel.id
      )
    ) {
      let nivel = [];
      itemLevel.childrenFirstLevel.forEach((item) => {
        nivel.push(item.childrenFirstLevel);
      });
      dispatch(referralLevel1Init({ nivel, id: itemLevel.id, level: 2 }));
    } else {
      dispatch(referralLevel1Init());
    }
  };

  const handleRenderList2 = (data, idNivel1, idNivel2) => {
    return (
      <div>
        {handleValidate(
          idNivel1.id === referralLevel1.id && idNivel2.id === data.id
        )
          ? data.nivel.map((item, key) => {
              return (
                <div
                  key={key}
                  style={{
                    backgroundColor: "#d6d6d678",
                    paddingTop: "10px",
                    paddingLeft: "4px",
                  }}
                >
                  {handleOnchangeRenderList(
                    3,
                    item,
                    handleClickLevel3Referral(item),
                    true,
                    { marginRight: "12px" },
                    handleIcon3List(item)
                  )}
                  <div className="divider" />
                  {referralLevel3 &&
                  referralLevel3 &&
                  referralLevel3.level === 4
                    ? handleRenderList3(
                        referralLevel3,
                        idNivel1,
                        idNivel2,
                        item
                      )
                    : null}
                </div>
              );
            })
          : null}
      </div>
    );
  };

  const handleRenderList3 = (data, idNivel1, idNivel2, idNivel3) => {
    return (
      <div>
        {handleValidate(
          idNivel1.id === referralLevel1.id &&
            idNivel2.id === referralLevel2.id &&
            data.id === idNivel3.id
        )
          ? data.nivel.map((item, key) => {
              return (
                <div
                  key={key}
                  style={{
                    backgroundColor: "#d6d6d6",
                    paddingTop: "10px",
                    paddingLeft: "5px",
                  }}
                >
                  {handleOnchangeRenderList(
                    4,
                    item,
                    handleClickLevel4Referral(item),
                    true,
                    { marginRight: "23px" },
                    handleIcon4List(item)
                  )}
                  <div className="divider" />
                  {referralLevel4 &&
                  referralLevel4 &&
                  referralLevel4.level === 5
                    ? handleRenderList4(
                        referralLevel4,
                        idNivel1,
                        idNivel2,
                        idNivel3,
                        item
                      )
                    : null}
                </div>
              );
            })
          : null}
      </div>
    );
  };

  const handleRenderList4 = (data, idNivel1, idNivel2, idNivel3, idNivel4) => {
    return (
      <div>
        {handleValidate(
          idNivel1.id === referralLevel1.id &&
            idNivel2.id === referralLevel2.id &&
            idNivel3.id === referralLevel3.id &&
            data.id === idNivel4.id
        )
          ? data.nivel.map((item, key) => {
              return (
                <div
                  key={key}
                  style={{
                    backgroundColor: "#cacaca",
                    paddingTop: "10px",
                    paddingLeft: "6px",
                  }}
                >
                  {handleOnchangeRenderList(
                    5,
                    item,
                    handleClickLevel5Referral(item),
                    true,
                    { marginRight: "35px" },
                    handleIcon5List(item)
                  )}
                  <div className="divider" />
                  {referralLevel5 &&
                  referralLevel5 &&
                  referralLevel5.level === 6
                    ? handleRenderList5(
                        referralLevel5,
                        idNivel1,
                        idNivel2,
                        idNivel3,
                        idNivel4,
                        item
                      )
                    : null}
                </div>
              );
            })
          : null}
      </div>
    );
  };

  const handleRenderList5 = (
    data,
    idNivel1,
    idNivel2,
    idNivel3,
    idNivel4,
    idNivel5
  ) => {
    return (
      <div>
        {handleValidate(
          idNivel1.id === referralLevel1.id &&
            idNivel2.id === referralLevel2.id &&
            idNivel3.id === referralLevel3.id &&
            idNivel4.id === referralLevel4.id &&
            data.id === idNivel5.id
        )
          ? data.nivel.map((item, key) => {
              return (
                <div
                  key={key}
                  style={{
                    backgroundColor: "#b7b7b7",
                    paddingTop: "10px",
                    paddingLeft: "7px",
                  }}
                >
                  {handleOnchangeRenderList(
                    6,
                    item,
                    handleClickLevel6Referral(item),
                    true,
                    {
                      marginRight: "47px",
                    },
                    handleIcon6List(item)
                  )}
                  <div className="divider" />
                  {referralLevel6 &&
                  referralLevel6 &&
                  referralLevel6.level === 7
                    ? handleRenderList6(
                        referralLevel6,
                        idNivel1,
                        idNivel2,
                        idNivel3,
                        idNivel4,
                        idNivel5,
                        item
                      )
                    : null}
                </div>
              );
            })
          : null}
      </div>
    );
  };

  const handleRenderList6 = (
    data,
    idNivel1,
    idNivel2,
    idNivel3,
    idNivel4,
    idNivel5,
    idNivel6
  ) => {
    return (
      <div>
        {handleValidate(
          idNivel1.id === referralLevel1.id &&
            idNivel2.id === referralLevel2.id &&
            idNivel3.id === referralLevel3.id &&
            idNivel4.id === referralLevel4.id &&
            idNivel5.id === referralLevel5.id &&
            data.id === idNivel6.id
        )
          ? data.nivel.map((item, key) => {
              return (
                <div
                  key={key}
                  style={{
                    backgroundColor: "#abaaaa",
                    paddingTop: "10px",
                    paddingLeft: "8px",
                  }}
                >
                  {handleOnchangeRenderList(
                    7,
                    item,
                    handleClickLevel7Referral(item),
                    true,
                    {
                      marginRight: "47px",
                    },
                    handleIcon7List(item)
                  )}
                  <div className="divider" />
                  {referralLevel7 &&
                  referralLevel7 &&
                  referralLevel7.level === 8
                    ? handleRenderList7(
                        referralLevel7,
                        idNivel1,
                        idNivel2,
                        idNivel3,
                        idNivel4,
                        idNivel5,
                        idNivel6,
                        item
                      )
                    : null}
                </div>
              );
            })
          : null}
      </div>
    );
  };

  const handleRenderList7 = (
    data,
    idNivel1,
    idNivel2,
    idNivel3,
    idNivel4,
    idNivel5,
    idNivel6,
    idNivel7
  ) => {
    return (
      <div>
        {handleValidate(
          idNivel1.id === referralLevel1.id &&
            idNivel2.id === referralLevel2.id &&
            idNivel3.id === referralLevel3.id &&
            idNivel4.id === referralLevel4.id &&
            idNivel5.id === referralLevel5.id &&
            idNivel6.id === referralLevel6.id &&
            data.id === idNivel7.id
            ? data.nivel.map((item, key) => {
                return (
                  <div
                    key={key}
                    style={{
                      backgroundColor: "#9c9898",
                      paddingTop: "10px",
                      paddingLeft: "8px",
                    }}
                  >
                    {handleOnchangeRenderList(
                      8,
                      item,
                      handleClickLevel8Referral(item),
                      true,
                      {
                        marginRight: "60px",
                      },
                      handleIcon8List(item)
                    )}
                    <div className="divider" />
                    {referralLevel8 &&
                    referralLevel8 &&
                    referralLevel8.level === 9
                      ? handleRenderList8(
                          referralLevel8,
                          idNivel1,
                          idNivel2,
                          idNivel3,
                          idNivel4,
                          idNivel5,
                          idNivel6,
                          idNivel7,
                          item
                        )
                      : null}
                  </div>
                );
              })
            : null
        )}
      </div>
    );
  };

  const handleRenderList8 = (
    data,
    idNivel1,
    idNivel2,
    idNivel3,
    idNivel4,
    idNivel5,
    idNivel6,
    idNivel7,
    idNivel8
  ) => {
    return (
      <div>
        {handleValidate(
          idNivel1.id === referralLevel1.id &&
            idNivel2.id === referralLevel2.id &&
            idNivel3.id === referralLevel3.id &&
            idNivel4.id === referralLevel4.id &&
            idNivel5.id === referralLevel5.id &&
            idNivel6.id === referralLevel6.id &&
            idNivel7.id === referralLevel7.id &&
            data.id === idNivel8.id
            ? data.nivel.map((item, key) => {
                return (
                  <div
                    key={key}
                    style={{
                      backgroundColor: "#908f8f",
                      paddingTop: "10px",
                      paddingLeft: "8px",
                    }}
                  >
                    {handleOnchangeRenderList(
                      9,
                      item,
                      handleClickLevel9Referral(item),
                      true,
                      {
                        marginRight: "70px",
                      },
                      handleIcon9List(item)
                    )}
                    <div className="divider" />
                    {referralLevel9 &&
                    referralLevel9 &&
                    referralLevel9.level === 10
                      ? handleRenderList9(
                          referralLevel9,
                          idNivel1,
                          idNivel2,
                          idNivel3,
                          idNivel4,
                          idNivel5,
                          idNivel6,
                          idNivel7,
                          idNivel8,
                          item
                        )
                      : null}
                  </div>
                );
              })
            : null
        )}
      </div>
    );
  };

  const handleRenderList9 = (
    data,
    idNivel1,
    idNivel2,
    idNivel3,
    idNivel4,
    idNivel5,
    idNivel6,
    idNivel7,
    idNivel8,
    idNivel9
  ) => {
    return (
      <div>
        {handleValidate(
          idNivel1.id === referralLevel1.id &&
            idNivel2.id === referralLevel2.id &&
            idNivel3.id === referralLevel3.id &&
            idNivel4.id === referralLevel4.id &&
            idNivel5.id === referralLevel5.id &&
            idNivel6.id === referralLevel6.id &&
            idNivel7.id === referralLevel7.id &&
            idNivel8.id === referralLevel8.id &&
            data.id === idNivel9.id
            ? data.nivel.map((item, key) => {
                return (
                  <div
                    key={key}
                    style={{
                      backgroundColor: "#777676",
                      paddingTop: "10px",
                      paddingLeft: "8px",
                    }}
                  >
                    {handleOnchangeRenderList(
                      10,
                      item,
                      handleClickLevel9Referral(item),
                      false
                      // {
                      //   marginRight: "67px",
                      // },
                      // handleIcon9List(item)
                    )}
                    <div className="divider" />
                    {/* {referralLevel9 &&
                    referralLevel9 &&
                    referralLevel9.level === 10
                      ? handleRenderList9(
                          referralLevel9,
                          idNivel1,
                          idNivel2,
                          idNivel3,
                          idNivel4,
                          idNivel5,
                          idNivel6,
                          idNivel7,
                          idNivel8,
                          item
                        )
                      : null} */}
                  </div>
                );
              })
            : null
        )}
      </div>
    );
  };

  const handleClickLevel2Referral = (itemLevel) => () => {
    if (
      !(
        referralLevel2 &&
        referralLevel2.id &&
        referralLevel2.id === itemLevel.id
      )
    ) {
      let nivel = [];
      itemLevel.childrenFirstLevel.forEach((item) => {
        nivel.push(item.childrenFirstLevel);
      });
      dispatch(referralLevel2Init({ nivel, id: itemLevel.id, level: 3 }));
    } else {
      dispatch(referralLevel2Init());
    }
  };

  const handleClickLevel3Referral = (itemLevel) => () => {
    if (
      !(
        referralLevel3 &&
        referralLevel3.id &&
        referralLevel3.id === itemLevel.id
      )
    ) {
      let nivel = [];
      itemLevel.childrenFirstLevel.forEach((item) => {
        nivel.push(item.childrenFirstLevel);
      });
      dispatch(referralLevel3Init({ nivel, id: itemLevel.id, level: 4 }));
    } else {
      dispatch(referralLevel3Init());
    }
  };

  const handleClickLevel4Referral = (itemLevel) => () => {
    if (
      !(
        referralLevel4 &&
        referralLevel4.id &&
        referralLevel4.id === itemLevel.id
      )
    ) {
      let nivel = [];
      itemLevel.childrenFirstLevel.forEach((item) => {
        nivel.push(item.childrenFirstLevel);
      });
      dispatch(referralLevel4Init({ nivel, id: itemLevel.id, level: 5 }));
    } else {
      dispatch(referralLevel4Init());
    }
  };

  const handleClickLevel5Referral = (itemLevel) => () => {
    if (
      !(
        referralLevel5 &&
        referralLevel5.id &&
        referralLevel5.id === itemLevel.id
      )
    ) {
      let nivel = [];
      itemLevel.childrenFirstLevel.forEach((item) => {
        nivel.push(item.childrenFirstLevel);
      });
      dispatch(referralLevel5Init({ nivel, id: itemLevel.id, level: 6 }));
    } else {
      dispatch(referralLevel5Init());
    }
  };

  const handleClickLevel6Referral = (itemLevel) => () => {
    if (
      !(
        referralLevel6 &&
        referralLevel6.id &&
        referralLevel6.id === itemLevel.id
      )
    ) {
      let nivel = [];
      itemLevel.childrenFirstLevel.forEach((item) => {
        nivel.push(item.childrenFirstLevel);
      });
      dispatch(referralLevel6Init({ nivel, id: itemLevel.id, level: 7 }));
    } else {
      dispatch(referralLevel6Init());
    }
  };

  const handleClickLevel7Referral = (itemLevel) => () => {
    if (
      !(
        referralLevel7 &&
        referralLevel7.id &&
        referralLevel7.id === itemLevel.id
      )
    ) {
      let nivel = [];
      itemLevel.childrenFirstLevel.forEach((item) => {
        nivel.push(item.childrenFirstLevel);
      });
      dispatch(referralLevel7Init({ nivel, id: itemLevel.id, level: 8 }));
    } else {
      dispatch(referralLevel7Init());
    }
  };

  const handleClickLevel8Referral = (itemLevel) => () => {
    if (
      !(
        referralLevel8 &&
        referralLevel8.id &&
        referralLevel8.id === itemLevel.id
      )
    ) {
      let nivel = [];
      itemLevel.childrenFirstLevel.forEach((item) => {
        nivel.push(item.childrenFirstLevel);
      });
      dispatch(referralLevel8Init({ nivel, id: itemLevel.id, level: 9 }));
    } else {
      dispatch(referralLevel8Init());
    }
  };

  const handleClickLevel9Referral = (itemLevel) => () => {
    if (
      !(
        referralLevel9 &&
        referralLevel9.id &&
        referralLevel9.id === itemLevel.id
      )
    ) {
      let nivel = [];
      itemLevel.childrenFirstLevel.forEach((item) => {
        nivel.push(item.childrenFirstLevel);
      });
      dispatch(referralLevel9Init({ nivel, id: itemLevel.id, level: 10 }));
    } else {
      dispatch(referralLevel9Init());
    }
  };

  const handleCopyToClipboard = (content) => () => {
    setActiveCopy(true);
    const el = document.createElement("textarea");
    el.value = content;
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
  };

  //Merge con Levels
  const [myLevels, setMyLevels] = useState([]);
  useEffect(() => {
    if (
      Array.isArray(referralMyLevel) &&
      referralMyLevel.length === 10 &&
      Array.isArray(requirementsLevel) &&
      requirementsLevel.length === 10
    ) {
      let array = [];
      for (const item of referralMyLevel) {
        const myLevel = requirementsLevel.find(
          (_item) => _item.level === item.level
        );
        array.push({ ...myLevel, levelUnlocked: item.levelUnlocked });
      }
      setMyLevels(array);
    }
  }, [referralMyLevel, requirementsLevel]);

  let levelTotal = 0;
  let totalReferralsTotal = 0;
  let totalReferralsWhitPaymentsTotal = 0;
  let totalBuyTotal = 0;
  let totalReferredTotal = 0;

  return (
    <div className="app sidebar-mini" id="AppSidebar">
      <Layout>
        <div className="page-header">
          <div className="page-leftheader">
            <h4 className="page-title">
              Empieza a ganar dinero con el programa de referidos
            </h4>
          </div>
        </div>
        <div className="card padding-20">
          <p className="card-title">Plan de Referidos</p>
          <p style={{ marginTop: "10px" }}>
            Recibe beneficios diariamente por tus referidos
          </p>
          <button
            onMouseLeave={() => setActiveCopy(false)}
            className="btn btn-lg btn-block highLighted w-100 opacity"
            style={{
              marginTop: "15px",
              boxShadow: "1px 2px #6c757d40",
              color: "#fff",
              height: "auto",
              fontSize: "18px",
            }}
            onClick={handleCopyToClipboard(
              `https://www.karakorumcorp.com/register/${
                profile && profile.referralCode
              }`
            )}
          >
            <p className="total-center text-referrals">
              {" "}
              {`${profile && profile.referralCode}`}
              <span>
                {activeCopy ? (
                  <i
                    className="fa fa-check"
                    data-toggle="tooltip"
                    title="copiar"
                    style={{ fontSize: "15px", marginLeft: "5px" }}
                  />
                ) : (
                  <i
                    className="fa fa-copy"
                    data-toggle="tooltip"
                    title="copiar"
                    style={{ fontSize: "15px", marginLeft: "5px" }}
                  />
                )}
              </span>
            </p>
          </button>
          <p style={{ marginTop: "15px" }}>
            Gana por volumen directo y gana por volumen de equipo
          </p>
          <p style={{ marginTop: "5px" }}>
            Cuando una persona recomendada por ti o una persona recomendada por
            un recomendado suyo realiza una aportación, automáticamente
            comenzarás a recibir beneficios sobre su rentabilidad hasta en 10
            niveles de profundidad.
          </p>
        </div>
        <div className="card padding-20">
          <div className="page-leftheader">
            <h4 className="page-title">Bono unilevel</h4>
            <p>
              Los niveles se desbloquean a medida que tu volumen incrementa.
            </p>
            <p style={{ marginTop: "5px" }}>
              Puedes desbloquear niveles por el volumen generado de forma
              directa, o puedes desbloquear niveles a través del volumen de
              equipo.
            </p>
          </div>
          <div style={{ marginTop: "15px" }}>
            {isLoggingInRequirementsLevel && isLoggingInRequirementsLevel ? (
              <Loader />
            ) : (
              <List>
                <thead className="text-white background-primary">
                  <tr>
                    <th className="text-white"> </th>
                    <th className="text-white"> </th>
                    <th className="text-white center-text-List size-text">
                      COMPRAS NIVEL 1
                    </th>
                    <th className="text-white center-text-List size-text">
                      VOLUMEN DE EQUIPO
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {myLevels && Array.isArray(myLevels) && myLevels.length
                    ? myLevels.map((item, key) => {
                        if (item.levelUnlocked === true) {
                          return (
                            <tr key={key}>
                              <th
                                style={{ width: "25%" }}
                                className="center-text-List"
                              >{`${
                                item && parseFloat(item.percentage, 10)
                              }%`}</th>
                              <th
                                style={{ width: "25%" }}
                                className="center-text-List"
                              >
                                Nivel {item && item.level}
                              </th>
                              <th
                                style={{ width: "25%" }}
                                className="center-text-List"
                              >
                                {item && item.minAmountDeposited
                                  ? `${formatCurrency(item.minAmountDeposited)}`
                                  : null}
                              </th>
                              <th
                                style={{ width: "25%" }}
                                className="center-text-List"
                              >
                                {item && item.minAmountDeposited
                                  ? `${formatCurrency(
                                      item.minAmountDepositedReferrals
                                    )}`
                                  : null}
                              </th>
                            </tr>
                          );
                        } else {
                          return (
                            <tr key={key}>
                              <th
                                style={{ width: "25%" }}
                                className="center-text-List opacity-list-bono"
                              >{`${
                                item && parseFloat(item.percentage, 10)
                              }%`}</th>
                              <th
                                style={{ width: "25%" }}
                                className="center-text-List opacity-list-bono"
                              >
                                Nivel {item && item.level}
                              </th>
                              {/* <th>
                                1
                              </th> */}
                              <th
                                style={{ width: "25%" }}
                                className="center-text-List"
                              >
                                <span className="key-level">
                                  <li
                                    className="fa fa-unlock-alt"
                                    style={{
                                      fontSize: "25px",
                                      color: "#8caebe",
                                    }}
                                  />
                                </span>
                                <div className="center-text-List  opacity-list-bono">
                                  <p>
                                    {item && item.minAmountDeposited
                                      ? `${formatCurrency(
                                          item.minAmountDeposited
                                        )}`
                                      : null}
                                  </p>
                                </div>
                              </th>
                              <th
                                style={{ width: "25%" }}
                                className="center-text-List opacity-list-bono"
                              >
                                {item && item.minAmountDeposited
                                  ? `${formatCurrency(
                                      item.minAmountDepositedReferrals
                                    )}`
                                  : null}
                              </th>
                            </tr>
                          );
                        }
                      })
                    : null}
                </tbody>
              </List>
            )}
          </div>
        </div>
        <div className="card padding-20">
          <div className="page-leftheader">
            <h4 className="page-title">Mis niveles</h4>
          </div>
          <div style={{ marginTop: "15px" }}>
            {isLoggingInReferralMyLevel && isLoggingInReferralMyLevel ? (
              <Loader />
            ) : (
              <List>
                <thead className="background-primary text-white">
                  <tr>
                    <th className="text-white center-text-List size-text">
                      Nivel
                    </th>
                    <th className="text-white center-text-List size-text">
                      Referidos totales
                    </th>
                    <th className="text-white center-text-List size-text">
                      Referidos con compras
                    </th>
                    <th className="text-white center-text-List size-text">
                      Compras
                    </th>
                    <th className="text-white center-text-List size-text">
                      Rendimiento por mis referidos
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {referralMyLevel &&
                  Array.isArray(referralMyLevel) &&
                  referralMyLevel.length
                    ? referralMyLevel.map((item, key) => {
                        if (item.level) {
                          levelTotal = levelTotal + item.level;
                        }
                        if (item.totalReferrals) {
                          totalReferralsTotal =
                            totalReferralsTotal + item.totalReferrals;
                        }
                        if (item.totalReferralsWhitPayments) {
                          totalReferralsWhitPaymentsTotal =
                            totalReferralsWhitPaymentsTotal +
                            item.totalReferralsWhitPayments;
                        }
                        if (item.totalBuy) {
                          item.totalBuy = parseFloat(item.totalBuy);
                          totalBuyTotal = totalBuyTotal + item.totalBuy;
                        }
                        if (item.totalReferred) {
                          item.totalReferred = parseFloat(item.totalReferred);
                          totalReferredTotal =
                            totalReferredTotal + item.totalReferred;
                        }
                        return (
                          <tr key={key}>
                            <th className="center-text-List">
                              {item && item.level}
                            </th>
                            <th className="center-text-List">
                              {item && item.totalReferrals}
                            </th>
                            <th className="center-text-List">
                              {item && item.totalReferralsWhitPayments}
                            </th>
                            <th className="center-text-List">
                              {item && formatCurrency(item.totalBuy)}
                            </th>
                            <th className="center-text-List">
                              {item && formatCurrency(item.totalReferred)}
                            </th>
                          </tr>
                        );
                      })
                    : null}

                  <tr>
                    <th className="center-text-List">
                      <b>Total: </b>
                    </th>
                    <th className="center-text-List">{totalReferralsTotal}</th>
                    <th className="center-text-List">
                      {totalReferralsWhitPaymentsTotal}
                    </th>
                    <th className="center-text-List">
                      {formatCurrency(totalBuyTotal)}
                    </th>
                    <th className="center-text-List">
                      {formatCurrency(totalReferredTotal)}
                    </th>
                  </tr>
                </tbody>
              </List>
            )}
          </div>
        </div>
        <div className="card padding-20">
          <div className="page-leftheader">
            <h4 className="page-title">Mi árbol de referidos</h4>
          </div>
          {referralIsLoading ? (
            <Loader />
          ) : (
            <div style={{ marginTop: "15px" }}>
              <List>
                <div className="background-primary text-white container-list grind-list">
                  <div className="text-white size-text center">Nivel</div>
                  <div className="text-white size-text center">
                    Correo electrónico
                  </div>
                  <div className="text-white size-text total-center">
                    Nombre
                  </div>
                  <div className="text-white size-text total-center">
                    Apellido
                  </div>
                  <div className="text-white size-text total-center">
                    Código de referido
                  </div>
                  <div className="text-white size-text total-center">
                    Compras
                  </div>
                  <div className="text-white size-text total-center">
                    Rendimientos
                  </div>
                  <div className="text-white size-text total-center">
                    Expandir
                  </div>
                </div>
                {referrals && Array.isArray(referrals) && referrals.length ? (
                  referrals.map((item, key) => {
                    return (
                      <div
                        style={{ marginTop: "10px", marginBottom: "10px" }}
                        key={key}
                      >
                        {handleRenderList(
                          item,
                          handleClickLevel1Referral(item.childrenFirstLevel),
                          0
                        )}
                        {referralLevel1 &&
                        referralLevel1 &&
                        referralLevel1.level === 2
                          ? handleRenderList1(
                              referralLevel1,
                              item.childrenFirstLevel
                            )
                          : null}
                      </div>
                    );
                  })
                ) : (
                  <p className="font-weight-bold total-center">
                    No se han encontrado referidos.
                  </p>
                )}
              </List>
            </div>
          )}
        </div>
      </Layout>
    </div>
  );
};

export default Referrals;
