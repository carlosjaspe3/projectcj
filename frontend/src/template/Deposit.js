import React from "react";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import Layout from "../components/layout/Index";
import { useParams, useHistory } from "react-router-dom";
import Button from "../components/Button";
import { getProfileInit } from "../ducks/profile/actions"
const Title = styled.p`
  font-size: 24px;
  font-family: "Poppins";
  font-weight: bold;
  color: ${(props) => props.color};
  font-style: italic;
  text-align: end;
  @media (max-width: 768px) {
    font-size: 23px;
  }
  @media (max-width: 420px) {
    font-size: 16px;
  }
`;
const SubTitle = styled.p`
  font-size: 13px;
  font-family: "Poppins", sans-serif;
  font-weight: bold;
  color: ${(props) => props.color};
  font-style: italic;
  text-align: end;
  @media (max-width: 768px) {
    font-size: 12px;
  }
  @media (max-width: 420px) {
    font-size: 8px;
  }
`;
const Center = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;
const ContainerBtnEmail = styled.div`
  margin-top: 10px;
  padding-bottom: 20px;
  @media (max-width: 620px) {
    margin-left: 0px;
  }
`;
const ContainerGrid = styled.div`
  display: grid;
  grid-template-columns: ${(props) => props.gridColumns};
  grid-gap: 15px;
  height: 200px;
  @media (max-width: 768px) {
    display: grid;
    grid-template-columns: 1fr;
    grid-gap: 15px;
  }
`;
const ContainerTextImg = styled.div`
  margin-right: 20px;
  width: 280px;
  @media (max-width: 420px) {
    margin-right: 20px;
    width: 200px;
  }
`;

const Deposit = () => {
  const dispatch = useDispatch()
  const params = useParams();
  const history = useHistory()
  React.useEffect(() => {
    dispatch(getProfileInit())
  }, [dispatch])
  const message = [
    {
      success: {
        colorTitle: "#b1bbcd",
        first_Title: "Tu depósito se realizo",
        last_Title: "con exito",
        subTitle: "Depósito Completado",
        colorSubtitle: "#3c434d",
        twoBtn: true,
        img: "../../assets/images/Grupo-883.png",
        gridColumns: "1fr",
        gridColumnsXs: "1fr",
        checkoutSuccess: true,
      },
    },
    {
      cancel: {
        colorTitle: "#b1bbcd",
        first_Title: "Ocurrio un error con",
        last_Title: "tu depósito",
        subTitle: "Depósito rechazado",
        colorSubtitle: "#ea4335",
        twoBtn: true,
        img: "../../assets/images/Grupo-907.png",
        gridColumns: "1fr",
        checkoutSuccess: false,
      },
    },
  ];
  
  return (
    <div className="app sidebar-mini" id="AppSidebar">
      <Layout>
        <div className="page-header">
          <div className="page-leftheader">
            <h4 className="page-title">Deposito</h4>
          </div>
        </div>
        <div className="card padding-20">
          {message.map((Element, key) => {
            if (Element[params.id]) {
              console.log(Element);
              return (
                <div key={key}>
                  <ContainerGrid gridColumns={Element[params.id].gridColumns}>
                    <div className="total-center">
                      <Center>
                        <ContainerTextImg>
                          <div>
                            <Title color={Element[params.id].colorTitle}>
                              {Element[params.id].first_Title}
                            </Title>
                          </div>
                          <div>
                            <Title color={Element[params.id].colorTitle}>
                              {Element[params.id].last_Title}
                            </Title>
                          </div>
                          <div>
                            <SubTitle color={Element[params.id].colorSubtitle}>
                              {Element[params.id].subTitle}
                            </SubTitle>
                          </div>
                        </ContainerTextImg>
                        {Element[params.id].checkoutSuccess ? (
                          <i
                            className="fa fa-check-circle"
                            style={{
                              fontSize: "120px",
                              color: "green",
                              marginLeft: "20px",
                            }}
                          />
                        ) : (
                          <i
                            className="fa fa-times-circle"
                            style={{
                              fontSize: "120px",
                              color: "red",
                              marginLeft: "20px",
                            }}
                          />
                        )}
                      </Center>
                    </div>
                  </ContainerGrid>
                  <ContainerBtnEmail>
                    <Center>
                      <Button
                        width="200px"
                        className="btn text-btn"
                        onClick={() => history.push("/home")}
                      >
                        Continuar
                      </Button>
                    </Center>
                  </ContainerBtnEmail>
                </div>
              );
            } else {
              return null;
            }
          })}
        </div>
      </Layout>
    </div>
  );
};

export default Deposit;
