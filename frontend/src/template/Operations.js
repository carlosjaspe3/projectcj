import React from "react";
import { useSelector, useDispatch } from "react-redux";
import moment from "moment";
import Layout from "../components/layout/Index";
import { operationsInit } from "../ducks/general/actions";
import { operationsSelector } from "../ducks/general/selectors";
import Closed from "../components/Closed";
import Modal from "../components/Modal";

const Operations = () => {
  const dispatch = useDispatch();
  const operations = useSelector(operationsSelector);
  const [fetch, setFetch] = React.useState(true);

  // Modal Info
  const itemModalDefault = {
    buy_btc: "0.00",
    buy_currency: "USD",
    buy_currency_btc: "0.0000000000",
    exchange: "binance",
    id: "0",
    opportunity: "1",
    profit: "0.00",
    profitableness: "0.00",
    quotation_pair: "USD/EUR",
    quotation_price: "0.000000",
    result: "0.00",
    sell_btc: "0.00",
    sell_currency: "EUR",
    sell_currency_btc: "0.0000000000",
    time_register: "1970-01-01 00:00:00",
  };
  const [show, setShow] = React.useState(false);
  const [itemModal, setItemModal] = React.useState({ ...itemModalDefault });
  // END Modal Info

  React.useEffect(() => {
    const handleOperations = () => {
      dispatch(
        operationsInit("1", (Response) => {
          console.log(Response);
        })
      );
    };
    if (fetch) {
      setFetch(false);
      dispatch(
        operationsInit("40", (Response) => {
          console.log(Response);
        })
      );
    }
    let timer;

    if (Math.floor(Math.random() * 10 + 1) >= 3) {
      timer = window.setInterval(() => {
        handleOperations();
      }, 6000);
    } else {
      timer = window.setInterval(() => {
        handleOperations();
      }, 3000);
    }
    return () => {
      // Return callback to run on unmount.
      window.clearInterval(timer);
    };
  }, [dispatch, fetch]);

  const handleRenderPorcentage = (item) => {
    if (parseFloat(item) >= 0) {
      return (
        <div className="total-center container-porcentage-operations">
          {item}%
        </div>
      );
    } else {
      return (
        <div className="container-porcentage-operations-faild total-center">
          {item}%
        </div>
      );
    }
  };

  // Modal Info
  const handleClose = () => {
    setItemModal({ ...itemModalDefault });
    setShow(false);
  };
  const handleClicOperation = (item) => {
    setItemModal(item);
    setShow(true);
  };
  const textInfo = () => {
    return (
      <>
        <h3 className="mb-30">Detalles de operación</h3>
        <p style={{ textAlign: "justify" }}>
          Hemos comprado BTC en <strong>{itemModal && itemModal.buy_currency}</strong> al precio <strong>{itemModal && itemModal.buy_btc}</strong> ,
          y lo hemos vendido en <strong>{itemModal && itemModal.sell_currency}</strong> al precio <strong>{itemModal && itemModal.sell_btc}</strong>,
          como en este momento la cotización de la divisas es <strong>{`${itemModal && itemModal.quotation_price.substring(0, 6)} ${itemModal && itemModal.quotation_pair}`}</strong> hemos obtenido un
          resultado de <strong>{`${itemModal && itemModal.result} ${itemModal && itemModal.sell_currency}`}</strong>,
          generando un beneficio de <strong>{`${itemModal && itemModal.profit} ${itemModal && itemModal.sell_currency}`}</strong> por cada BTC invertido.
        </p>
        <br/>
        <div style={{ display: "flex" }}>
          <div style={{ marginRight: "6px" }}>Rentabilidad de</div>
          <div className="total-center container-porcentage-operations" style={{ width: "65px" }}>
            {itemModal && itemModal.profitableness}%
          </div>
        </div>
        <div className="mt-30">
          <button
            onClick={() => {
              handleClose();
            }}
            className="button-k-primary total-center"
          >
            OK
          </button>
        </div>
      </>
    );
  };
  const handleRenderModal = () => {
    return (
      <div className="container-modal">
        <button className="close" type="button" onClick={handleClose}>
          <span aria-hidden="true">&times;</span>
        </button>
        <div className="total-center">
          <img
            src="../assets/images/logo-completo.png"
            className="header-brand-img "
            alt="Covido logo"
          />
        </div>
        <div className="total-center container-logo-modal">
          <div className="center-info-modal">{textInfo()}</div>
        </div>
      </div>
    );
  };
  // END Modal Info

  return (
    <div className="app sidebar-mini" id="AppSidebar">
      <Modal isOpen={show}>{handleRenderModal()}</Modal>
      <Layout>
        {moment(new Date()).format("dddd") === "Saturday" ||
        moment(new Date()).format("dddd") === "Sunday" ? null : (
          <div className="page-header">
            <div className="page-leftheader">
              <h4 className="page-title">Operaciones</h4>
            </div>
          </div>
        )}
        {moment(new Date()).format("dddd") === "Saturday" ||
        moment(new Date()).format("dddd") === "Sunday" ? (
          <div>
            <Closed />
          </div>
        ) : (
          <div className="card padding-20">
            <div className="table-responsive">
              <div className="total-center background-table-operations-list">
                <div className="grid-table-operations">
                  <div className=" total-center label-operations">
                    <div>COMPRA</div>
                  </div>
                  <div className=" total-center label-operations background-table-operations">
                    VENTA
                  </div>
                  <div className="total-center label-operations">
                    OPERACIONES
                  </div>
                </div>
              </div>
              <div className="total-center background-table-operations-list">
                <div className="grid-table-operations">
                  <div className="total-center">
                    <div className="grid-table-operations-two">
                      <div className="total-center text-label-list">Divisa</div>
                      <div className="total-center text-label-list">
                        Precio BTC
                      </div>
                    </div>
                  </div>
                  <div className="total-center">
                    <div className="grid-table-operations-two background-table-operations">
                      <div className="total-center text-label-list">Divisa</div>
                      <div className="total-center text-label-list">
                        Precio BTC
                      </div>
                    </div>
                  </div>
                  <div className="total-center">
                    <div className="grid-table-operations-four ">
                      <div className="total-center">
                        <div>
                          <p className="text-label-list">Cotización</p>
                          <p
                            className="text-label-list"
                            style={{ textAlign: "center" }}
                          >
                            divisas
                          </p>
                        </div>
                      </div>
                      <div className="total-center text-label-list">
                        Resultado
                      </div>
                      <div className="total-center text-label-list">
                        Beneficio
                      </div>
                      <div className="total-center text-label-list">
                        Rentabilidad
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {operations && Array.isArray(operations) && operations.length
                ? operations.map((item, key) => {
                    if (item && item.opportunity === "1") {
                      return (
                        <div
                          className="total-center background-table-operations-success width-result-list-tab"
                          style={{ cursor: "pointer" }}
                          key={key}
                          onClick={() => handleClicOperation(item)}
                        >
                          <div className="grid-table-operations">
                            <div className="total-center">
                              <div className="grid-table-operations-two">
                                <div className="total-center text-label-list">
                                  {item && item.buy_currency}
                                </div>
                                <div className="total-center text-label-list">
                                  {item && item.buy_btc}
                                </div>
                              </div>
                            </div>
                            <div className="total-center">
                              <div className="grid-table-operations-two">
                                <div className="total-center text-label-list">
                                  {item && item.sell_currency}
                                </div>
                                <div className="total-center text-label-list">
                                  {item && item.sell_btc}
                                </div>
                              </div>
                            </div>
                            <div className="total-center">
                              <div className="grid-table-operations-four">
                                <div className="total-center">
                                  <p className="text-label-list">
                                    {`${
                                      item &&
                                      item.quotation_price.substring(0, 6)
                                    } ${item && item.quotation_pair}`}
                                  </p>
                                </div>
                                <div className="total-center text-label-list">
                                  {`${item && item.result} ${
                                    item && item.sell_currency
                                  }`}
                                </div>
                                <div className="total-center text-label-list">
                                  {`${item && item.profit} ${
                                    item && item.sell_currency
                                  }`}
                                </div>
                                <div className="total-center text-label-list">
                                  {`${item && item.profitableness}%`}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    } else {
                      return (
                        <div
                          div
                          className="total-center width-result-list-tab"
                          key={key}
                          style={{
                            backgroundColor: key % 2 === 0 ? "#f4f5fb" : "#fff",
                          }}
                        >
                          <div className="grid-table-operations">
                            <div className="total-center">
                              <div className="grid-table-operations-two">
                                <div className="total-center text-label-list">
                                  {item && item.buy_currency}
                                </div>
                                <div className="total-center text-label-list">
                                  {item && item.buy_btc}
                                </div>
                              </div>
                            </div>
                            <div className="total-center">
                              <div className="grid-table-operations-two background-table-operations">
                                <div className="total-center text-label-list">
                                  {item && item.sell_currency}
                                </div>
                                <div className="total-center text-label-list">
                                  {item && item.sell_btc}
                                </div>
                              </div>
                            </div>
                            <div className="total-center">
                              <div className="grid-table-operations-four">
                                <div className="total-center">
                                  <p className="text-label-list">
                                    {`${
                                      item &&
                                      item.quotation_price.substring(0, 6)
                                    } ${item && item.quotation_pair}`}
                                  </p>
                                </div>
                                <div className="total-center text-label-list">
                                  {`${item && item.result} ${
                                    item && item.sell_currency
                                  }`}
                                </div>
                                <div className="total-center text-label-list">
                                  {`${item && item.profit} ${
                                    item && item.sell_currency
                                  }`}
                                </div>
                                <div className="total-center text-label-list">
                                  {handleRenderPorcentage(
                                    item && item.profitableness
                                  )}
                                  {/* {`${item && item.profitableness}%`} */}
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      );
                    }
                  })
                : null}
            </div>
          </div>
        )}
      </Layout>
    </div>
  );
};

export default Operations;
