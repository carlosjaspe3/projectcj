import React from "react";
import { useHistory } from "react-router-dom";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { recoveryPasswordInit } from "../../ducks/access/actions";
import { isRecoveryPasswordSelector } from "../../ducks/access/selectors";
import styled from "styled-components";

const ForgotPassword = () => {
  const token = useSelector((state) => state.accessStore.token);
  const history = useHistory();
  const dispatch = useDispatch();
  const isRecoveryPassword = useSelector(isRecoveryPasswordSelector);
  React.useEffect(() => {
    if (token) {
      history.push("/home");
    }
  }, [token, history]);
  const handleClickRecoveryPassword = (values, { resetForm }) => {
    const body = {
      email: values.email,
    };
    dispatch(
      recoveryPasswordInit(body, (Response) => {
        if (!Response.bool) {
          document.getElementById("title").value = Response.message;
          document.getElementById("message").value = "";
          document.getElementById("type").value = "error";
          document.getElementById("click").click();
        } else {
          document.getElementById("title").value = "¡Correo enviado!";
          document.getElementById("message").value =
            "Te acabamos de enviar un correo, confírmanos que lo has recibido.";
          document.getElementById("type").value = "success";
          document.getElementById("click").click();
          resetForm();
          history.push("/login");
        }
      })
    );
  };
  return (
    <div className="bg-white">
      <Container>
        <ContainerForm className="card-body mt-lg-9">
          <div onClick={() => history.push("/")}>
            <Logo src="../../assets/images/logo-completo.png" alt="logo" />
          </div>
          <h3 style={{ textAlign: "center" }}>Recuperar contraseña</h3>
          <Formik
            onSubmit={handleClickRecoveryPassword}
            initialValues={{
              email: "",
            }}
            validationSchema={Yup.object({
              email: Yup.string()
                .required("El campo es requerido*")
                .email("Ingrese un correo electrónico válido*"),
            })}
          >
            {({ values, handleChange, handleBlur }) => {
              return (
                <Form>
                  <div className="form-group">
                    <div className="width">
                      <input
                        type="text"
                        className="form-control"
                        placeholder="Correo electrónico"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        id="email"
                        value={values.email}
                      />
                    </div>
                    <ErrorMessage
                      name="email"
                      component="div"
                      className="textErrorLabel"
                    />
                  </div>
                  <div className="row">
                    <div className="col-12 mt-5">
                      <button
                        type="submit"
                        className={
                          isRecoveryPassword
                            ? "btn btn-lg btn-primary btn-block btn-loading gradien-style-buttons"
                            : "btn btn-lg btn-primary btn-block gradien-style-buttons "
                        }
                      >
                        {isRecoveryPassword ? (
                          <div className="btn btn-lg btn-primary btn-block" />
                        ) : (
                          "Enviar"
                        )}
                      </button>
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
          <div className="text-center mt-7 mb-5">
            <div
              className="font-weight-normal btn-login fs-16 text-muted color-text"
              onClick={() => history.push("/login")}
            >
              Iniciar sesión
            </div>
          </div>
        </ContainerForm>
        <div className="background-recover"></div>
      </Container>
    </div>
  );
};
const Container = styled.div`
  width: 100%;
  min-height: 100vh;
  display: grid;
  grid-template-columns: 400px 1fr;
  justify-items: center;
  align-items: center;
  @media (max-width: 768px) {
    background-image: url("assets/images/recover-bg.jpg");
    background-size: cover;
    background-position: center;
    grid-template-columns: 1fr;
    padding-top: 10px;
    padding-bottom: 10px;
  }
`;
const ContainerForm = styled.div`
  width: 100%;
  max-width: 320px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  form {
    width: 100% !important;
  }
  @media (max-width: 768px) {
    background-color: rgb(255 255 255);
    padding: 20px;
    border-radius: 10px;
    max-width: 380px;
    min-height: 80vh;
  }
  @media (max-width: 420px) {
    width: 90vw;
  }
`;
const Logo = styled.img`
  width: 200px;
  height: 50px;
  object-fit: contain;
  margin-bottom: 20px;

  @media (max-width: 768px) {
    height: 35px;
  }
`;
export default ForgotPassword;
