import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Formik, Form, ErrorMessage } from "formik";
import ReCAPTCHA from "react-google-recaptcha";
import * as Yup from "yup";
import { useHistory, useParams } from "react-router-dom";
import {
  registerInit,
  verificationsEmailInit,
} from "../../ducks/access/actions";
import { isRegisteringSelector } from "../../ducks/access/selectors";
import styled from "styled-components";
import Button from "../../components/Button";
import { codePhoneCountry } from "../../constants";
import Select from "react-select";
import API from "./../../api";

///api/validate-referral-code/{referralCode} GET

const validateReferralCode = async (referralCode) => {
  return new Promise((resolve, reject) => {
    try {
      const response = API.get(`/validate-referral-code/${referralCode}`);
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
};

export default function Register() {
  const recaptcha = React.useRef(null);
  const token = useSelector((state) => state.accessStore.token);
  const history = useHistory();
  const params = useParams();
  const isRegistering = useSelector(isRegisteringSelector);
  const dispatch = useDispatch();
  const [recaptCha, setRecaptCha] = React.useState(null);
  const [errorRecapCha, setErrorRecapCha] = React.useState("");
  const [errorReferralCode, setErrorReferralCode] = React.useState("");
  const [loadingEmail, setLoadingEmail] = React.useState(false);
  const _renderRecaptcha = () => {
    return (
      <ReCAPTCHA
        ref={recaptcha}
        sitekey="6LeBEiIaAAAAAJhqPnKNPV6eqD4j8eQmeWNIXyw3"
        onChange={_handleOnChange}
      />
    );
  };
  React.useEffect(() => {
    if (token) {
      history.push("/home");
    }
  }, [token, history]);

  const resetCaptcha = () => {
    recaptcha.current.reset();
  };

  const _handleOnChange = (captcha) => {
    if (captcha !== null) {
      setErrorRecapCha("");
      setRecaptCha(true);
    }
  };

  const handleClickRegister = async (values, { resetForm, setFieldValue }) => {
    if (!values.nextPersonalInformation) {
      setLoadingEmail(true);
      setErrorReferralCode("");
      const { data } = await validateReferralCode(values.referralCode);
      if (data.OK) {
        dispatch(
          verificationsEmailInit(values.email, (Response) => {
            setLoadingEmail(false);
            if (Response.bool) {
              setFieldValue("nextPersonalInformation", true);
            } else {
              document.getElementById("title").value = Response.message;
              document.getElementById("message").value = "";
              document.getElementById("type").value = "error";
              document.getElementById("click").click();
            }
          })
        );
      } else {
        setErrorReferralCode("Código de referido inválido");
        setLoadingEmail(false);
      }
      // setFieldValue("nextPersonalInformation", true);
    } else if (!values.nextAddressInformation) {
      setFieldValue("nextAddressInformation", true);
    } else {
      const body = {
        email: values.email,
        name: values.name,
        surname: values.surname,
        password: values.password,
        passwordRepeat: values.repeatPassword,
        phone: values.phone,
        address: values.address,
      };

      if (values.referralCode !== "") {
        body.referralCode = values.referralCode;
      }

      if (recaptCha) {
        setErrorRecapCha("");
        dispatch(
          registerInit(body, (Response) => {
            if (!Response.bool) {
              document.getElementById("title").value = Response.message;
              document.getElementById("message").value = "";
              document.getElementById("type").value = "error";
              document.getElementById("click").click();
              resetCaptcha();
            } else {
              resetCaptcha();
              document.getElementById("title").value = "¡Estas registrado!";
              document.getElementById("message").value =
                "Te acabamos de enviar un correo, confírmanos que lo has recibido.";
              document.getElementById("type").value = "success";
              document.getElementById("click").click();
              resetForm();
              history.push("/login");
            }
          })
        );
      } else {
        setErrorRecapCha("Error en el captcha.");
      }
    }
  };

  const handleRenderArrowLeft = (value, setFieldValue) => {
    return (
      <i
        className="fa fa-arrow-left arrow-left mb-4"
        onClick={() => {
          setFieldValue(value, false);
          setRecaptCha(null);
        }}
      />
    );
  };

  const CustomOption = (props) => {
    const { innerProps, isDisabled, label } = props;
    return !isDisabled ? (
      <div className="customOption" {...innerProps}>
        {label}{" "}
        <img
          src={`${window.location.origin}/assets/images/flags/${label.toLowerCase()}.svg`}
          alt={label}
          width="20px"
          srcset={`${window.location.origin}/assets/images/flags/${label.toLowerCase()}.svg`}
        />
      </div>
    ) : null;
  };

  return (
    <div className="bg-white">
      <Container>
        <ContainerForm>
          <div className="custom-logo">
            <div onClick={() => history.push("/")}>
              <Logo src="../../assets/images/logo-completo.png" alt="logo" />
            </div>
          </div>
          <div className="mb-6">
            <h3 className="mb-2">Crear cuenta</h3>
          </div>
          <Formik
            onSubmit={handleClickRegister}
            initialValues={{
              email: "",
              password: "",
              repeatPassword: "",
              referralCode: params && params.id ? params.id : "",
              setReferralCode: true, //params && params.id ? true : false,
              nextPersonalInformation: false,
              nextAddressInformation: false,
              name: "",
              surname: "",
              phone: "",
              address: "",
              codePhone: "",
            }}
            validationSchema={Yup.object({
              email: Yup.string()
                .required("El campo es requerido*")
                .email("Ingrese un correo electrónico válido*"),
              repeatEmail: Yup.string()
                .oneOf(
                  [Yup.ref("email"), null],
                  "Los correos electrónicos no coinciden*"
                )
                .required("El campo es requerido*"),
              password: Yup.string()
                .min(6, "Debe tener mínimo 6 caracteres")
                .required("El campo es requerido*"),
              repeatPassword: Yup.string()
                .oneOf(
                  [Yup.ref("password"), null],
                  "Las contraseñas no coinciden*"
                )
                .required("El campo es requerido*"),
              name: Yup.string().when("nextPersonalInformation", {
                is: true,
                then: Yup.string().required("El campo es requerido*"),
              }),
              referralCode: Yup.string().when("setReferralCode", {
                is: true,
                then: Yup.string().required("El campo es requerido*"),
              }),
              surname: Yup.string().when("nextPersonalInformation", {
                is: true,
                then: Yup.string().required("El campo es requerido*"),
              }),
              phone: Yup.string().when("nextPersonalInformation", {
                is: true,
                then: Yup.string()
                  .matches(/^\+?\d*$/, "Número de teléfono invalido*")
                  .max(15, "Maximo 15 caracteres")
                  .required("El campo es requerido*"),
              }),
              codePhone: Yup.string().when("nextPersonalInformation", {
                is: true,
                then: Yup.string().required("El campo es requerido*"),
              }),
              address: Yup.string().when("nextAddressInformation", {
                is: true,
                then: Yup.string().required("El campo es requerido*"),
              }),
            })}
            validateOnBlur={false}
            validateOnChange={false}
            width="100%"
          >
            {({ values, handleChange, handleBlur, setFieldValue, errors }) => {
              return (
                <Form>
                  {values.nextPersonalInformation &&
                    !values.nextAddressInformation &&
                    handleRenderArrowLeft(
                      "nextPersonalInformation",
                      setFieldValue
                    )}
                  {values.nextAddressInformation &&
                    handleRenderArrowLeft(
                      "nextAddressInformation",
                      setFieldValue
                    )}
                  {!values.nextPersonalInformation &&
                    [
                      {
                        title: "Correo electrónico",
                        id: "email",
                        type: "text",
                      },
                      {
                        title: "Repetir correo electrónico",
                        id: "repeatEmail",
                        type: "text",
                      },
                      {
                        title: "Contraseña",
                        id: "password",
                        type: "password",
                      },
                      {
                        title: "Repetir contraseña",
                        id: "repeatPassword",
                        type: "password",
                      },
                      {
                        titleLabel: "Tengo un código de referido.",
                      },
                      {
                        title: "Código de referido",
                        id: "referralCode",
                        type: "text",
                      },
                    ].map((form, key) => {
                      return (
                        <div className="input-group mb-4 " key={key}>
                          <div className="width">
                            {form && form.titleLabel ? (
                              <>
                                {/* <div className="custom-controls-stacked">
                                <label className="custom-control custom-checkbox">
                                  <input
                                    type="checkbox"
                                    className="custom-control-input"
                                    name="example-checkbox1"
                                    value={values.setReferralCode}
                                    onChange={(e) => {
                                      if (e.target.value === "false") {
                                        setFieldValue("setReferralCode", true);
                                      } else {
                                        setFieldValue("setReferralCode", false);
                                        setFieldValue("referralCode", "");
                                      }
                                    }}
                                    checked={values.setReferralCode}
                                  />
                                  <span className="custom-control-label">
                                    Tengo un código de referido.
                                  </span>
                                </label>
                              </div> */}
                              </>
                            ) : (
                              <div>
                                {form.id === "referralCode" &&
                                params &&
                                params.id ? (
                                  <>
                                    <input
                                      className="form-control"
                                      placeholder={form && form.title}
                                      id={form && form.id}
                                      value={values[form && form.id]}
                                      type={form && form.type}
                                      onPaste={(event) => {
                                        event.preventDefault();
                                      }}
                                      disabled={true}
                                    />
                                  </>
                                ) : (
                                  <>
                                    <input
                                      className="form-control"
                                      placeholder={form && form.title}
                                      id={form && form.id}
                                      onChange={handleChange}
                                      onBlur={handleBlur}
                                      value={values[form && form.id]}
                                      type={form && form.type}
                                      onPaste={(event) => {
                                        event.preventDefault();
                                      }}
                                      disabled={
                                        form &&
                                        form.id &&
                                        form.id === "referralCode" &&
                                        values.setReferralCode === false
                                          ? true
                                          : false
                                      }
                                    />
                                  </>
                                )}

                                <ErrorMessage
                                  name={form && form.id}
                                  component="div"
                                  className="textErrorLabel"
                                />
                                {errorReferralCode &&
                                  form.id === "referralCode" && (
                                    <div style={{ marginTop: "10px" }}>
                                      <p className="textErrorLabel">
                                        {errorReferralCode}
                                      </p>
                                    </div>
                                  )}
                              </div>
                            )}
                          </div>
                        </div>
                      );
                    })}
                  <div className="row">
                    {values.nextPersonalInformation &&
                      !values.nextAddressInformation &&
                      [
                        {
                          title: "Nombre",
                          id: "name",
                          type: "text",
                        },
                        {
                          title: "Apellido",
                          id: "surname",
                          type: "text",
                        },
                        {
                          title: "Codigo",
                          id: "codePhone",
                          type: "select",
                        },
                        {
                          title: "Teléfono",
                          id: "phone",
                          type: "text",
                        },
                      ].map((form, key) => {
                        if (form.type === "text") {
                          return (
                            <div
                              className={`input-group mb-4 ${
                                form.id === "phone" ? "col-8" : "col-12"
                              }`}
                              key={key}
                            >
                              <div className="width">
                                <input
                                  className="form-control"
                                  placeholder={form.title}
                                  id={form.id}
                                  onChange={(e) => {
                                    if (
                                      values.phone === "" &&
                                      values.codePhone !== ""
                                    ) {
                                      setFieldValue("phone", values.codePhone);
                                    } else {
                                      setFieldValue(form.id, e.target.value);
                                    }
                                  }}
                                  onBlur={handleBlur}
                                  value={values[form.id]}
                                  type={form.type}
                                  disabled={
                                    form.id === "phone" &&
                                    values.codePhone === ""
                                      ? true
                                      : false
                                  }
                                />
                              </div>
                              <ErrorMessage
                                name={form.id}
                                component="div"
                                className="textErrorLabel"
                              />
                            </div>
                          );
                        }
                        return (
                          <div className="col-4" key={key}>
                            <div className="form-group">
                              <Select
                                style={{
                                  fontSize: "13px",
                                  borderColor:
                                    errors && errors.codePhone ? "red" : null,
                                }}
                                placeholder={"Código"}
                                defaultValue={values[form.id]}
                                onChange={(item) => {
                                  if (item.value !== "") {
                                    setFieldValue("codePhone", item.value);
                                    setFieldValue("phone", item.value);
                                    const phoneInput = document.getElementById(
                                      "phone"
                                    );
                                    if (phoneInput) {
                                      phoneInput.focus();
                                    }
                                  } else {
                                    setFieldValue("codePhone", "");
                                  }
                                }}
                                onBlur={handleBlur}
                                components={{ Option: CustomOption }}
                                options={codePhoneCountry.map((item) => {
                                  return {
                                    label: item.code,
                                    value: item.dial_code,
                                    ...item,
                                  };
                                })}
                              />
                            </div>
                          </div>
                        );
                      })}
                  </div>
                  {values.nextAddressInformation &&
                    [
                      {
                        title: "Dirección",
                        id: "address",
                        type: "text",
                      },
                    ].map((form, key) => {
                      return (
                        <div className="input-group mb-4" key={key}>
                          <div className="width">
                            <input
                              className="form-control"
                              placeholder={form.title}
                              id={form.id}
                              onChange={handleChange}
                              onBlur={handleBlur}
                              value={values[form.id]}
                              type={form.type}
                            />
                          </div>
                          <ErrorMessage
                            name={form.id}
                            component="div"
                            className="textErrorLabel"
                          />
                          <div style={{ marginTop: "10px" }}>
                            {_renderRecaptcha()}
                            {errorRecapCha && (
                              <p className="textErrorLabel">{errorRecapCha}</p>
                            )}
                          </div>
                        </div>
                      );
                    })}

                  <div className="row">
                    <div className="col-6" />
                    <div className="col-12 mt-5">
                      <Button
                        className={
                          isRegistering || loadingEmail
                            ? "btn btn-loading"
                            : "btn text-btn"
                        }
                        type="submit"
                        small="100%"
                        height="50px"
                      >
                        {values.nextAddressInformation ? (
                          isRegistering || loadingEmail ? (
                            <div className="btn btn-lg btn-block" />
                          ) : (
                            "Crear cuenta"
                          )
                        ) : (
                          "Siguiente"
                        )}
                      </Button>
                    </div>
                  </div>
                  <div className="text-center mt-7 mb-5">
                    <div className="font-weight-normal fs-16 text-muted">
                      ¿Ya tienes una cuenta?{" "}
                      <span
                        className="btn-link font-weight-normal color-text"
                        onClick={() => history.push("/login")}
                      >
                        Entrar
                      </span>
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </ContainerForm>
        <div className="background-register"></div>
      </Container>
    </div>
  );
}

const Container = styled.div`
  width: 100%;
  min-height: 100vh;
  display: grid;
  grid-template-columns: 400px 1fr;
  justify-items: center;
  align-items: center;
  @media (max-width: 768px) {
    background-image: url("assets/images/register-bg.jpg");
    background-size: cover;
    background-position: center;
    grid-template-columns: 1fr;
    padding-top: 10px;
    padding-bottom: 10px;
  }
`;
const ContainerForm = styled.div`
  width: 100%;
  max-width: 320px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  form {
    width: 100% !important;
  }
  @media (max-width: 768px) {
    background-color: rgb(255 255 255);
    padding: 20px;
    border-radius: 10px;
    max-width: 380px;
    min-height: 80vh;
  }
  @media (max-width: 420px) {
    width: 90vw;
  }
`;
const Logo = styled.img`
  width: 200px;
  height: 50px;
  object-fit: contain;
  margin-bottom: 20px;

  @media (max-width: 768px) {
    height: 35px;
  }
`;
