import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { useHistory } from "react-router-dom";
import ReCAPTCHA from "react-google-recaptcha";
import {
  loginInit,
  enterCodeAuthenticatorGoogleInit,
} from "../../ducks/access/actions";
import { isLoggingInSelector } from "../../ducks/access/selectors";
import styled from "styled-components";
import Button from "../../components/Button";

export default function Login() {
  const recaptcha = React.useRef(null);
  const token = useSelector((state) => state.accessStore.token);
  const history = useHistory();
  const dispatch = useDispatch();
  const isLoadingIn = useSelector(isLoggingInSelector);
  const isCodeEnterAuthenticatorGoogle = useSelector(
    (state) => state.accessStore.isEnterCodeActiveAuthenticador
  );
  const [codeQr, setCodeQr] = React.useState("");
  const [body, setBody] = React.useState({});
  const [recaptCha, setRecaptCha] = React.useState(null);
  const [errorRecapCha, setErrorRecapCha] = React.useState("");

  React.useEffect(() => {
    if (token) {
      history.push("/home");
    }
    // _renderRecaptcha();
  }, [token, history]);

  const resetCaptcha = () => {
    recaptcha.current.reset();
  };

  const _handleOnChange = (captcha) => {
    if (captcha !== null) {
      setErrorRecapCha("");
      setRecaptCha(true);
    }
  };

  const _renderRecaptcha = () => {
    return (
      <ReCAPTCHA
        ref={recaptcha}
        sitekey="6LeBEiIaAAAAAJhqPnKNPV6eqD4j8eQmeWNIXyw3"
        onChange={_handleOnChange}
      />
    );
  };

  const handleClickLogin = (values) => {
    if (recaptCha) {
      const body = {
        email: values.email,
        password: values.password,
      };
      dispatch(
        loginInit(body, (Response) => {
          resetCaptcha();
          if (!Response.bool) {
            document.getElementById("title").value = Response.message;
            document.getElementById("message").value = "";
            document.getElementById("type").value = "error";
            document.getElementById("click").click();
          } else if (Response && Response.showModal) {
            setBody(Response.body);
            document.getElementById("modaldemoAuto").click();
          } else {
            history.push("/Home");
          }
        })
      );
    } else {
      setErrorRecapCha("Error en el captcha.");
    }
  };

  const handleClickGoogleAuthenticator = () => {
    const formData = new FormData();
    formData.append("code", codeQr);
    formData.append("email", body.email);
    dispatch(
      enterCodeAuthenticatorGoogleInit(formData, (Response) => {
        if (Response.bool) {
          document.querySelector(".close").click();
          history.push("/Home");
        } else {
          document.getElementById("title").value = Response.message;
          document.getElementById("message").value = "";
          document.getElementById("type").value = "error";
          document.getElementById("click").click();
        }
      })
    );
  };

  return (
    <div className="h-100vh custom-page1">
      <div className="col-sm-6 col-md-4 col-xl-3">
        <a
          className="modal-effect btn btn-primary btn-block mb-3"
          data-effect="effect-scale"
          data-toggle="modal"
          href="#modaldemo8"
          id="modaldemoAuto"
          style={{ display: "none" }}
        >
          Scale
        </a>
      </div>
      <div className="modal" id="modaldemo8">
        <div
          className="modal-dialog modal-dialog-centered text-center"
          role="document"
        >
          <div className="modal-content modal-content-demo">
            <div className="modal-header">
              <h6 className="modal-title">Google Authenticator</h6>
              <button
                aria-label="Close"
                className="close"
                data-dismiss="modal"
                type="button"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <h6>Debes introducir el código 2FA.</h6>
            </div>
            <div className="modal-body">
              <div>
                <input
                  className="code_2FA"
                  maxLength="6"
                  onChange={(event) => setCodeQr(event.target.value)}
                  value={codeQr}
                />
              </div>
            </div>
            <div className="modal-footer">
              <Button
                className={
                  isCodeEnterAuthenticatorGoogle
                    ? "btn btn-loading"
                    : "btn text-btn"
                }
                type="button"
                small="100%"
                height="50px"
                onClick={handleClickGoogleAuthenticator}
              >
                {isCodeEnterAuthenticatorGoogle ? (
                  <div style={{ height: "30px" }} />
                ) : (
                  "Enviar"
                )}
              </Button>
            </div>
          </div>
        </div>
      </div>
      <Container>
        <ContainerForm>
          <div className="custom-logo total-center">
            <div onClick={() => history.push("/")}>
              <Logo src="../../assets/images/logo-completo.png" alt="logo" />
            </div>
          </div>
          <div className="antiphising-container">
            <p className="title-antiphising">Precaución antiphising</p>
            <p className="secondary-text-antiphising">
              Asegúrate que en tu navegador la url que aparece es:
            </p>
            <p className="link-antiphising">
              https://www.karakorumcorp.com/login
            </p>
          </div>
          {/* <div className="total-center">
            <h3>Iniciar sesión</h3>
          </div> */}
          <Formik
            onSubmit={handleClickLogin}
            initialValues={{
              email: "",
              password: "",
            }}
            validationSchema={Yup.object({
              email: Yup.string()
                .required("El campo es requerido*")
                .email("Ingrese un correo electrónico válido*"),
              password: Yup.string().required("El campo es requerido*"),
            })}
          >
            {({ values, handleChange, handleBlur }) => {
              return (
                <Form>
                  {[
                    {
                      title: "Correo electrónico",
                      id: "email",
                      type: "text",
                    },
                    {
                      title: "Contraseña",
                      id: "password",
                      type: "password",
                    },
                  ].map((form, key) => {
                    return (
                      <div className="input-group mb-4" key={key}>
                        <div className="width">
                          <input
                            className="form-control"
                            placeholder={form.title}
                            id={form.id}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            value={values[form.id]}
                            type={form.type}
                          />
                        </div>
                        <ErrorMessage
                          name={form.id}
                          component="div"
                          className="textErrorLabel"
                        />
                      </div>
                    );
                  })}
                  <div style={{ marginTop: "10px" }}>
                    {_renderRecaptcha()}
                    {errorRecapCha && (
                      <p className="textErrorLabel">{errorRecapCha}</p>
                    )}
                  </div>
                  <div className="row">
                    <div className="col-6" />
                    <div className="col-6 text-right mt-1">
                      <div
                        className="text-muted color-text"
                        onClick={() => history.push("/forgot-password")}
                      >
                        Recuperar contraseña?
                      </div>
                    </div>
                    <div className="col-12 mt-5">
                      <Button
                        className={
                          isLoadingIn ? "btn btn-loading" : "btn text-btn"
                        }
                        small="100%"
                        height="50px"
                        type="submit"
                        href="#modaldemo8"
                      >
                        {isLoadingIn ? (
                          <div style={{ width: "200px" }} />
                        ) : (
                          "Iniciar sesión"
                        )}
                      </Button>
                    </div>
                  </div>
                  <div className="text-center mt-7 mb-5">
                    <div className="font-weight-normal fs-16 text-muted">
                      No tienes una cuenta{" "}
                      <span
                        className="btn-link font-weight-normal color-text"
                        onClick={() => history.push("/register")}
                      >
                        Regístrate aquí
                      </span>
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
        </ContainerForm>
        <div className="background-login"></div>
      </Container>
    </div>
  );
}
const Container = styled.div`
  width: 100%;
  min-height: 100vh;
  display: grid;
  grid-template-columns: 400px 1fr;
  justify-items: center;
  align-items: center;
  @media (max-width: 768px) {
    background-image: url("assets/images/login-bg.jpg");
    background-size: cover;
    background-position: center;
    grid-template-columns: 1fr;
    padding: 10px 0px;
  }
  @media (max-width: 620px) {
    min-height: 100vh;
  }
`;
const ContainerForm = styled.div`
  width: 100%;
  max-width: 320px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  @media (max-width: 768px) {
    background-color: rgb(255 255 255);
    padding: 20px;
    border-radius: 10px;
    max-width: 380px;
    min-height: 80vh;
  }
  @media (max-width: 420px) {
    width: 90vw;
  }
`;
const Logo = styled.img`
  width: 200px;
  height: 50px;
  object-fit: contain;

  @media (max-width: 768px) {
    height: 35px;
  }
`;
