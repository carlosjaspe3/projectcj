import React from "react";
import { useHistory, useRouteMatch } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { activeAccountInit } from "../../ducks/access/actions";
import styled from "styled-components";

const ActivateAccount = () => {
  const history = useHistory();
  const match = useRouteMatch();
  const dispatch = useDispatch();
  const isLoading = useSelector((state) => state.accessStore.isActiveAccount);
  const handleClickActiveAccount = () => {
    dispatch(
      activeAccountInit(match.params.id, (Response) => {
        if (!Response.bool) {
          document.getElementById("title").value = Response.message;
          document.getElementById("message").value = "";
          document.getElementById("type").value = "error";
          document.getElementById("click").click();
        } else {
          document.getElementById("title").value = "¡Cuenta Activada!";
          document.getElementById("message").value = "";
          document.getElementById("type").value = "success";
          document.getElementById("click").click();
          history.push("/login");
        }
      })
    );
  };
  return (
    <div className="bg-white">
      <Container>
        <ContainerForm className="card-body mt-lg-9">
          <div>
            <div onClick={() => history.push("/")}>
              <Logo src="../../assets/images/logo-completo.png" alt="logo" />
            </div>
          </div>
          <div className="mb-6">
            <h3 className="mb-2">Activar cuenta</h3>
          </div>
          <div className="row">
            <div className="col-12 mt-5">
              <button
                type="submit"
                className={
                  isLoading
                    ? "btn btn-lg btn-primary btn-block btn-loading"
                    : "btn btn-lg btn-primary btn-block"
                }
                onClick={handleClickActiveAccount}
              >
                {isLoading ? (
                  <div className="btn btn-lg btn-primary btn-block" />
                ) : (
                  "Activar"
                )}
              </button>
            </div>
          </div>
        </ContainerForm>
        <div className="background-recover"></div>
      </Container>
    </div>
  );
};
const Container = styled.div`
  width: 100%;
  min-height: 100vh;
  display: grid;
  grid-template-columns: 400px 1fr;
  justify-items: center;
  align-items: center;
  @media (max-width: 768px) {
    background-image: url("assets/images/register-bg.jpg");
    background-size: cover;
    background-position: center;
    grid-template-columns: 1fr;
    padding-top: 10px;
    padding-bottom: 10px;
  }
`;
const ContainerForm = styled.div`
  width: 100%;
  max-width: 320px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  form {
    width: 100% !important;
  }
  @media (max-width: 768px) {
    background-color: rgb(255 255 255);
    padding: 20px;
    border-radius: 10px;
    max-width: 380px;
    min-height: 80vh;
  }
  @media (max-width: 420px) {
    width: 90vw;
  }
`;
const Logo = styled.img`
  width: 200px;
  height: 50px;
  object-fit: contain;
  margin-bottom: 20px;

  @media (max-width: 768px) {
    height: 35px;
  }
`;
export default ActivateAccount;
