import * as React from "react";
import { useHistory, useRouteMatch } from "react-router-dom";
import { Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { changePasswordInit } from "../../ducks/access/actions";
import { isChangePasswordSelector } from "../../ducks/access/selectors";
import styled from "styled-components";

const ChangePassword = () => {
  const history = useHistory();
  const match = useRouteMatch();
  const dispatch = useDispatch();
  const isChangePassword = useSelector(isChangePasswordSelector);
  const handleClickChangePassword = (values, { resetForm }) => {
    console.log(values, match);
    const body = {
      token: match.params.id,
      password: values.repeatPassword,
    };
    dispatch(
      changePasswordInit(body, (Response) => {
        if (!Response.bool) {
          document.getElementById("title").value = Response.message;
          document.getElementById("message").value = "";
          document.getElementById("type").value = "error";
          document.getElementById("click").click();
        } else {
          document.getElementById("title").value = "Contraseña cambiada!";
          document.getElementById("message").value = "";
          document.getElementById("type").value = "success";
          document.getElementById("click").click();
          resetForm();
          history.push("/login");
        }
      })
    );
    console.log(body);
  };

  return (
    <div className="bg-white">
      <Container>
        <ContainerForm>
          <div className="custom-logo">
            <div onClick={() => history.push("/")}>
              <Logo src="../../assets/images/logo-completo.png" alt="logo" />
            </div>
          </div>
          <div className="">
            <h3>Cambiar contraseña</h3>
          </div>
          <Formik
            onSubmit={handleClickChangePassword}
            initialValues={{
              password: "",
              repeatPassword: "",
            }}
            validationSchema={Yup.object({
              password: Yup.string()
                .min(6, "Debe tener mínimo 6 caracteres")
                .required("El campo es requerido*"),
              repeatPassword: Yup.string()
                .oneOf(
                  [Yup.ref("password"), null],
                  "Las contraseñas no coinciden*"
                )
                .required("El campo es requerido*"),
            })}
          >
            {({ values, handleChange, handleBlur }) => {
              return (
                <Form>
                  <div className="form-group">
                    <div className="width">
                      <input
                        type="password"
                        className="form-control"
                        placeholder="Nueva contraseña"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        id="password"
                        value={values.password}
                      />
                    </div>
                    <ErrorMessage
                      name="password"
                      component="div"
                      className="textErrorLabel"
                    />
                  </div>
                  <div className="form-group">
                    <div className="width">
                      <input
                        type="password"
                        className="form-control"
                        placeholder="Repetir contraseña"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        id="repeatPassword"
                        value={values.repeatPassword}
                      />
                    </div>
                    <ErrorMessage
                      name="repeatPassword"
                      component="div"
                      className="textErrorLabel"
                    />
                  </div>
                  <div className="row">
                    <div className="col-12 mt-5">
                      <button
                        type="submit"
                        className={
                          isChangePassword
                            ? "btn btn-lg btn-primary btn-block btn-loading gradien-style-buttons"
                            : "btn btn-lg btn-primary btn-block gradien-style-buttons"
                        }
                      >
                        {isChangePassword ? (
                          <div className="btn btn-lg btn-primary btn-block" />
                        ) : (
                          "Enviar"
                        )}
                      </button>
                    </div>
                  </div>
                </Form>
              );
            }}
          </Formik>
          <div className="text-center mt-7 mb-5">
            <div
              className="font-weight-normal btn-login fs-16 text-muted color-text"
              onClick={() => history.push("/login")}
            >
              Iniciar sesión
            </div>
          </div>
        </ContainerForm>
        <div className="background-recover"></div>
      </Container>
    </div>
  );
};
const Container = styled.div`
  width: 100%;
  min-height: 100vh;
  display: grid;
  grid-template-columns: 400px 1fr;
  justify-items: center;
  align-items: center;
  @media (max-width: 768px) {
    background-image: url("assets/images/recover-bg.jpg");
    background-size: cover;
    background-position: center;
    grid-template-columns: 1fr;
    padding-top: 10px;
    padding-bottom: 10px;
  }
`;
const ContainerForm = styled.div`
  width: 100%;
  max-width: 320px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  form {
    width: 100% !important;
  }
  @media (max-width: 768px) {
    background-color: rgb(255 255 255);
    padding: 20px;
    border-radius: 10px;
    max-width: 380px;
    min-height: 80vh;
  }
  @media (max-width: 420px) {
    width: 90vw;
  }
`;
const Logo = styled.img`
  width: 200px;
  height: 50px;
  object-fit: contain;
  margin-bottom: 20px;

  @media (max-width: 768px) {
    height: 35px;
  }
`;
export default ChangePassword;
