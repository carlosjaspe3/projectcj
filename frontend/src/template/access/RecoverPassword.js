import React from "react";
import Botton from "../../components/Botton";
import Input from "../../components/Input";

const RecoverPassword = () => {
  return (
    <div className="page">
      <div className="page-single">
        <div className="container">
          <div className="row">
            <div className="col mx-auto">
              <div className="row justify-content-center">
                <div className="col-md-7 col-lg-4">
                  <div className="card mb-0">
                    <div className="card-body">
                      <div className="text-center  mb-6">
                        <h2 className="mb-2">Recuperar contraseña</h2>
                      </div>
                      <div className="form-group">
                        <Input placeholder="Correo electrónico" fullWidth />
                      </div>
                      <div className="row">
                        <div className="col-12">
                          <div className="form-group mb-0">
                            <label className="custom-control custom-checkbox mb-0">
                              <input
                                type="checkbox"
                                className="custom-control-input"
                              />
                              <span className="custom-control-label text-muted">
                                Acepta los términos y la política
                              </span>
                            </label>
                          </div>
                        </div>
                        <div className="col-12 mt-5">
                          <Botton fullWidth>Enviar</Botton>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RecoverPassword;
