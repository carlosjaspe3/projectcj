import React from "react";
import Layout from "../components/layout/Index";

const logo = [
  {
    img: "http://lorempixel.com/g/400/200/",
  },
  {
    img: "http://lorempixel.com/g/400/200/",
  },
  {
    img: "http://lorempixel.com/g/400/200/",
  },
];

const promotionBanners = [
  {
    img: "http://lorempixel.com/g/400/200/",
    height: "250px",
    width: "300px",
    link: "https://mind.capital/?referral=Q73d2A6z6U",
  },
  {
    img: "http://lorempixel.com/g/400/200/",
    height: "250px",
    width: "300px",
    link: "https://mind.capital/?referral=Q73d2A6z6U",
  },
  {
    img: "http://lorempixel.com/g/400/200/",
    height: "250px",
    width: "300px",
    link: "https://mind.capital/?referral=Q73d2A6z6U",
  },
];

const promotionBannersSm = [
  {
    img:
      "https://downloadwap.com/thumbs2/wallpapers/p2ls/2019/nature/45/g9l6p12313319939.jpg",
    height: "525px",
    width: "300px",
    link: "https://mind.capital/?referral=Q73d2A6z6U",
  },
  {
    img:
      "https://downloadwap.com/thumbs2/wallpapers/p2ls/2019/nature/45/g9l6p12313319939.jpg",
    height: "525px",
    width: "300px",
    link: "https://mind.capital/?referral=Q73d2A6z6U",
  },
  {
    img:
      "https://downloadwap.com/thumbs2/wallpapers/p2ls/2019/nature/45/g9l6p12313319939.jpg",
    height: "525px",
    width: "300px",
    link: "https://mind.capital/?referral=Q73d2A6z6U",
  },
];

const Promotions = () => {
  const handleCopyToClipboard = (content) => {
    const el = document.createElement("textarea");
    el.value = content;
    console.log(el);
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
    console.log("Se ha copiado la info");
  };
  return (
    <div className="app sidebar-mini">
      <Layout>
        <div className="page-header">
          <div className="page-leftheader">
            <h4 className="page-title">Promoción</h4>
          </div>
        </div>
        <div className="card">
          <div className="card-header">
            <p style={{ marginTop: "10px", fontSize: "18px" }}>
              Si quieres promocionar <strong>karakorum</strong> puedes utilizar
              cualquiera de los siguientes banners. Acuérdate de enlazarlos
              siempre con tu enlace de referido para que te ayuden a generar
              ingresos.
            </p>
          </div>
          <div className="card-header">
            <h4 className="page-title">Logos</h4>
          </div>
          <div className="row">
            <div className="col-xl-12 col-lg-12">
              <div className="row">
                {logo.map((item, key) => {
                  return (
                    <div
                      className="col-sm-4 col-md-4 total-center margin-bottom"
                      key={key}
                    >
                      <img
                        src={item.img}
                        style={{
                          height: "150px",
                          width: "250px",
                          objectFit: "cover",
                        }}
                        alt="logo"
                      />
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
          <div className="card-header">
            <h4 className="page-title">Banners</h4>
          </div>
          <div className="row" style={{ marginTop: "30px" }}>
            <div className="col-xl-12 col-lg-12">
              <div className="row">
                {promotionBanners.map((item, key) => {
                  return (
                    <div className="col-sm-4 col-md-4 total-center margin-bottom">
                      <div>
                        <img
                          src={item.img}
                          style={{
                            height: item.height,
                            width: item.width,
                            objectFit: "cover",
                          }}
                          alt="logo"
                        />
                        <div className="container-link-banners">
                          {item.link}{" "}
                          <i
                            title="fa fa-clone"
                            className="fa fa-clone copy"
                            onClick={() => handleCopyToClipboard(item.link)}
                          />
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
          <div style={{ marginTop: "30px" }}>
            <div className="row">
              <div className="col-xl-12 col-lg-12">
                <div className="row">
                  {promotionBannersSm.map((item, key) => {
                    return (
                      <div
                        className="col-sm-4 col-md-4 total-center margin-bottom"
                        key={key}
                      >
                        <div>
                          <img
                            src={item.img}
                            style={{
                              height: item.height,
                              width: item.width,
                              objectFit: "cover",
                            }}
                            alt="logo"
                          />
                          <div
                            className="container-link-banners"
                            style={{ width: item.width }}
                          >
                            {item.link}{" "}
                            <i
                              title="fa fa-clone"
                              className="fa fa-clone copy"
                              onClick={() => handleCopyToClipboard(item.link)}
                            />
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
          <div style={{ marginTop: "30px" }}>
            <div className="row">
              <div className="col-xl-12 col-lg-12">
                <div className="row">
                  <div className="col-sm-12 col-md-12 total-center margin-bottom">
                    <div style={{ width: "100%" }}>
                      <img
                        src="https://downloadwap.com/thumbs2/wallpapers/p2ls/2019/nature/45/g9l6p12313319939.jpg"
                        style={{
                          height: "250px",
                          width: "100%",
                          objectFit: "cover",
                        }}
                        alt="logo"
                      />
                      <div
                        className="container-link-banners"
                        style={{ width: "100%", height: "55px" }}
                      >
                        {`https://mind.capital/?referral=Q73d2A6z6U`}{" "}
                        <i
                          title="fa fa-clone"
                          className="fa fa-clone copy"
                          onClick={() =>
                            handleCopyToClipboard(
                              `https://mind.capital/?referral=Q73d2A6z6U`
                            )
                          }
                        />
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-12 col-md-12 total-center margin-bottom">
                    <div style={{ width: "100%" }}>
                      <img
                        src="https://downloadwap.com/thumbs2/wallpapers/p2ls/2019/nature/45/g9l6p12313319939.jpg"
                        style={{
                          height: "250px",
                          width: "100%",
                          objectFit: "cover",
                        }}
                        alt="logo"
                      />
                      <div
                        className="container-link-banners"
                        style={{ width: "100%", height: "55px" }}
                      >
                        {`https://mind.capital/?referral=Q73d2A6z6U`}{" "}
                        <i
                          title="fa fa-clone"
                          className="fa fa-clone copy"
                          onClick={() =>
                            handleCopyToClipboard(
                              `https://mind.capital/?referral=Q73d2A6z6U`
                            )
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </div>
  );
};

export default Promotions;
