import React, { useState } from "react";
import { Button } from "reactstrap";
import { ModalsMovements } from "./../components/Modals/ModalsMovements";

const TestPage = () => {
  const [isOpen, setModal] = useState(false);

  const toggle = () => setModal(!isOpen);
  return (
    <div className="container">
      <div className="main">
        <div className="row">
          <div className="col-md-12">
            <Button color="primary" className="mt-30" onClick={toggle}>
              ModalsMovements
            </Button>

            <ModalsMovements isOpen={isOpen} handleClose={toggle} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default TestPage;
