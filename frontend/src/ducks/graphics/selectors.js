export const isLoggingInSelector = (state) => state.graphicsStore.isLoggingIn;
export const graphicPlatformPerformanceSelector = (state) =>
  state.graphicsStore.graphicPlatformPerformance;
export const isLoggingDashboardInSelector = (state) =>
  state.graphicsStore.isLoggingDashboardIn;
export const performanceLast30DaysSelector = (state) =>
  state.graphicsStore.performanceLast30Days;
export const infoTotalSelector = (state) => state.graphicsStore.infoTotal;
