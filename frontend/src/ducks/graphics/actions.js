import * as types from "./types";

export function getPerformanceInit(payload, callback) {
  return {
    type: types.GET_PERFORMANCE_INIT,
    payload,
    callback,
  };
}

export function getPerformanceDashboardInit(payload, callback) {
  return {
    type: types.GET_DASHBOARD_INIT,
    payload,
    callback,
  };
}
