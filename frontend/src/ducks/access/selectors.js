export const isRegisteringSelector = (state) => state.accessStore.isRegistering;
export const isLoggingInSelector = state => state.accessStore.isLoggingIn
export const isRecoveryPasswordSelector = state => state.accessStore.isRecoveryPassword
export const isChangePasswordSelector = state => state.accessStore.isChangePassword
export const userSelector = state => state.accessStore.user