import * as types from "./types";

export function loginInit(payload, callback) {
  return {
    type: types.LOGIN_INIT,
    payload,
    callback,
  };
}

export function registerInit(payload, callback) {
  return {
    type: types.REGISTER_INIT,
    payload,
    callback,
  };
}

export function verificationsEmailInit(payload, callback) {
  return {
    type: types.VERIFICATIONS_EMAIL_INIT,
    payload,
    callback,
  };
}

export function activeAccountInit(payload, callback) {
  return {
    type: types.ACTIVE_ACCOUNT_INIT,
    payload,
    callback,
  };
}

export function recoveryPasswordInit(payload, callback) {
  return {
    type: types.RECOVERY_PASSWORD_INIT,
    payload,
    callback,
  };
}

export function changePasswordInit(payload, callback) {
  return {
    type: types.CHANGE_PASSWORD_INIT,
    payload,
    callback,
  };
}

export function authenticatorGoogleInit(payload, callback) {
  return {
    type: types.GOOGLE_AUTHENTICADOR_QR_INIT,
    payload,
    callback,
  };
}

export function activeAuthenticatorGoogleInit(payload, callback) {
  return {
    type: types.ACTIVE_GOOGLE_AUTHENTICADOR_QR_INIT,
    payload,
    callback,
  };
}

export function enterCodeAuthenticatorGoogleInit(payload, callback) {
  return {
    type: types.ENTER_CODE_GOOGLE_AUTHENTICADOR_INIT,
    payload,
    callback,
  };
}

export function logout() {
  return {
    type: types.LOGOUT,
  };
}
