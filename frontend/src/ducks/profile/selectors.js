export const isLoggingInSelector = (state) => state.profileStore.isLoggingIn;
export const profileSelector = (state) => state.profileStore.profile;
export const isPutPasswordSelector = (state) => state.profileStore.isPutPassword;
