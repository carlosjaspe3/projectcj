import numeral from "numeral";
import moment from "moment-timezone";

export const formatCurrency = (value) => {
  return numeral(parseFloat(value).toFixed(2)).format("0,0[.]00");
};

export const searchToObject = () => {
  let parameters = window.location.search.substring(1).split("&"),
    obj = {},
    parameter,
    index;

  for (index in parameters) {
    if (parameters[index] === "") continue;

    parameter = parameters[index].split("=");
    obj[decodeURIComponent(parameter[0])] = decodeURIComponent(parameter[1]);
  }

  return obj;
};

const MILLISECONDS_OF_A_SECOND = 1000;
const MILLISECONDS_OF_A_MINUTE = MILLISECONDS_OF_A_SECOND * 60;
const MILLISECONDS_OF_A_HOUR = MILLISECONDS_OF_A_MINUTE * 60;
const MILLISECONDS_OF_A_DAY = MILLISECONDS_OF_A_HOUR * 24;

export const updateCountdown = ({ endDate, callRunning, callFinalized }) => {
  moment.tz("Europe/Madrid");
  const DATE_TARGET = new Date(endDate);

  const nowES = moment().tz("Europe/Madrid").format("YYYY-MM-DD HH:mm:ss");
  const NOW = new Date(nowES);
  const DURATION = DATE_TARGET - NOW;
  const REMAINING_DAYS = Math.floor(DURATION / MILLISECONDS_OF_A_DAY);
  const REMAINING_HOURS = Math.floor(
    (DURATION % MILLISECONDS_OF_A_DAY) / MILLISECONDS_OF_A_HOUR
  );
  const REMAINING_MINUTES = Math.floor(
    (DURATION % MILLISECONDS_OF_A_HOUR) / MILLISECONDS_OF_A_MINUTE
  );
  const REMAINING_SECONDS = Math.floor(
    (DURATION % MILLISECONDS_OF_A_MINUTE) / MILLISECONDS_OF_A_SECOND
  );

  const FINALIZED =
    REMAINING_DAYS <= 0 &&
    REMAINING_HOURS <= 0 &&
    REMAINING_MINUTES <= 0 &&
    REMAINING_SECONDS <= 0;
  if (FINALIZED) {
    if (typeof callFinalized === "function") {
      callFinalized();
    }
  } else {
    if (typeof callRunning === "function") {
      callRunning({
        FINALIZED,
        REMAINING_DAYS,
        REMAINING_HOURS,
        REMAINING_MINUTES,
        REMAINING_SECONDS,
      });
    }
  }
};
